#include "tcpservercycle.h"

TCPServerCycle::TCPServerCycle(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<TCPServerCycleData>>("QList<TCPServerCycleData>");
    m_bContinue = true;
}

void TCPServerCycle::SetCycleData(const QList<TCPServerCycleData> CycleDatas, const unsigned int delay)
{
    m_TCPServerCycleDatas = CycleDatas;
    m_nDelay = delay;
}

void TCPServerCycle::SetContinue(bool contin)
{
    m_bContinue = contin;
}

void TCPServerCycle::Start()
{
    while(true)
    {
        if (m_TCPServerCycleDatas.isEmpty())
        {
            return ;
        }
        for (int i = 0; i < m_TCPServerCycleDatas.count(); i++)
        {
            if (!m_TCPServerCycleDatas[i].senddata.isEmpty())
            {
                emit SendDataSignal(i);
            }
            MySleep(m_TCPServerCycleDatas[i].delay);
            MySleep(m_nDelay);
            //qDebug() << "continue:" << m_bContinue;
            if (!m_bContinue)
            {
                return ;
            }
        }
    }
}

void TCPServerCycle::StartCycle(const QList<TCPServerCycleData> CycleDatas, const unsigned int delay)
{
    SetCycleData(CycleDatas, delay);
    Start();
}
