#ifndef TCPSERVERSENIORSENDCYCLE_H
#define TCPSERVERSENIORSENDCYCLE_H

#include <QWidget>
#include <QStandardItemModel>
#include <QCloseEvent>
#include <QFile>
#include <QFileDialog>
#include <QDomDocument>

#include "Main/basecode.h"

namespace Ui {
class TCPServerSeniorSendCycle;
}

class TCPServerSeniorSendCycle : public QWidget
{
    Q_OBJECT

public:
    explicit TCPServerSeniorSendCycle(QWidget *parent = nullptr);
    ~TCPServerSeniorSendCycle();

    void inform(QString strdata);
    void SetData(QList<TCPServerCycleData> TCPServerCycleDatas, int data);   // 设置
    void SetObjects(QList<TCPServerClientData> cdatas);

    void show();
    void InitQSS();

signals:
    void CloseSignal();    // 关闭
    void FinishSignal(QList<TCPServerCycleData> TCPServerCycleDatas, unsigned int delay);        // 完成
    void JudgeDataSignal(bool &sameok, bool &overstate, QList<TCPServerCycleData> TCPServerCycleDatas, int delay);   // 判断是否一致

private slots:
    void closeEvent(QCloseEvent *event);
    void on_btn_close_clicked();
    void on_btn_add_clicked();
    void on_btn_insert_clicked();
    void on_btn_revise_clicked();
    void on_btn_delete_clicked();
    void on_tableView_ShowEvents_clicked(const QModelIndex &index);
    void on_btn_ok_clicked();
    void on_btn_import_clicked();
    void on_btn_derive_clicked();

private:
    Ui::TCPServerSeniorSendCycle *ui;

    void ShowEventsInit();
    void ShowEventsAdd(TCPServerCycleData sc);
    void ShowEventsInsert(int row, TCPServerCycleData sc);
    void ShowEventsRevise(int row, TCPServerCycleData sc);
    void ShowEventsRemove(int row);
    void ShowEventsClear();
    bool SaveXml(QString path);
    //bool SaveXml();
    bool ReadXml(QString path);
    //bool ReadXml();

    void InitTitlepar();

    WidgetPar *m_selMainWidget;

    QStandardItemModel *m_staCycle;
    int m_CycleChoose;
    QList<TCPServerCycleData> m_TCPServerCycleDatas;
};

#endif // TCPSERVERSENIORSENDCYCLE_H
