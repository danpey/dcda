#include "tcpserverinteractunit.h"
#include "ui_tcpserverinteractunit.h"

TCPServerInteractUnit::TCPServerInteractUnit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TCPServerInteractUnit)
{
    ui->setupUi(this);

    InitTitlepar();
}

TCPServerInteractUnit::~TCPServerInteractUnit()
{
    delete ui;
}

void TCPServerInteractUnit::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    TitleBar *pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showMinmizeBtn(false);
    pTitleBar->showMaxmizeBtn(false);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &TCPServerInteractUnit::closeEvent);
    m_selMainWidget->setWidgetResizable(false);  // 设置窗体不可缩放
    m_selMainWidget->setWindowModality(Qt::ApplicationModal);

    m_selMainWidget->setWindowTitle(QString("TCP Server 收发互动事件属性"));
    this->setWindowTitle(QString("TCP Server 收发互动事件属性"));
    setWindowIcon(QIcon(":/image/DCDA.ico"));

    ui->comboBox_readobject->setView(new QListView(this));
    ui->comboBox_mode->setView(new QListView(this));
    ui->comboBox_writeobject->setView(new QListView(this));
}

void TCPServerInteractUnit::InitQSS()
{
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();
}

void TCPServerInteractUnit::show()
{
    m_selMainWidget->show();
}

void TCPServerInteractUnit::clear()
{
    ui->comboBox_mode->setCurrentIndex(0);
    ui->textEdit_Read->clear();
    ui->checkBox_ReadHex->setChecked(false);
    ui->textEdit_Write->clear();
    ui->comboBox_writeobject->setCurrentIndex(0);
    ui->comboBox_readobject->clear();
    ui->comboBox_writeobject->clear();
}

void TCPServerInteractUnit::SetAdd(bool add)
{
    m_bAdd = add;
}

void TCPServerInteractUnit::SetData(TCPServerInteractData si)
{
    ui->comboBox_mode->setCurrentIndex(si.mode);
    ui->textEdit_Read->setText(si.readdata);
    ui->textEdit_Write->setText(si.writedata);
    if (si.readTextFormat == HEX)
    {
        ui->checkBox_ReadHex->setChecked(true);
    }
    else
    {
        ui->checkBox_ReadHex->setChecked(false);
    }

    ui->comboBox_sendType->setCurrentIndex(si.sendTextFormat);
    if (si.readip == "ALL Connections")
    {
        ui->comboBox_readobject->setCurrentText(si.readip);
    }
    else
    {
        ui->comboBox_readobject->setCurrentText(si.readip + ":" + QString::number(si.readport));
    }
    if (si.writeip == "ALL Connections")
    {
        ui->comboBox_writeobject->setCurrentText(si.writeip);
    }
    else
    {
        ui->comboBox_writeobject->setCurrentText(si.writeip + ":" + QString::number(si.writeport));
    }
}

///
/// \brief 导入客户端名
/// \param cdatas
///
void TCPServerInteractUnit::SetObjects(QList<TCPServerClientData> cdatas)
{
    ui->comboBox_readobject->addItem("ALL Connections");
    ui->comboBox_writeobject->addItem("ALL Connections");
    for (int i = 1; i < cdatas.count(); i++)
    {
        ui->comboBox_readobject->addItem(cdatas[i].ip + ":" + QString::number(cdatas[i].port));
        ui->comboBox_writeobject->addItem(cdatas[i].ip + ":" + QString::number(cdatas[i].port));
    }
}

///
/// \brief 确定
///
void TCPServerInteractUnit::on_btn_ok_clicked()
{
    TCPServerInteractData si;
    si.mode = ui->comboBox_mode->currentIndex();
    si.modename = ui->comboBox_mode->currentText();
    si.readdata = ui->textEdit_Read->toPlainText();
    si.writedata = ui->textEdit_Write->toPlainText();
    if (ui->checkBox_ReadHex->isChecked())
    {
        si.readTextFormat = HEX;
    }
    else {
        si.readTextFormat = Local;
    }
    si.sendTextFormat = static_cast<TextFormat>(ui->comboBox_sendType->currentIndex());
    QString readobj = ui->comboBox_readobject->currentText();
    QString writeobj = ui->comboBox_writeobject->currentText();
    if (readobj == "ALL Connections")
    {
        si.readip = readobj;
        si.readport = 0;
    }
    else
    {
        si.readip = readobj.section(':', 0, 0);
        si.readport = static_cast<quint16>(readobj.section(':', 1, 1).toInt());
    }
    if (writeobj == "ALL Connections")
    {
        si.writeip = writeobj;
        si.writeport = 0;
    }
    else
    {
        si.writeip = writeobj.section(':', 0, 0);
        si.writeport = static_cast<quint16>(writeobj.section(':', 1, 1).toInt());
    }
    if (si.readdata.isEmpty())
    {
        inform("请将填写读取信息！");
    }
    else if (si.writedata.isEmpty())
    {
        inform("请填写发送信息！");
    }
    else
    {
        if (si.readTextFormat == HEX)
        {
            if (si.readdata != "[alldata]")
            {
                if (!toHex(si.readdata))
                {
                    inform("读取数据不是Hex数据！请重新填写！");
                    return ;
                }
            }
        }
        if (si.sendTextFormat == HEX)
        {
            if (si.readdata != "[alldata]")
            {
                if (!toHex(si.writedata))
                {
                    inform("发送数据不是Hex数据！请重新填写！");
                    return ;
                }
            }
        }
        if (m_bAdd)
        {
            emit AddSignal(si);
        }
        else
        {
            emit ReviseSignal(si);
        }
        m_selMainWidget->close();
    }
}

///
/// \brief 关闭
///
void TCPServerInteractUnit::on_btn_close_clicked()
{
    m_selMainWidget->close();
}

///
/// \brief 弹窗通知
/// \param strdata: 通知内容
///
void TCPServerInteractUnit::inform(QString strdata)
{
    QMessageBox m_r;

    WidgetPar *wid;
    wid = new WidgetPar;
    wid->setWindowFlags(Qt::FramelessWindowHint);
    wid->setWindowModality(Qt::ApplicationModal);
    m_r.setParent(wid);
    TitleBar *titbar = new TitleBar(wid);

    titbar->showMinmizeBtn(false);
    titbar->showMaxmizeBtn(false);
    titbar->EnableCloseBtn(false);

    m_r.installEventFilter(titbar);
    QGridLayout *layout = new QGridLayout();

    layout->addWidget(titbar);  //添加标题栏
    layout->addWidget(&m_r);       //添加UI界面
    layout->setSpacing(0);         //布局之间的距离
    layout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    wid->setLayout(layout);  //将这个布局器设置在QWidget上
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);

    wid->setWidgetResizable(false);

    wid->move(m_selMainWidget->x() + (m_selMainWidget->width() - 150 - 3*strdata.length())/2, m_selMainWidget->y() + (m_selMainWidget->height() - (titbar->height() + 50))/2);
    wid->setTitleHeight(static_cast<uint>(titbar->height()));

    m_r.setWindowTitle("提示");
    m_r.setWindowIcon(QIcon(":/image/DCDA.ico"));
    m_r.setText(strdata);

    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        wid->setStyleSheet(stylesheet);
    }
    file.close();

    wid->show();
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);
    m_r.exec();
    wid->close();
    titbar->deleteLater();
    layout->deleteLater();
    wid->deleteLater();
    titbar = nullptr;
    layout = nullptr;
    wid = nullptr;
}

