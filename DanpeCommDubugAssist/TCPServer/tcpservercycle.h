#ifndef TCPSERVERCYCLE_H
#define TCPSERVERCYCLE_H

#include <QObject>
#include <QMetaType>
#include "Main/basecode.h"

class TCPServerCycle : public QObject
{
    Q_OBJECT
public:
    explicit TCPServerCycle(QObject *parent = nullptr);

    void SetContinue(bool contin);

signals:
    void SendDataSignal(int num);
    //void SendOverSignal();

public slots:
    void SetCycleData(const QList<TCPServerCycleData> SerialPortCycleDatas, const unsigned int delay);   // 导入数据
    // 开始
    void StartCycle(const QList<TCPServerCycleData> SerialPortCycleDatas, const unsigned int delay);
    void Start();

private:
    bool m_bContinue = true;

    QList<TCPServerCycleData> m_TCPServerCycleDatas;
    unsigned int m_nDelay;
};

#endif // TCPSERVERCYCLE_H
