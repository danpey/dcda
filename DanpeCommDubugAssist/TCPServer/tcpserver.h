#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

#include "Main/basecode.h"

class TCPServer : public QObject
{
    Q_OBJECT
public:
    explicit TCPServer(QObject *parent = nullptr);

    bool StartListening(QString ip, quint16 port);
    bool isListening();
    QString ServerAddress();
    quint16 ServerPort();
    void StopListening();
    bool SendData(QString ip, quint16 port, const QByteArray senddata);
    void DisConnectALLClients();

signals:
    void NewConnectingSignal(QString ip, quint16 port);
    void ReadDataSignal(QString ip, quint16 port, QByteArray readdata);
    void DisConnectionSignal(QString ip, quint16 port);

public slots:

private:
    void NewConnection();
    void SocketRead(QTcpSocket *m_tcpsocket);

    QTcpServer *m_tcpserver;

    QTcpSocket* m_tcpsocket;

    //QList<QScopedPointer<QTcpSocket>> m_tcpsockets;
    //QList<QScopedPointer<QTcpSocket>> m_tcpsockets;
    QList<QTcpSocket*> m_tcpsockets;

};

#endif // TCPSERVER_H
