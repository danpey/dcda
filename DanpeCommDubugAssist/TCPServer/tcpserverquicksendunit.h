#ifndef TCPSERVERQUICKSENDUNIT_H
#define TCPSERVERQUICKSENDUNIT_H

#include <QWidget>
#include "Main/basecode.h"

namespace Ui {
class TCPServerQuickSendUnit;
}

class TCPServerQuickSendUnit : public QWidget
{
    Q_OBJECT

public:
    explicit TCPServerQuickSendUnit(QWidget *parent = nullptr);
    ~TCPServerQuickSendUnit();

    void clear();
    void SetAdd(bool add);
    void inform(QString strdata);
    void SetData(TCPServerQuickSendData sp);
    void SetObjects(QList<TCPServerClientData> cdatas);

    void show();
    void InitQSS();

signals:
    void AddSignal(TCPServerQuickSendData sp);
    void ReviseSignal(TCPServerQuickSendData sp);
    void closeSignal();

private slots:
    void on_btn_ok_clicked();
    void on_btn_cancel_clicked();

private:
    Ui::TCPServerQuickSendUnit *ui;

    TCPServerQuickSendData GetUIData();
    void InitTitlepar();

    WidgetPar *m_selMainWidget;
    bool m_bAdd;
};

#endif // TCPSERVERQUICKSENDUNIT_H
