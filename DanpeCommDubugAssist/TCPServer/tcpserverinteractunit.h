#ifndef TCPSERVERINTERACTUNIT_H
#define TCPSERVERINTERACTUNIT_H

#include <QWidget>
#include "Main/basecode.h"

namespace Ui {
class TCPServerInteractUnit;
}

class TCPServerInteractUnit : public QWidget
{
    Q_OBJECT

public:
    explicit TCPServerInteractUnit(QWidget *parent = nullptr);
    ~TCPServerInteractUnit();

    void clear();
    void inform(QString strdata);
    void SetAdd(bool add);
    void SetData(TCPServerInteractData si);
    void SetObjects(QList<TCPServerClientData> cdatas);

    void show();
    void InitQSS();

signals:
    void CloseSignal();
    void AddSignal(TCPServerInteractData si);
    void ReviseSignal(TCPServerInteractData si);

private slots:
    void on_btn_ok_clicked();
    void on_btn_close_clicked();

private:
    Ui::TCPServerInteractUnit *ui;

    void InitTitlepar();

    WidgetPar *m_selMainWidget;
    bool m_bAdd;

};

#endif // TCPSERVERINTERACTUNIT_H
