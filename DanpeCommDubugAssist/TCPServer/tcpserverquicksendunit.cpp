#include "tcpserverquicksendunit.h"
#include "ui_tcpserverquicksendunit.h"

TCPServerQuickSendUnit::TCPServerQuickSendUnit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TCPServerQuickSendUnit)
{
    ui->setupUi(this);

    InitTitlepar();
}

TCPServerQuickSendUnit::~TCPServerQuickSendUnit()
{
    delete ui;
}

void TCPServerQuickSendUnit::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    TitleBar *pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showMinmizeBtn(false);
    pTitleBar->showMaxmizeBtn(false);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &TCPServerQuickSendUnit::closeEvent);
    m_selMainWidget->setWidgetResizable(false);  // 设置窗体不可缩放
    m_selMainWidget->setWindowModality(Qt::ApplicationModal);

    m_selMainWidget->setWindowTitle(QString("TCP Server 快捷发送项属性"));
    this->setWindowTitle(QString("TCP Server 快捷发送项属性"));
    setWindowIcon(QIcon(":/image/DCDA.ico"));

    ui->comboBox_sendobject->setView(new QListView(this));
}

void TCPServerQuickSendUnit::InitQSS()
{
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();
}

void TCPServerQuickSendUnit::show()
{
    m_selMainWidget->show();
}

///
/// \brief 清空信息
///
void TCPServerQuickSendUnit::clear()
{
    ui->lineEdit_name->clear();
    ui->textEdit_senddata->clear();
    ui->checkBox_Hex->setChecked(false);
    ui->checkBox_AddLine->setChecked(false);
    ui->comboBox_sendobject->clear();
}

///
/// \brief 设置界面参数
/// \param sp
///
void TCPServerQuickSendUnit::SetData(TCPServerQuickSendData sp)
{
    ui->lineEdit_name->setText(sp.name);
    ui->textEdit_senddata->setText(sp.writedata);
    ui->checkBox_Hex->setChecked(sp.isHex);
    ui->checkBox_AddLine->setChecked(sp.addLine);
    if (sp.ip == "ALL Connections")
    {
        ui->comboBox_sendobject->setCurrentText(sp.ip);
    }
    else
    {
        ui->comboBox_sendobject->setCurrentText(sp.ip + ":" + QString::number(sp.port));
    }
}

///
/// \brief 导入客户端名
/// \param cdatas
///
void TCPServerQuickSendUnit::SetObjects(QList<TCPServerClientData> cdatas)
{
    ui->comboBox_sendobject->addItem("ALL Connections");
    for (int i = 1; i < cdatas.count(); i++)
    {
        ui->comboBox_sendobject->addItem(cdatas[i].ip + ":" + QString::number(cdatas[i].port));
    }
}

///
/// \brief 导入是否为增加还是修改
/// \param add
///
void TCPServerQuickSendUnit::SetAdd(bool add)
{
    m_bAdd = add;
}

///
/// \brief 获取界面数据
/// \return
///
TCPServerQuickSendData TCPServerQuickSendUnit::GetUIData()
{
    TCPServerQuickSendData sp;
    sp.name = ui->lineEdit_name->text();
    sp.writedata = ui->textEdit_senddata->toPlainText();
    sp.addLine = ui->checkBox_AddLine->isChecked();
    sp.isHex = ui->checkBox_Hex->isChecked();
    QString obj = ui->comboBox_sendobject->currentText();
    if (obj == "ALL Connections" || obj == "NULL")
    {
        sp.ip = obj;
        sp.port = 0;
    }
    else
    {
        sp.ip = obj.section(':', 0, 0);
        sp.port = static_cast<quint16>(obj.section(':', 1, 1).toInt());
    }
    return sp;
}

///
/// \brief 确定
///
void TCPServerQuickSendUnit::on_btn_ok_clicked()
{
    TCPServerQuickSendData sp = GetUIData();
    if (sp.name.isEmpty() || sp.writedata.isEmpty())  // 如果名字和内容都为空，则不保存
    {
        if (sp.name.isEmpty())
        {
            inform("没有名称！");
        }
        else
        {
            inform("没有发送内容！");
        }
    }
    else if (sp.ip == "NULL")
    {
        inform("无效客户端！");
    }
    else
    {
        if (sp.isHex)
        {
            if (!toHex(sp.writedata))
            {
                inform("发送数据不是Hex数据！请重新填写！");
                return ;
            }
        }
        if (m_bAdd)
        {
            emit AddSignal(sp);   // 正常传递数据返回参数
        }
        else
        {
            emit ReviseSignal(sp);   // 正常传递数据返回参数
        }
        m_selMainWidget->close();
    }
}

///
/// \brief 取消
///
void TCPServerQuickSendUnit::on_btn_cancel_clicked()
{
    m_selMainWidget->close();
}

///
/// \brief 通知
/// \param strdata
///
void TCPServerQuickSendUnit::inform(QString strdata)
{
    QMessageBox m_r;

    WidgetPar *wid;
    wid = new WidgetPar;
    wid->setWindowFlags(Qt::FramelessWindowHint);
    wid->setWindowModality(Qt::ApplicationModal);
    m_r.setParent(wid);
    TitleBar *titbar = new TitleBar(wid);

    titbar->showMinmizeBtn(false);
    titbar->showMaxmizeBtn(false);
    titbar->EnableCloseBtn(false);

    m_r.installEventFilter(titbar);
    QGridLayout *layout = new QGridLayout();

    layout->addWidget(titbar);  //添加标题栏
    layout->addWidget(&m_r);       //添加UI界面
    layout->setSpacing(0);         //布局之间的距离
    layout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    wid->setLayout(layout);  //将这个布局器设置在QWidget上
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);

    wid->setWidgetResizable(false);

    wid->move(m_selMainWidget->x() + (m_selMainWidget->width() - 150 - 3*strdata.length())/2, m_selMainWidget->y() + (m_selMainWidget->height() - (titbar->height() + 50))/2);
    wid->setTitleHeight(static_cast<uint>(titbar->height()));

    m_r.setWindowTitle("提示");
    m_r.setWindowIcon(QIcon(":/image/DCDA.ico"));
    m_r.setText(strdata);

    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        wid->setStyleSheet(stylesheet);
    }
    file.close();

    wid->show();
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);
    m_r.exec();
    wid->close();
    titbar->deleteLater();
    layout->deleteLater();
    wid->deleteLater();
    titbar = nullptr;
    layout = nullptr;
    wid = nullptr;
}

