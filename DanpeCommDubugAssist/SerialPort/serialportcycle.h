#ifndef SERIALPORTCYCLE_H
#define SERIALPORTCYCLE_H

#include <QObject>
#include <QMetaType>
#include "Main/basecode.h"

class SerialPortCycle : public QObject
{
    Q_OBJECT
public:
    explicit SerialPortCycle(QObject *parent = nullptr);

    void SetContinue(bool contin);

signals:
    void SendDataSignal(int num);
    //void SendOverSignal();

public slots:
    void SetCycleData(const QList<CycleData> SerialPortCycleDatas, const unsigned int delay);   // 导入数据
    // 开始
    void StartCycle(const QList<CycleData> SerialPortCycleDatas, const unsigned int delay);
    void Start();

private:
    bool m_bContinue = true;

    QList<CycleData> m_SerialPortCycleDatas;
    unsigned int m_nDelay;
};

#endif // SERIALPORTCYCLE_H
