#include "serialportcycle.h"

SerialPortCycle::SerialPortCycle(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<CycleData>>("QList<CycleData>");
    m_bContinue = true;
}

void SerialPortCycle::SetCycleData(const QList<CycleData> SerialPortCycleDatas, const unsigned int delay)
{
    m_SerialPortCycleDatas = SerialPortCycleDatas;
    m_nDelay = delay;
}

void SerialPortCycle::SetContinue(bool contin)
{
    m_bContinue = contin;
}

void SerialPortCycle::Start()
{
    while(true)
    {
        if (m_SerialPortCycleDatas.isEmpty())
        {
            return ;
        }
        for (int i = 0; i < m_SerialPortCycleDatas.count(); i++)
        {
            if (!m_SerialPortCycleDatas[i].senddata.isEmpty())
            {
                emit SendDataSignal(i);
            }
            MySleep(m_SerialPortCycleDatas[i].delay);
            MySleep(m_nDelay);
            //qDebug() << "continue:" << m_bContinue;
            if (!m_bContinue)
            {
                return ;
            }
        }
    }
}

void SerialPortCycle::StartCycle(const QList<CycleData> SerialPortCycleDatas, const unsigned int delay)
{
    SetCycleData(SerialPortCycleDatas, delay);
    Start();
}
