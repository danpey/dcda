<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="DanpeCommDubugAssist_zh_CN">
<context>
    <name>About</name>
    <message>
        <location filename="Other/about.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="108"/>
        <source>Open Source address：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="132"/>
        <source>More About：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="174"/>
        <source>&lt;style&gt; a {text-decoration: none} &lt;/style&gt;&lt;a href=&quot;mailto:danpe@lijianxun.cn&quot;&gt;danpe@lijianxun.cn&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="198"/>
        <source>Based on Qt 5.12.6 (Qt Creator 4.10.2, 32bit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="220"/>
        <source>蛋皮通信调试助手</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="235"/>
        <source>Copyright © 2020 Danpe. All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="159"/>
        <source>Email：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="120"/>
        <source>Build on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="147"/>
        <source>&lt;style&gt; a {text-decoration: none} &lt;/style&gt;  &lt;a href=https://gitee.com/danpe/DanpeCommDubugAssist&gt;https://gitee.com/danpe/DanpeCommDubugAssist&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="186"/>
        <source>&lt;style&gt; a {text-decoration: none} &lt;/style&gt;  &lt;a href=https://lijianxun.cn/dcda&gt;https://lijianxun.cn/dcda/&lt;/a&gt;</source>
        <oldsource>&lt;style&gt; a {text-decoration: none} &lt;/style&gt;  &lt;a href=https://lijianxun.cn/2020/04/22/248&gt;https://lijianxun.cn/2020/04/22/248&lt;/a&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="60"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="63"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/about.ui" line="66"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Clothes</name>
    <message>
        <location filename="Main/clothes.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/clothes.ui" line="20"/>
        <source>更多皮肤，敬请期待！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/clothes.ui" line="41"/>
        <source>暗橙空间</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/clothes.ui" line="94"/>
        <source>淡蓝天空</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/clothes.ui" line="161"/>
        <source>共2款皮肤，快来试试吧！</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommDubugAssist</name>
    <message>
        <location filename="Main/commdubugassist.ui" line="20"/>
        <source>CommDubugAssist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="35"/>
        <source>串口调试</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="43"/>
        <source>串口号：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="57"/>
        <source>停止位：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="64"/>
        <source>校验位：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="74"/>
        <location filename="Main/commdubugassist.ui" line="78"/>
        <source>1200</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="83"/>
        <source>2400</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="88"/>
        <source>4800</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="93"/>
        <source>9600</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="98"/>
        <source>19200</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="103"/>
        <source>38400</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="108"/>
        <source>57600</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="113"/>
        <source>115200</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="121"/>
        <location filename="Main/commdubugassist.ui" line="125"/>
        <source>无流控 (No Flow)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="130"/>
        <source>硬件流控 (HardWare Flow)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="135"/>
        <source>软件流控 (Software Flow)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="143"/>
        <source>流  控：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="153"/>
        <location filename="Main/commdubugassist.ui" line="157"/>
        <source>5 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="162"/>
        <source>6 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="167"/>
        <source>7 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="172"/>
        <source>8 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="182"/>
        <location filename="Main/commdubugassist.ui" line="1168"/>
        <location filename="Main/commdubugassist.ui" line="1855"/>
        <source>发送：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="189"/>
        <location filename="Main/commdubugassist.ui" line="1175"/>
        <location filename="Main/commdubugassist.ui" line="1862"/>
        <source>接收：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="196"/>
        <location filename="Main/commdubugassist.ui" line="203"/>
        <location filename="Main/commdubugassist.ui" line="1182"/>
        <location filename="Main/commdubugassist.ui" line="1189"/>
        <location filename="Main/commdubugassist.ui" line="1869"/>
        <location filename="Main/commdubugassist.ui" line="1876"/>
        <source>Tel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="250"/>
        <location filename="Main/commdubugassist.ui" line="967"/>
        <location filename="Main/commdubugassist.ui" line="1674"/>
        <source>发送与循环设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="258"/>
        <location filename="Main/commdubugassist.ui" line="1038"/>
        <location filename="Main/commdubugassist.ui" line="1682"/>
        <source>循环发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="297"/>
        <location filename="Main/commdubugassist.ui" line="1077"/>
        <location filename="Main/commdubugassist.ui" line="1721"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="308"/>
        <location filename="Main/commdubugassist.ui" line="391"/>
        <location filename="Main/commdubugassist.ui" line="1002"/>
        <location filename="Main/commdubugassist.ui" line="1149"/>
        <location filename="Main/commdubugassist.ui" line="1757"/>
        <location filename="Main/commdubugassist.ui" line="1836"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="315"/>
        <location filename="Main/commdubugassist.ui" line="1009"/>
        <location filename="Main/commdubugassist.ui" line="1764"/>
        <source>自动添加换行</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="333"/>
        <location filename="Main/commdubugassist.ui" line="1027"/>
        <location filename="Main/commdubugassist.ui" line="1746"/>
        <source>循环发送转Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="340"/>
        <location filename="Main/commdubugassist.ui" line="1020"/>
        <location filename="Main/commdubugassist.ui" line="1739"/>
        <source>循环发送内容：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="364"/>
        <location filename="Main/commdubugassist.ui" line="1101"/>
        <location filename="Main/commdubugassist.ui" line="1788"/>
        <source>接收与显示设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="370"/>
        <location filename="Main/commdubugassist.ui" line="1114"/>
        <location filename="Main/commdubugassist.ui" line="1801"/>
        <source>记录发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="377"/>
        <location filename="Main/commdubugassist.ui" line="1107"/>
        <location filename="Main/commdubugassist.ui" line="1794"/>
        <source>记录时间</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="384"/>
        <location filename="Main/commdubugassist.ui" line="1128"/>
        <location filename="Main/commdubugassist.ui" line="1815"/>
        <source>暂停显示</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="398"/>
        <location filename="Main/commdubugassist.ui" line="1142"/>
        <location filename="Main/commdubugassist.ui" line="1829"/>
        <source>自动换行</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="419"/>
        <location filename="Main/commdubugassist.ui" line="1156"/>
        <location filename="Main/commdubugassist.ui" line="1843"/>
        <source>自动滚屏</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="405"/>
        <location filename="Main/commdubugassist.ui" line="1135"/>
        <location filename="Main/commdubugassist.ui" line="1822"/>
        <source>保存接收</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="412"/>
        <location filename="Main/commdubugassist.ui" line="1121"/>
        <location filename="Main/commdubugassist.ui" line="1808"/>
        <source>清空接收</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="429"/>
        <source>数据位：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="436"/>
        <source>波特率：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="447"/>
        <source>无校验位 (NONE)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="452"/>
        <source>奇校验位 (ODD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="457"/>
        <source>偶校验位 (EVEN)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="462"/>
        <source>MARK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="467"/>
        <source>SPACE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="479"/>
        <source>1 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="484"/>
        <source>1.5 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="489"/>
        <source>2 位</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="506"/>
        <location filename="Main/commdubugassist.ui" line="1601"/>
        <source>连接</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="567"/>
        <location filename="Main/commdubugassist.ui" line="1239"/>
        <location filename="Main/commdubugassist.ui" line="1929"/>
        <source>发送区</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="576"/>
        <location filename="Main/commdubugassist.ui" line="1282"/>
        <location filename="Main/commdubugassist.ui" line="1938"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Shift+Backspace&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="579"/>
        <location filename="Main/commdubugassist.ui" line="1285"/>
        <location filename="Main/commdubugassist.ui" line="1941"/>
        <source>清空</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="599"/>
        <location filename="Main/commdubugassist.ui" line="1245"/>
        <location filename="Main/commdubugassist.ui" line="1961"/>
        <source>发送后清空</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="619"/>
        <location filename="Main/commdubugassist.ui" line="1272"/>
        <location filename="Main/commdubugassist.ui" line="1981"/>
        <source>回车键发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="639"/>
        <location filename="Main/commdubugassist.ui" line="1252"/>
        <location filename="Main/commdubugassist.ui" line="2001"/>
        <source>发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="647"/>
        <location filename="Main/commdubugassist.ui" line="1342"/>
        <location filename="Main/commdubugassist.ui" line="2009"/>
        <source>快捷发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="656"/>
        <location filename="Main/commdubugassist.ui" line="720"/>
        <location filename="Main/commdubugassist.ui" line="1351"/>
        <location filename="Main/commdubugassist.ui" line="1418"/>
        <location filename="Main/commdubugassist.ui" line="2018"/>
        <location filename="Main/commdubugassist.ui" line="2085"/>
        <source>添加</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="663"/>
        <location filename="Main/commdubugassist.ui" line="740"/>
        <location filename="Main/commdubugassist.ui" line="1358"/>
        <location filename="Main/commdubugassist.ui" line="1438"/>
        <location filename="Main/commdubugassist.ui" line="2025"/>
        <location filename="Main/commdubugassist.ui" line="2105"/>
        <source>修改</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="683"/>
        <location filename="Main/commdubugassist.ui" line="1378"/>
        <location filename="Main/commdubugassist.ui" line="2045"/>
        <source>双击某行快捷发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="703"/>
        <location filename="Main/commdubugassist.ui" line="760"/>
        <location filename="Main/commdubugassist.ui" line="1401"/>
        <location filename="Main/commdubugassist.ui" line="1458"/>
        <location filename="Main/commdubugassist.ui" line="2068"/>
        <location filename="Main/commdubugassist.ui" line="2125"/>
        <source>删除</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="711"/>
        <location filename="Main/commdubugassist.ui" line="1409"/>
        <location filename="Main/commdubugassist.ui" line="2076"/>
        <source>互动发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="768"/>
        <location filename="Main/commdubugassist.ui" line="1466"/>
        <location filename="Main/commdubugassist.ui" line="2133"/>
        <source>高级循环发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="777"/>
        <location filename="Main/commdubugassist.ui" line="1475"/>
        <location filename="Main/commdubugassist.ui" line="2142"/>
        <source>开始循环</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="784"/>
        <location filename="Main/commdubugassist.ui" line="1482"/>
        <location filename="Main/commdubugassist.ui" line="2149"/>
        <source>当前无执行事件</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="804"/>
        <location filename="Main/commdubugassist.ui" line="1502"/>
        <location filename="Main/commdubugassist.ui" line="2169"/>
        <source>编辑事件</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="812"/>
        <location filename="Main/commdubugassist.ui" line="1510"/>
        <location filename="Main/commdubugassist.ui" line="2177"/>
        <source>发送记录</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="821"/>
        <location filename="Main/commdubugassist.ui" line="1519"/>
        <location filename="Main/commdubugassist.ui" line="2186"/>
        <source>清空记录</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="841"/>
        <location filename="Main/commdubugassist.ui" line="1539"/>
        <location filename="Main/commdubugassist.ui" line="2206"/>
        <source>双击发送记录即可快速发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="861"/>
        <location filename="Main/commdubugassist.ui" line="1559"/>
        <location filename="Main/commdubugassist.ui" line="2226"/>
        <source>开启记录</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="877"/>
        <source>TCP Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="887"/>
        <location filename="Main/commdubugassist.ui" line="1594"/>
        <source>3620</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="894"/>
        <source>开启监听</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="930"/>
        <source>端口号：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="940"/>
        <source>本地IP：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="975"/>
        <source>发送对象</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="983"/>
        <location filename="Main/commdubugassist.ui" line="1305"/>
        <location filename="Main/commdubugassist.ui" line="1575"/>
        <source>NULL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="1294"/>
        <source>发送对象：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="1584"/>
        <source>TCP Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="1637"/>
        <source>服务器端口：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="1647"/>
        <source>服务器IP：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2255"/>
        <location filename="Main/commdubugassist.ui" line="2300"/>
        <source>帮助</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2264"/>
        <source>选项</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2276"/>
        <source>功能建议与报告bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2279"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2284"/>
        <source>关于</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2287"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2292"/>
        <source>首选项</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2295"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2303"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2308"/>
        <source>工具箱</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2311"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2316"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2319"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.ui" line="2324"/>
        <source>ceshi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.cpp" line="1444"/>
        <location filename="Main/commdubugassist.cpp" line="1445"/>
        <location filename="Main/commdubugassist.cpp" line="1446"/>
        <location filename="Main/commdubugassist.cpp" line="1486"/>
        <location filename="Main/commdubugassist.cpp" line="1573"/>
        <location filename="Main/commdubugassist.cpp" line="1574"/>
        <location filename="Main/commdubugassist.cpp" line="1575"/>
        <location filename="Main/commdubugassist.cpp" line="1576"/>
        <location filename="Main/commdubugassist.cpp" line="1577"/>
        <location filename="Main/commdubugassist.cpp" line="1668"/>
        <location filename="Main/commdubugassist.cpp" line="1669"/>
        <location filename="Main/commdubugassist.cpp" line="1684"/>
        <location filename="Main/commdubugassist.cpp" line="1685"/>
        <location filename="Main/commdubugassist.cpp" line="4396"/>
        <location filename="Main/commdubugassist.cpp" line="4397"/>
        <location filename="Main/commdubugassist.cpp" line="4398"/>
        <location filename="Main/commdubugassist.cpp" line="4399"/>
        <location filename="Main/commdubugassist.cpp" line="4444"/>
        <location filename="Main/commdubugassist.cpp" line="4445"/>
        <location filename="Main/commdubugassist.cpp" line="4603"/>
        <location filename="Main/commdubugassist.cpp" line="4604"/>
        <location filename="Main/commdubugassist.cpp" line="4605"/>
        <location filename="Main/commdubugassist.cpp" line="4606"/>
        <location filename="Main/commdubugassist.cpp" line="4607"/>
        <location filename="Main/commdubugassist.cpp" line="4865"/>
        <location filename="Main/commdubugassist.cpp" line="4866"/>
        <location filename="Main/commdubugassist.cpp" line="4875"/>
        <location filename="Main/commdubugassist.cpp" line="4876"/>
        <location filename="Main/commdubugassist.cpp" line="5384"/>
        <location filename="Main/commdubugassist.cpp" line="5385"/>
        <location filename="Main/commdubugassist.cpp" line="5386"/>
        <location filename="Main/commdubugassist.cpp" line="5426"/>
        <location filename="Main/commdubugassist.cpp" line="5579"/>
        <location filename="Main/commdubugassist.cpp" line="5580"/>
        <location filename="Main/commdubugassist.cpp" line="5581"/>
        <location filename="Main/commdubugassist.cpp" line="5582"/>
        <location filename="Main/commdubugassist.cpp" line="5583"/>
        <location filename="Main/commdubugassist.cpp" line="5822"/>
        <location filename="Main/commdubugassist.cpp" line="5823"/>
        <location filename="Main/commdubugassist.cpp" line="5838"/>
        <location filename="Main/commdubugassist.cpp" line="5839"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.cpp" line="2331"/>
        <source>确认</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/commdubugassist.cpp" line="2332"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="Other/help.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="23"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="26"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="37"/>
        <source>帮助</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="46"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;1. 本软件支持串口、TCP服务端和TCP客户端。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;2. 本软件的高级功能中，每一种连接方式都支持快捷发送、互动发送和高级循环功能。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;3. 互动发送功能支持软件内部指令发送，具体内容如下：&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  指令内容：[alldata]   格式：Hex   意义：指收到的任意信息&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  该指令可以让收到的信息进行原文转发或者普遍回复，而不用在意原文的内容是否是普通文本格式还是Hex格式。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  后续会增加多个互动发送的指令功能。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;4. 本软件暂时未对多客户端的数据做过多优化，仅做了有限优化，具体问题方式表现为：比如普通场景下连接客户端数量为100个之后，内部的数据处理扩充到100个，但后续如果只有10个左右客户端连接，那么内部依然会保留100个客户端的位置供以连接。关闭服务器选择断开所有客户端后，可以重置程序数据。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;5. 发送栏中，在使用回车键进行快捷发送的时候，使用Ctrl+Enter组合便可以换行，反之，不使用回车键快捷发送时，用Ctrl+Enter组合便可快捷发送。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;6. 拖拽文件到界面即可导入文件内容到发送框，支持文件类型：txt,html,xml,log,php&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;7. TCP服务端和客户端均只支持IPv4，IPv6尚不兼容。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;8. 多开此程序后，除了第一个打开的程序，后续的所有多开程序均不支持保存设置。(多开程序仅对英文名的程序名有效。所以请勿更改程序名非英文！否则会导致多开检测失效。）&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;1. 本软件支持串口、TCP服务端和TCP客户端。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;2. 本软件的高级功能中，每一种连接方式都支持快捷发送、互动发送和高级循环功能。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;3. 互动发送功能支持软件内部指令发送，具体内容如下：&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  指令内容：[alldata]   格式：Hex   意义：指收到的任意信息&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  该指令可以让收到的信息进行原文转发或者普遍回复，而不用在意原文的内容是否是普通文本格式还是Hex格式。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  后续会增加多个互动发送的指令功能。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;4. 本软件暂时未对多客户端的数据做过多优化，仅做了有限优化，具体问题方式表现为：比如普通场景下连接客户端数量为100个之后，内部的数据处理扩充到100个，但后续如果只有10个左右客户端连接，那么内部依然会保留100个客户端的位置供以连接。关闭服务器选择断开所有客户端后，可以重置程序数据。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;5. 发送栏中，在使用回车键进行快捷发送的时候，使用Ctrl+Enter组合便可以换行，反之，不使用回车键快捷发送时，用Ctrl+Enter组合便可快捷发送。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;6. 拖拽文件到界面即可导入文件内容到发送框，支持文件类型：txt,html,xml,log,php&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;7. TCP服务端和客户端均只支持IPv4，IPv6尚不兼容。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;8. 多开此程序后，除了第一个打开的程序，后续的所有多开程序均不支持保存设置。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="68"/>
        <source>版本修订历史</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="77"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;Beta 1.0.1 (2020-6-2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  1. 更新帮助内容&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  2. 修改与优化关于界面&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  3. 添加软件LOGO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  4. 优化关闭软件的提示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  5. 将功能建议与报告bug的菜单指向网站连接&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  6. 优化版本号的显示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  7. 添加菜单快捷键&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  8. 增加拖拽外部文件到程序内即可导入文件内容到发送框&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  9. 增加了多个按钮的快捷键，并对可使用快捷键的按钮添加了快捷键提示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  10. 主界面所有表格增加了内容提示功能，便于在表格内容展示不完整时通过提示显示完整内容&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  11. 添加更换皮肤页面，新增两款皮肤&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  12. 为了配合新增的皮肤功能，统一界面的整体风格，删除了白底黑字的复选框功能&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  13. 添加主界面窗口置顶的功能&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  14. 添加了首选项设置，支持修改与保存多项设置&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  15. 支持导出和导入快捷发送和交互发送的功能&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  16. 优化软件部分操作&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  17. 添加软件双开提醒并支持多开软件&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  18. 新增软件自动检测更新的功能，并支持在线更新&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;SimSun&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;Beta 1.0.0 (2020-4-22)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  1. 正式发布测试版本&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;SimSun&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;Beta 1.0.1 (2020-)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  1. 更新帮助内容&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  2. 修改与优化关于界面&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  3. 添加软件LOGO&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  4. 优化关闭软件的提示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  5. 将功能建议与报告bug的菜单指向网站连接&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  6. 优化版本号的显示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  7. 添加菜单快捷键&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  8. 增加拖拽外部文件到程序内即可导入文件内容到发送框&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  9. 增加了多个按钮的快捷键，并对可使用快捷键的按钮添加了快捷键提示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  10. 主界面所有表格增加了内容提示功能，便于在表格内容展示不完整时通过提示显示完整内容&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  11. 添加更换皮肤页面，新增两款皮肤&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  12. 添加主界面窗口置顶的功能&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  13. 添加了首选项设置，支持修改与保存多项设置&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  14. 支持导出和导入快捷发送和交互发送的功能&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  15. 优化软件部分操作&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  16. 添加软件双开提醒并支持多开软件&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  17. 新增软件自动检测更新的功能，并支持在线更新&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  18. 添加了工具箱，增加多项实用工具&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;SimSun&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;Beta 1.0.0 (2020-4-22)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;SimSun&apos;;&quot;&gt;  1. 正式发布测试版本&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;SimSun&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="114"/>
        <source>更多疑问，欢迎发送邮件询问：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/help.ui" line="134"/>
        <source>&lt;style&gt; a {text-decoration: none} &lt;/style&gt;&lt;a href=&quot;mailto:danpe@lijianxun.cn&quot;&gt;danpe@lijianxun.cn&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InteractUnit</name>
    <message>
        <location filename="Other/interactunit.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="28"/>
        <source>等于</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="33"/>
        <source>包含</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="38"/>
        <source>首部</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="43"/>
        <source>尾部</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="61"/>
        <location filename="Other/interactunit.ui" line="115"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="68"/>
        <source>当接收到：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="82"/>
        <source>  则回应：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="126"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="129"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="132"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="152"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="155"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/interactunit.ui" line="158"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="Main/preferences.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2125"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2128"/>
        <location filename="Main/preferences.cpp" line="321"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2131"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="69"/>
        <source> 串口设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="91"/>
        <location filename="Main/preferences.ui" line="729"/>
        <location filename="Main/preferences.ui" line="1299"/>
        <source>请点开子项进行选择</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="321"/>
        <source>上次连接的串口：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="335"/>
        <source>启动程序自动连接上次连接的串口</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="290"/>
        <location filename="Main/preferences.ui" line="817"/>
        <location filename="Main/preferences.ui" line="1502"/>
        <source>关闭软件后数据自动清零</source>
        <oldsource>（超过此值将自动清除前一半内容，0表示无限制）</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="266"/>
        <location filename="Main/preferences.ui" line="872"/>
        <location filename="Main/preferences.ui" line="1454"/>
        <source>数据接收长度阈值：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="297"/>
        <location filename="Main/preferences.ui" line="824"/>
        <location filename="Main/preferences.ui" line="1509"/>
        <source>收发数据清零</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="192"/>
        <source> 串口基本设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="239"/>
        <location filename="Main/preferences.ui" line="892"/>
        <location filename="Main/preferences.ui" line="1481"/>
        <source>（超过此值将自动清除前四分之一内容，0表示无限制）</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="353"/>
        <location filename="Main/preferences.ui" line="910"/>
        <location filename="Main/preferences.ui" line="1540"/>
        <source>历史发送数据数量阈值：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="423"/>
        <source> 串口快捷发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="445"/>
        <location filename="Main/preferences.ui" line="1029"/>
        <location filename="Main/preferences.ui" line="1631"/>
        <source>导出快捷发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="452"/>
        <location filename="Main/preferences.ui" line="1036"/>
        <location filename="Main/preferences.ui" line="1638"/>
        <source>导入快捷发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="528"/>
        <location filename="Main/preferences.ui" line="1126"/>
        <location filename="Main/preferences.ui" line="1742"/>
        <source>导出交互发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="535"/>
        <location filename="Main/preferences.ui" line="1133"/>
        <location filename="Main/preferences.ui" line="1749"/>
        <source>导入交互发送</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="577"/>
        <source> 串口交互发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="617"/>
        <source> 串口高级循环发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="639"/>
        <location filename="Main/preferences.ui" line="1223"/>
        <location filename="Main/preferences.ui" line="1839"/>
        <source>打开高级循环编辑器</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="707"/>
        <source> TCP服务端设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="967"/>
        <source> TCP服务端基本设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="856"/>
        <source>启动程序自动开始监听</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1007"/>
        <source> TCP服务端快捷发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1104"/>
        <source> TCP服务端交互发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1201"/>
        <source> TCP服务端高级循环发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1341"/>
        <source> TCP客户端设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1412"/>
        <source>端口号：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1419"/>
        <source>上次连接服务端IP：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1433"/>
        <source>启动程序自动连接</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1381"/>
        <source> TCP客户端基本设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1680"/>
        <source> TCP客户端快捷发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1720"/>
        <source> TCP客户端交互发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1817"/>
        <source> TCP客户端高级循环发送设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1907"/>
        <source> 通用设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1992"/>
        <source>还原为原始默认设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1951"/>
        <source>提示：默认设置用于软件初始化的设置状态</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1931"/>
        <source>当前设置保存为默认设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="1958"/>
        <source>每次修改自动保存为默认设置</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2016"/>
        <source>检查更新</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2074"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2077"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2080"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2087"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+S&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2090"/>
        <source>应用</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.ui" line="2093"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Main/preferences.cpp" line="320"/>
        <source>确认</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickSendUnit</name>
    <message>
        <location filename="Other/quicksendunit.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="41"/>
        <source>发送内容：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="61"/>
        <source>名称：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="86"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="106"/>
        <source>末尾添加换行</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="130"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="133"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="136"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="156"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="159"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/quicksendunit.ui" line="162"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeniorSendCycle</name>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="42"/>
        <location filename="Other/seniorsendcycle.ui" line="246"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="49"/>
        <source>事件间隔：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="118"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="138"/>
        <source>发送内容为空则仅为延迟时间</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="182"/>
        <source>事件名称：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="198"/>
        <source> 发送内容：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="239"/>
        <source>事件延迟：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="278"/>
        <source>删除</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="291"/>
        <source>修改</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="304"/>
        <source>插入</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="317"/>
        <source>添加</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="324"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="327"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="330"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="343"/>
        <source>导出</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="356"/>
        <source>导入</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="363"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="366"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.ui" line="369"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.cpp" line="105"/>
        <source>确认</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.cpp" line="106"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/seniorsendcycle.cpp" line="167"/>
        <location filename="Other/seniorsendcycle.cpp" line="168"/>
        <location filename="Other/seniorsendcycle.cpp" line="183"/>
        <location filename="Other/seniorsendcycle.cpp" line="184"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TCPServerInteractUnit</name>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="35"/>
        <source>接收对象：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="46"/>
        <source>等于</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="51"/>
        <source>包含</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="56"/>
        <source>首部</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="61"/>
        <source>尾部</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="69"/>
        <source>当接收到：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="79"/>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="123"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="90"/>
        <source>  则回应：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="113"/>
        <source>发送对象：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="148"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="151"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="171"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="174"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverinteractunit.ui" line="177"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TCPServerQuickSendUnit</name>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="37"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="57"/>
        <source>末尾添加换行</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="105"/>
        <source>名称：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="118"/>
        <source>发送内容：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="128"/>
        <source>发送对象：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="143"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="146"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="166"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="169"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverquicksendunit.ui" line="172"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TCPServerSeniorSendCycle</name>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="42"/>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="221"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="49"/>
        <source>事件间隔：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="106"/>
        <source>发送对象：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="118"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="138"/>
        <source>发送内容为空则仅为延迟时间</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="214"/>
        <source>事件延迟：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="262"/>
        <source> 发送内容：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="278"/>
        <source>事件名称：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="304"/>
        <source>删除</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="317"/>
        <source>修改</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="330"/>
        <source>插入</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="343"/>
        <source>添加</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="350"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esc&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="353"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="356"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="369"/>
        <source>导出</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="382"/>
        <source>导入</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="389"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ctrl+Enter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="392"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.ui" line="395"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.cpp" line="102"/>
        <source>确认</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.cpp" line="103"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TCPServer/tcpserverseniorsendcycle.cpp" line="160"/>
        <location filename="TCPServer/tcpserverseniorsendcycle.cpp" line="161"/>
        <location filename="TCPServer/tcpserverseniorsendcycle.cpp" line="176"/>
        <location filename="TCPServer/tcpserverseniorsendcycle.cpp" line="177"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Upgra</name>
    <message>
        <location filename="Other/upgra.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/upgra.ui" line="122"/>
        <source>本次更新内容：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/upgra.ui" line="134"/>
        <source>版本</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/upgra.ui" line="146"/>
        <source>更新日期</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/upgra.ui" line="158"/>
        <source>  1. 更新帮助内容
  2. 修改与优化关于界面
  3. 添加软件LOGO
  4. 优化关闭软件的提示
  5. 将功能建议与报告bug的菜单指向网站连接
  6. 优化版本号的显示
  7. 添加菜单快捷键
  8. 增加拖拽外部文件到程序内即可导入文件内容到发送框
  9. 增加了多个按钮的快捷键，并对可使用快捷键的按钮添加了快捷键提示
  10. 主界面所有表格增加了内容提示功能，便于在表格内容展示不完整时通过提示显示完整内容
  11. 添加更换皮肤页面，新增两款皮肤
  12. 添加主界面窗口置顶的功能
  13. 添加了首选项设置，支持修改与保存多项设置
  14. 支持导出和导入快捷发送和交互发送的功能
  15. 优化软件部分操作
  16. 添加软件双开提醒并支持多开软件
  17. 新增软件自动检测更新的功能，并支持在线更新</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Other/upgra.ui" line="191"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
