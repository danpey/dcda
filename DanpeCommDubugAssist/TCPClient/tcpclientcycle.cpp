#include "tcpclientcycle.h"

TCPClientCycle::TCPClientCycle(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<CycleData>>("QList<CycleData>");
    m_bContinue = true;
}

void TCPClientCycle::SetCycleData(const QList<CycleData> CycleDatas, const unsigned int delay)
{
    m_TCPClientCycleDatas = CycleDatas;
    m_nDelay = delay;
}

void TCPClientCycle::SetContinue(bool contin)
{
    m_bContinue = contin;
}

void TCPClientCycle::Start()
{
    while(true)
    {
        if (m_TCPClientCycleDatas.isEmpty())
        {
            return ;
        }
        for (int i = 0; i < m_TCPClientCycleDatas.count(); i++)
        {
            if (!m_TCPClientCycleDatas[i].senddata.isEmpty())
            {
                emit SendDataSignal(i);
            }
            MySleep(m_TCPClientCycleDatas[i].delay);
            MySleep(m_nDelay);
            //qDebug() << "continue:" << m_bContinue;
            if (!m_bContinue)
            {
                return ;
            }
        }
    }
}

void TCPClientCycle::StartCycle(const QList<CycleData> CycleDatas, const unsigned int delay)
{
    SetCycleData(CycleDatas, delay);
    Start();
}

