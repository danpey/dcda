#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>
#include <QTcpSocket>

#include "Main/basecode.h"

class TCPClient : public QObject
{
    Q_OBJECT
public:
    explicit TCPClient(QObject *parent = nullptr);
    bool isOpen();
    bool isValid();
    bool isConnect();
    void Connect(QString ip, quint16 port);
    void Disconnected();
    void WriteData(QByteArray writedata);
    void SetCloseOver(bool closeover);

signals:
    void ConnectedSignal();
    void DisconnectedSignal();
    void ReadDataSignal(QByteArray readdata);

public slots:

private:
    QTcpSocket *m_tcpsocket;

    void TcpSocketConnects();
    void TcpSocketConnected();

    void TcpSocketReadyRead();

    bool m_bIsConnect;
    bool m_bDisconnect;
    bool m_bCloseOver;

};

#endif // TCPCLIENT_H
