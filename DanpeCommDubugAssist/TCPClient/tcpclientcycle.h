#ifndef TCPCLIENTCYCLE_H
#define TCPCLIENTCYCLE_H

#include <QObject>
#include <QMetaType>
#include "Main/basecode.h"

class TCPClientCycle : public QObject
{
    Q_OBJECT
public:
    explicit TCPClientCycle(QObject *parent = nullptr);

    void SetContinue(bool contin);

signals:
    void SendDataSignal(int num);
    //void SendOverSignal();

public slots:
    void SetCycleData(const QList<CycleData> CycleDatas, const unsigned int delay);   // 导入数据
    // 开始
    void StartCycle(const QList<CycleData> CycleDatas, const unsigned int delay);
    void Start();

private:
    bool m_bContinue = true;

    QList<CycleData> m_TCPClientCycleDatas;
    unsigned int m_nDelay;
};

#endif // TCPCLIENTCYCLE_H
