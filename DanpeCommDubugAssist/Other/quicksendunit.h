#ifndef QUICKSENDUNIT_H
#define QUICKSENDUNIT_H

#include <QWidget>
#include "Main/basecode.h"

namespace Ui {
class QuickSendUnit;
}

class QuickSendUnit : public QWidget
{
    Q_OBJECT

public:
    explicit QuickSendUnit(QWidget *parent = nullptr);
    ~QuickSendUnit();

    void clear();
    void setAdd(bool add);
    void inform(QString strdata);
    void setdata(QuickSendData sp);
    void setUInum(int uinum);

    void show();
    void InitQSS();

signals:
    void AddSignal(QuickSendData sp, int uinum);
    void ReviseSignal(QuickSendData sp, int uinum);
    void closeSignal();

private slots:
    void on_btn_ok_clicked();
    void on_btn_cancel_clicked();

private:
    Ui::QuickSendUnit *ui;

    void InitTitlepar();
    QuickSendData GetUIData();

    bool m_bAdd;
    int m_nUInum;
    WidgetPar *m_selMainWidget;
};

#endif // QUICKSENDUNIT_H
