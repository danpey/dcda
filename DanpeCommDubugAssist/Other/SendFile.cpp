#include "SendFile.h"

SendFile::SendFile()
{
    m_sendOver = false;
    m_sendState = false;
}

SendFile::~SendFile()
{

}

bool SendFile::initStart(unsigned char sendID, QString path)
{
    bool ret = false;

    if (path.isEmpty())
    {
        return ret;
    }
    else
    {
        m_sendID = sendID;
        m_path = path;
        m_sendOver = false;
        m_sendState = true;
        this->start();
        ret = true;
    }
    return ret;
}

///
/// \brief 暂停或者继续
/// \param state
/// \return
///
bool SendFile::paused(bool state)
{
    bool ret = false;
    if (state)  // 继续
    {
        m_sendState = true;
    }
    else	// 暂停
    {
        m_sendState = false;
    }
    ret = true;

    return ret;
}

bool SendFile::stop()
{
    bool ret = false;

    m_sendOver = true;

    ret = true;

    return ret;
}


void SendFile::run()
{
    m_sendFileName.clear();
    m_sendSize = 0;

    QFileInfo info(m_path);
    m_sendFileName = info.fileName();
    m_sendFileSize = info.size();

    m_sendfile.setFileName(m_path);
    if (m_sendfile.open(QIODevice::ReadOnly))
    {
        while (!m_sendOver)
        {
            if (m_sendState)
            {
                qint64 len = 0;
                char buf[1024] = {0};
                len = m_sendfile.read(buf, sizeof(buf));
                if (len > 0)
                {
                    emit returnSendData(m_sendID, buf, len);
                    msleep(30);
                    //qDebug() << "send len:" << len;
                }
                else
                {
                    m_sendOver = true;
                    //qDebug() << "send over";
                }
            }
        }
    }
    m_sendfile.close();
    this->exit();
}

