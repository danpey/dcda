#ifndef INTERACTUNIT_H
#define INTERACTUNIT_H

#include <QWidget>

#include "Main/basecode.h"

namespace Ui {
class InteractUnit;
}

class InteractUnit : public QWidget
{
    Q_OBJECT

public:
    explicit InteractUnit(QWidget *parent = nullptr);
    ~InteractUnit();

    void clear();
    void inform(QString strdata);
    void SetAdd(bool add);
    void SetData(InteractData si);
    void SetUInum(int uinum);

    void show();
    void InitQSS();

signals:
    void CloseSignal();
    void AddSignal(InteractData si, int uinum);
    void ReviseSignal(InteractData si, int uinum);

private slots:
    void on_btn_ok_clicked();
    void on_btn_close_clicked();

private:
    Ui::InteractUnit *ui;

    void InitTitlepar();

    bool m_bAdd;
    int m_nUInum;
    WidgetPar *m_selMainWidget;
};

#endif // INTERACTUNIT_H
