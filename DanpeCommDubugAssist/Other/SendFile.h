#ifndef SENDFILE_H
#define SENDFILE_H

#include <QThread>
#include <QObject>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>

#include <QDebug>

class SendFile : public QThread
{
    Q_OBJECT
public:
    SendFile();
    virtual ~SendFile();

    bool initStart(unsigned char sendID, QString path);
    bool paused(bool state);
    bool stop();

    void run();
signals:
    bool returnSendData(unsigned char sendID, char* buf, qint64 size);

private:
    unsigned char m_sendID;

    bool m_sendState;
    bool m_sendOver;

    QFile m_sendfile;
    QString m_path;
    QString m_sendFileName;
    qint64 m_sendFileSize;
    qint64 m_sendSize;
};



#endif // SENDFILE_H

