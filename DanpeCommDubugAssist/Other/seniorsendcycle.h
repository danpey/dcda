#ifndef SENIORSENDCYCLE_H
#define SENIORSENDCYCLE_H

#include <QWidget>
#include <QStandardItemModel>
#include <QCloseEvent>
#include <QFile>
#include <QFileDialog>
#include <QDomDocument>

#include "Main/basecode.h"

namespace Ui {
class SeniorSendCycle;
}

class SeniorSendCycle : public QWidget
{
    Q_OBJECT

public:
    explicit SeniorSendCycle(QWidget *parent = nullptr);
    ~SeniorSendCycle();

    void inform(QString strdata);
    void SetData(QList<CycleData> CycleDatas, int data);   // 设置
    void SetUInum(int uinum);

    void show();
    void InitQSS();

signals:
    void CloseSignal();    // 关闭
    void FinishSignal(QList<CycleData> CycleDatas, unsigned int delay, int uinum);        // 完成
    void JudgeDataSignal(bool &sameok, bool &overstate, QList<CycleData> CycleDatas, int delay, int uinum);   // 判断是否一致

private slots:
    void closeEvent(QCloseEvent *event);
    void on_btn_close_clicked();
    void on_btn_add_clicked();
    void on_btn_insert_clicked();
    void on_btn_revise_clicked();
    void on_btn_delete_clicked();
    void on_tableView_ShowEvents_clicked(const QModelIndex &index);
    void on_btn_ok_clicked();
    void on_btn_import_clicked();
    void on_btn_derive_clicked();

private:
    Ui::SeniorSendCycle *ui;

    void ShowEventsInit();
    void ShowEventsAdd(CycleData sc);
    void ShowEventsInsert(int row, CycleData sc);
    void ShowEventsRevise(int row, CycleData sc);
    void ShowEventsRemove(int row);
    void ShowEventsClear();
    bool SaveXml(QString path);
    //bool SaveXml();
    bool ReadXml(QString path);
    //bool ReadXml();

    void InitTitlepar();

    WidgetPar *m_selMainWidget;

    int m_nUInum;

    QStandardItemModel *m_staCycle;
    int m_CycleChoose;
    QList<CycleData> m_CycleDatas;
};

#endif // SENIORSENDCYCLE_H
