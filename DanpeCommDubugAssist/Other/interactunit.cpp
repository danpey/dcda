#include "interactunit.h"
#include "ui_interactunit.h"

InteractUnit::InteractUnit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InteractUnit)
{
    ui->setupUi(this);

    InitTitlepar();

    this->setWindowTitle("收发互动事件属性");

    ui->comboBox_SendType->setView(new QListView(this));
    ui->comboBox_mode->setView(new QListView(this));

    m_nUInum = 0;
}

InteractUnit::~InteractUnit()
{
    delete ui;
}

void InteractUnit::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    TitleBar *pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showMinmizeBtn(false);
    pTitleBar->showMaxmizeBtn(false);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &InteractUnit::closeEvent);
    m_selMainWidget->setWidgetResizable(false);  // 设置窗体不可缩放
    m_selMainWidget->setWindowModality(Qt::ApplicationModal);

    m_selMainWidget->setWindowTitle(QString("收发互动事件属性"));
    this->setWindowTitle(QString("收发互动事件属性"));
    setWindowIcon(QIcon(":/image/DCDA.ico"));
}

void InteractUnit::InitQSS()
{
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();
}

void InteractUnit::show()
{
    m_selMainWidget->show();
}

void InteractUnit::clear()
{
    ui->comboBox_mode->setCurrentIndex(0);
    ui->textEdit_Read->clear();
    ui->checkBox_ReadHex->setChecked(false);
    ui->textEdit_Write->clear();
    //ui->checkBox_WriteHex->setChecked(false);
    ui->comboBox_SendType->setCurrentText("Local");
}

void InteractUnit::SetAdd(bool add)
{
    m_bAdd = add;
}

void InteractUnit::SetData(InteractData si)
{
    ui->comboBox_mode->setCurrentIndex(si.mode);
    ui->textEdit_Read->setText(si.readdata);
    ui->textEdit_Write->setText(si.writedata);
    bool readhex = false;
    if (si.readTextFormat == HEX)
    {
        readhex = true;
    }
    ui->checkBox_ReadHex->setChecked(readhex);
    QString sendtypestr;
    switch (si.sendTextFormat) {
    case Local: sendtypestr = Local; break;
    case GBK: sendtypestr = GBK; break;
    case UTF8: sendtypestr = UTF8; break;
    case Unicode: sendtypestr = Unicode; break;
    case HEX: sendtypestr = HEX; break;
    }
    ui->comboBox_SendType->setCurrentText(sendtypestr);
}

void InteractUnit::SetUInum(int uinum)
{
    m_nUInum = uinum;
}

///
/// \brief 确定
///
void InteractUnit::on_btn_ok_clicked()
{
    InteractData si;
    si.mode = ui->comboBox_mode->currentIndex();
    si.modename = ui->comboBox_mode->currentText();
    si.readdata = ui->textEdit_Read->toPlainText();
    si.writedata = ui->textEdit_Write->toPlainText();
    if (ui->checkBox_ReadHex->isChecked())
    {
        si.readTextFormat = HEX;
    }
    else {
        si.readTextFormat = Local;
    }
    si.sendTextFormat = (TextFormat)ui->comboBox_SendType->currentIndex();
    if (si.readdata.isEmpty())
    {
        inform("请将填写读取信息！");
    }
    else if (si.writedata.isEmpty())
    {
        inform("请填写发送信息！");
    }
    else
    {
        if (si.readTextFormat == HEX)
        {
            if (si.readdata != "[alldata]")
            {
                if (!toHex(si.readdata))
                {
                    inform("读取数据不是Hex数据！请重新填写！");
                    return ;
                }
            }
        }
        if (si.sendTextFormat == HEX)
        {
            if (si.writedata != "[alldata]")
            {
                if (!toHex(si.writedata))
                {
                    inform("读取数据不是Hex数据！请重新填写！");
                    return ;
                }
            }
        }
        if (m_bAdd)
        {
            emit AddSignal(si, m_nUInum);
        }
        else
        {
            emit ReviseSignal(si, m_nUInum);
        }
        m_selMainWidget->close();
    }
}

///
/// \brief 关闭
///
void InteractUnit::on_btn_close_clicked()
{
    m_selMainWidget->close();
}

///
/// \brief 弹窗通知
/// \param strdata
///
void InteractUnit::inform(QString strdata)
{
    QMessageBox m_r;

    WidgetPar *wid;
    wid = new WidgetPar;
    wid->setWindowFlags(Qt::FramelessWindowHint);
    wid->setWindowModality(Qt::ApplicationModal);
    m_r.setParent(wid);
    TitleBar *titbar = new TitleBar(wid);

    titbar->showMinmizeBtn(false);
    titbar->showMaxmizeBtn(false);
    titbar->EnableCloseBtn(false);

    m_r.installEventFilter(titbar);
    QGridLayout *layout = new QGridLayout();

    layout->addWidget(titbar);  //添加标题栏
    layout->addWidget(&m_r);       //添加UI界面
    layout->setSpacing(0);         //布局之间的距离
    layout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    wid->setLayout(layout);  //将这个布局器设置在QWidget上
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);

    wid->setWidgetResizable(false);

    wid->move(m_selMainWidget->x() + (m_selMainWidget->width() - 150 - 3*strdata.length())/2, m_selMainWidget->y() + (m_selMainWidget->height() - (titbar->height() + 50))/2);
    wid->setTitleHeight(static_cast<uint>(titbar->height()));

    m_r.setWindowTitle("提示");
    m_r.setWindowIcon(QIcon(":/image/DCDA.ico"));
    m_r.setText(strdata);

    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        wid->setStyleSheet(stylesheet);
    }
    file.close();

    wid->show();
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);
    m_r.exec();
    wid->close();
    titbar->deleteLater();
    layout->deleteLater();
    wid->deleteLater();
    titbar = nullptr;
    layout = nullptr;
    wid = nullptr;
}
