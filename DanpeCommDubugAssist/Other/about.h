#ifndef ABOUT_H
#define ABOUT_H

#include <QWidget>
#include <QDate>
#include "Main/basecode.h"

namespace Ui {
class About;
}

class About : public QWidget
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = nullptr);
    ~About();

    void show();
    void close();
    void InitQSS();

signals:
    void CloseSignal();

private slots:
    void on_btn_close_clicked();

private:
    Ui::About *ui;

    void InitTitlepar();

    WidgetPar *m_selMainWidget;
};

#endif // ABOUT_H
