#ifndef HELP_H
#define HELP_H

#include <QWidget>
#include "Main/basecode.h"

namespace Ui {
class Help;
}

class Help : public QWidget
{
    Q_OBJECT

public:
    explicit Help(QWidget *parent = nullptr);
    ~Help();

    void show();
    void close();
    void InitQSS();

signals:
    void CloseSignal();

private slots:
    void on_btn_close_clicked();

private:
    Ui::Help *ui;

    void InitTitlepar();

    WidgetPar *m_selMainWidget;
};

#endif // HELP_H
