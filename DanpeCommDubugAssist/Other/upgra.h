#ifndef UPGRADE_H
#define UPGRADE_H

#include <QWidget>

#include "Main/basecode.h"

namespace Ui {
class Upgra;
}

class Upgra : public QWidget
{
    Q_OBJECT

public:
    explicit Upgra(QWidget *parent = nullptr);
    ~Upgra();

    void InitQSS();
    void show();

private slots:
    void on_btn_close_clicked();

private:
    Ui::Upgra *ui;

    void InitTitlepar();

    WidgetPar *m_selMainWidget;
};

#endif // UPGRADE_H
