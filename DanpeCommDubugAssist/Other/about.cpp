#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);

    InitTitlepar();

    ui->label_OpenSourceAddress->setOpenExternalLinks(true);
    ui->label_Domain->setOpenExternalLinks(true);
    ui->label_Email->setOpenExternalLinks(true);

    ui->label_name->setText(QString("蛋皮通信调试助手 ") + VERSION);

    QString datestr = QString::fromUtf8(__DATE__);
    QString timestr = QString::fromUtf8(__TIME__);
    ui->label_build->setText("Build on " + datestr + " " + timestr);
    ui->label_based->setText("Based on Qt " QT_VERSION_STR " (Qt Creator 4.12.4, 32bit)");
}

About::~About()
{
    delete ui;
}

void About::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    TitleBar *pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showMinmizeBtn(false);
    pTitleBar->showMaxmizeBtn(false);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &About::closeEvent);

    m_selMainWidget->setWindowTitle(QString("关于"));
    this->setWindowTitle(QString("关于"));
    setWindowIcon(QIcon(":/image/DCDA.ico"));
}

void About::InitQSS()
{
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();
}

void About::show()
{
    m_selMainWidget->show();
}

void About::close()
{
    m_selMainWidget->close();
}

///
/// \brief 关闭
///
void About::on_btn_close_clicked()
{
    m_selMainWidget->close();
    //emit CloseSignal();
}
