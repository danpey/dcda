#include "upgra.h"
#include "ui_upgra.h"

Upgra::Upgra(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Upgra)
{
    ui->setupUi(this);

    InitTitlepar();

    ui->label_ver->setText(VERSION);
    ui->label_date->setText("更新时间：2020年6月2日");
}

Upgra::~Upgra()
{
    delete ui;
}

void Upgra::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    TitleBar *pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showMinmizeBtn(false);
    pTitleBar->showMaxmizeBtn(false);
    pTitleBar->EnableCloseBtn(false);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));
    m_selMainWidget->setWidgetResizable(false);

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &Upgra::closeEvent);

    m_selMainWidget->setWindowModality(Qt::ApplicationModal);
    m_selMainWidget->setWindowTitle(QString("更新内容"));
    this->setWindowTitle(QString("更新内容"));
    setWindowIcon(QIcon(":/image/DCDA.ico"));
}

void Upgra::show()
{
    m_selMainWidget->show();
}

void Upgra::InitQSS()
{
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();
}

///
/// \brief 关闭
///
void Upgra::on_btn_close_clicked()
{
    m_selMainWidget->close();
}
