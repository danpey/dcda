#ifndef COMMDUBUGASSIST_H
#define COMMDUBUGASSIST_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QSerialPort>
#include <QStringList>
#include <QTextCursor>
#include <QTimer>
#include <QFileDialog>
#include <QKeyEvent>
#include <QFocusEvent>
#include <QTimer>
#include <QDateTime>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QCloseEvent>
#include <QFile>
#include <QThread>
#include <QRegExpValidator>
#include <QRegExp>
#include <QTcpSocket>
#include <QMimeData>
#include <QDropEvent>
#include <QModelIndex>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QUrl>

#include "basecode.h"
#include "Other/quicksendunit.h"
#include "Other/interactunit.h"
#include "Other/seniorsendcycle.h"
#include "Other/QReplyTimeout.h"
#include "SerialPort/serialportcycle.h"
#include "TCPServer/tcpserver.h"
#include "TCPServer/tcpserverquicksendunit.h"
#include "TCPServer/tcpserverinteractunit.h"
#include "TCPServer/tcpserverseniorsendcycle.h"
#include "TCPServer/tcpservercycle.h"
#include "TCPClient/tcpclient.h"
#include "TCPClient/tcpclientcycle.h"
#include "Other/help.h"
#include "Other/about.h"
#include "preferences.h"
#include "clothes.h"
#include "Other/upgra.h"
#include "Other/SendFile.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CommDubugAssist; }
QT_END_NAMESPACE

class CommDubugAssist : public QMainWindow
{
    Q_OBJECT

public:
    CommDubugAssist(QWidget *parent = nullptr);
    ~CommDubugAssist();

    void Inform(QString strdata);
    bool AskInform(QString strdata);

signals:
    void SerialPortStartCycle(const QList<CycleData> CycleDatas, const unsigned int delay);
    void TCPClientStartCycle(const QList<CycleData> CycleDatas, const unsigned int delay);
    void TCPServerStartCycle(const QList<TCPServerCycleData> CycleDatas, const unsigned int delay);
    //void entered(const QModelIndex &);

private slots:
    void keyPressEvent(QKeyEvent *event);
    void SerialPortReadData();
    void MyTimerEvent();
    void SerialPortSendCycle();
    bool eventFilter(QObject *target, QEvent *event);
    void on_action_Close_triggered();
    void closeEvent(QCloseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void ForceExit();

    void OpenClothes();
    void SetQSSes();

    void ResetSettings();

    void SerialPortQuickSendMoveEntered(const QModelIndex & index);
    void SerialPortInteractMoveEntered(const QModelIndex & index);
    void SerialPortSeniorSendCycleMoveEntered(const QModelIndex & index);
    void SerialPortHistoryMoveEntered(const QModelIndex & index);

    void TCPServerQuickSendMoveEntered(const QModelIndex & index);
    void TCPServerInteractMoveEntered(const QModelIndex & index);
    void TCPServerSeniorSendCycleMoveEntered(const QModelIndex & index);
    void TCPServerHistoryMoveEntered(const QModelIndex & index);

    void TCPClientQuickSendMoveEntered(const QModelIndex & index);
    void TCPClientInteractMoveEntered(const QModelIndex & index);
    void TCPClientSeniorSendCycleMoveEntered(const QModelIndex & index);
    void TCPClientHistoryMoveEntered(const QModelIndex & index);

    void SerialPortQuickSendDerive(QString path);
    void SerialPortQuickSendInduct(QString path);
    void SerialPortInteractDerive(QString path);
    void SerialPortInteractInduct(QString path);

    void TCPServerQuickSendDerive(QString path);
    void TCPServerQuickSendInduct(QString path);
    void TCPServerInteractDerive(QString path);
    void TCPServerInteractInduct(QString path);

    void TCPClientQuickSendDerive(QString path);
    void TCPClientQuickSendInduct(QString path);
    void TCPClientInteractDerive(QString path);
    void TCPClientInteractInduct(QString path);

    void SendFileGetData(unsigned char sendID, char *buf, qint64 len);

//    void textEdit_SerialPortSend_dragEnterEvent(QDragEnterEvent *event);
//    void textEdit_SerialPortSend_dropEvent(QDropEvent *event);

    void Developing();
    void Bug();

    void QS_Add(QuickSendData sp, int uinum);
    void QS_Revise(QuickSendData sp, int uinum);
    void QS_close();

    void TCPS_QS_Add(TCPServerQuickSendData sp);
    void TCPS_QS_Revise(TCPServerQuickSendData sp);
    void TCPS_QS_close();

    void Inter_Add(InteractData si, int uinum);
    void Inter_Revise(InteractData si, int uinum);
    void Inter_close();

    void TCPS_Inter_Add(TCPServerInteractData si);
    void TCPS_Inter_Revise(TCPServerInteractData si);
    void TCPS_Inter_close();

    void SSC_Finish(QList<CycleData> CycleDatas, unsigned int delay, int uinum);
    void SSC_JudgeData(bool &sameok, bool &overstate, QList<CycleData> CycleDatas, int delay, int uinum);
    void SSC_close();

    void TCPS_SSC_Finish(QList<TCPServerCycleData> CycleDatas, unsigned int delay);
    void TCPS_SSC_JudgeData(bool &sameok, bool &overstate, QList<TCPServerCycleData> CycleDatas, int delay);
    void TCPS_SSC_close();

    void HelpShow();
    void HelpClose();

    void AboutShow();
    void AboutClose();

    void PrefShow();
    void PrefClose();
    void PrefReadSendNumsUpdate();

    void checkUpdateShow();

    void SerialPortSeniorCycleSendData(int num);
    void SerialPortSendData(QString senddata, bool isHex);

    void TCPServerNewConncetion(QString ip, quint16 port);
    void TCPServerDisConnection(QString ip, quint16 port);
    void TCPServerReadData(QString ip, quint16 port, QByteArray readdata);
    void TCPServerSendCycle();
    void TCPServerSeniorCycleSendData(int num);
    void TCPServerSendData(QString ip, quint16 port, QString senddata, bool isHex);

    void TCPClientConnection();
    void TCPClientDisconnection();
    void TCPClientReaddata(QByteArray readdata);
    void TCPClientSendCycle();
    void TCPClientSeniorCycleSendData(int num);
    void TCPClientSendData(QString senddata, bool isHex);

    void on_btn_SerialPortConnect_clicked();
    void on_btn_SerialPortSend_clicked();
    void on_btn_SerialPortReadClear_clicked();
    void on_btn_SerialPortSendClear_clicked();
    void on_btn_SerialPortClearSend_clicked();
    void on_tableView_SerialPortSendHistory_doubleClicked(const QModelIndex &index);
    void on_btn_SerialPortReadSave_clicked();
    void on_btn_SerialPortQuickSendAdd_clicked();
    void on_btn_SerialPortQuickSendRevise_clicked();
    void on_btn_SerialPortQuickSendDelete_clicked();
    void on_tableView_SerialPortQuickSend_doubleClicked(const QModelIndex &index);
    void on_tableView_SerialPortQuickSend_clicked(const QModelIndex &index);
    void on_btn_SerialPortInteractAdd_clicked();
    void on_btn_SerialPortInteractRevise_clicked();
    void on_tableView_SerialPortInteract_clicked(const QModelIndex &index);
    void on_btn_SerialPortInteractDelete_clicked();
    void on_tableView_SerialPortInteract_doubleClicked(const QModelIndex &index);
    void on_btn_SerialPortSeniorSendCycleStart_clicked();
    void on_btn_SerialPortSeniorSendCycleEditor_clicked();

    void on_btn_TCPServerStartListening_clicked();
    void on_btn_TCPServerSend_clicked();
    void on_btn_TCPServerReadClear_clicked();
    void on_btn_TCPServerReadSave_clicked();
    void on_btn_TCPServerSendClear_clicked();
    void on_btn_TCPServerQuickSendAdd_clicked();
    void on_btn_TCPServerQuickSendRevise_clicked();
    void on_btn_TCPServerQuickSendDelete_clicked();

    void on_btn_TCPClientConnect_clicked();
    void on_btn_TCPClientSend_clicked();
    void on_btn_TCPClientReadClear_clicked();
    void on_btn_TCPClientSendClear_clicked();
    void on_btn_TCPClientQuickSendAdd_clicked();
    void on_btn_TCPClientQuickSendRevise_clicked();
    void on_btn_TCPClientQuickSendDelete_clicked();

    void on_comboBox_TCPServerReadTarget_currentIndexChanged(const QString &arg1);
    void on_checkBox_TCPServerSendCyCle_stateChanged(int arg1);
    void on_checkBox_TCPClientSendCyCle_stateChanged(int arg1);
    void on_tableView_TCPServerSendHistory_doubleClicked(const QModelIndex &index);
    void on_tableView_TCPClientSendHistory_doubleClicked(const QModelIndex &index);

    void on_tableView_TCPServerQuickSend_doubleClicked(const QModelIndex &index);
    void on_tableView_TCPServerQuickSend_clicked(const QModelIndex &index);
    void on_tableView_TCPClientQuickSend_clicked(const QModelIndex &index);
    void on_tableView_TCPClientQuickSend_doubleClicked(const QModelIndex &index);

    void on_btn_TCPServerInteractAdd_clicked();
    void on_btn_TCPServerInteractRevise_clicked();
    void on_btn_TCPServerInteractDelete_clicked();
    void on_btn_TCPClientInteractAdd_clicked();
    void on_btn_TCPClientInteractRevise_clicked();
    void on_btn_TCPClientInteractDelete_clicked();

    void on_tableView_TCPClientInteract_clicked(const QModelIndex &index);
    void on_tableView_TCPServerInteract_clicked(const QModelIndex &index);

    void on_btn_TCPClientSeniorSendCycleStart_clicked();
    void on_btn_TCPClientSeniorSendCycleEditor_clicked();
    void on_btn_TCPServerSeniorSendCycleEditor_clicked();
    void on_btn_TCPServerSeniorSendCycleStart_clicked();

    void on_btn_TCPServerClearSend_clicked();
    void on_btn_TCPClientClearSend_clicked();
    void on_tableView_TCPServerInteract_doubleClicked(const QModelIndex &index);
    void on_tableView_TCPClientInteract_doubleClicked(const QModelIndex &index);
    void on_checkBox_SerialPortEnterSend_stateChanged(int arg1);
    void on_checkBox_TCPServerEnterSend_stateChanged(int arg1);
    void on_checkBox_TCPClientEnterSend_stateChanged(int arg1);
    void on_checkBox_SerialPortSendCyCle_stateChanged(int arg1);

    void on_btn_SerialPortChooseSendFile_clicked();
    void on_btn_SerialPortSendFile_clicked();
    void on_btn_SerialPortPausedSendFile_clicked();

    void on_btn_SerialPortStopSendFile_clicked();

private:
    Ui::CommDubugAssist *ui;

    void InitTitlepar();
    void InitQSS();
    void InitData();
    void InitSet();
    void InitConnect();
    void InitStart();

    void Reset();

    void ReadSettings();
    void SaveSettings();

    bool ReadUpDate();

    void SerialPortConnect(bool informstate);
    void SerialPortDisconnect();
    void SerialPortSendLation(QString senddata, TextFormat sendtextformat);
    bool SerialPortSendHex(QString senddata);
    void SerialPortSendHistoryInit();
    void SerialPortAddSendHistory(QString datatime, QString type, QString senddata);
    void SerialPortQuickSendInit();
    void SerialPortQuickSendAdd(QString name);
    void SerialPortQuickSendRevise(int row, QString name);
    void SerialPortQuickSendDelete(int row);
    void SerialPortQuickSendClear();
    void SerialPortInteractInit();
    void SerialPortInteractAdd(InteractData si);
    void SerialPortInteractRevise(int row, InteractData si);
    void SerialPortInteractDelete(int row);
    void SerialPortInteractClear();
    void SerialPortInteract(QByteArray info);
    void SerialPortSeniorCycleInit();
    void SerialPortSeniorCycleAdd(CycleData sc);
    void SerialPortSeniorCycleInsert(int row, CycleData sc);
    void SerialPortSeniorCycleRevise(int row, CycleData sc);
    void SerialPortSeniorCycleSet(QList<CycleData> CycleDatas);
    void SerialPortSeniorCycleClear();

    void TCPServerListen(bool informstate);
    void TCPServerDislisten();
    bool TCPServerSendLation(QString ip, quint16 port, QString senddata, TextFormat sendtextformat);
    bool TCPServerSendHex(QString ip, quint16 port, QString senddata);
    bool TCPServerSendByteArray(QString ip, quint16 port, QByteArray senddata);
    int TCPServerGetClientNum(QString ip, quint16 port);
    void TCPServerShowAddData(QString ip, quint16 port, QString showdata, QString data);
    void TCPServerSendHistoryInit();
    void TCPServerAddSendHistory(QString datatime, QString ip, quint16 port, QString type, QString senddata);
    void TCPServerQuickSendInit();
    void TCPServerQuickSendAdd(QString obj, QString name);
    void TCPServerQuickSendRevise(int row, QString obj, QString name);
    void TCPServerQuickSendDelete(int row);
    void TCPServerQuickSendClear();
    void TCPServerInteractInit();
    void TCPServerInteractAdd(TCPServerInteractData si);
    void TCPServerInteractRevise(int row, TCPServerInteractData si);
    void TCPServerInteractDelete(int row);
    void TCPServerInteractClear();
    void TCPServerInteract(QString ip, quint16 port, QByteArray info);
    void TCPServerSeniorCycleInit();
    void TCPServerSeniorCycleAdd(TCPServerCycleData sc);
    void TCPServerSeniorCycleInsert(int row, TCPServerCycleData sc);
    void TCPServerSeniorCycleRevise(int row, TCPServerCycleData sc);
    void TCPServerSeniorCycleSet(QList<TCPServerCycleData> CycleDatas);
    void TCPServerSeniorCycleClear();

    void TCPClientConnect(bool informstate);
    void TCPClientDisconnect();
    bool TCPClientSendLation(QString senddata, TextFormat sendtextformat);
    bool TCPClientSendHex(QString senddata);
    bool TCPClientSendByteArray(QByteArray senddata);
    void TCPClientShowAddData(QString data);
    void TCPClientSendHistoryInit();
    void TCPClientAddSendHistory(QString datatime, QString type, QString senddata);
    void TCPClientQuickSendInit();
    void TCPClientQuickSendAdd(QString name);
    void TCPClientQuickSendRevise(int row, QString name);
    void TCPClientQuickSendDelete(int row);
    void TCPClientQuickSendClear();
    void TCPClientInteractInit();
    void TCPClientInteractAdd(InteractData si);
    void TCPClientInteractRevise(int row, InteractData si);
    void TCPClientInteractDelete(int row);
    void TCPClientInteractClear();
    void TCPClientInteract(QByteArray info);
    void TCPClientSeniorCycleInit();
    void TCPClientSeniorCycleAdd(CycleData sc);
    void TCPClientSeniorCycleInsert(int row, CycleData sc);
    void TCPClientSeniorCycleRevise(int row, CycleData sc);
    void TCPClientSeniorCycleSet(QList<CycleData> CycleDatas);
    void TCPClientSeniorCycleClear();

    void SerialPortReadDataLimit();
    void TCPServerReadDataLimit();
    void TCPClientReadDataLimit();

    bool m_bForceExit = false;

    QSerialPort *m_serialPort;
    TCPServer *m_tcpserver = new TCPServer();
    TCPClient *m_tcpclient = new TCPClient();
    //QList<QString> m_listSerialPortNames;
    QStandardItemModel *m_staSerialportHistoryModel;
    QStandardItemModel *m_staTCPServerHistoryModel;
    QStandardItemModel *m_staTCPClientHistoryModel;
    QTimer *m_timer;
    QTimer *m_timerSerialPortCycle;
    QTimer *m_timerTCPServerCycle;
    QTimer *m_timerTCPClientCycle;
    int m_nSerialPortCycleTime;
    int m_nTCPServerCycleTime;
    int m_nTCPClientCycleTime;
    QString m_sTCPServerCycleObject;
    QString m_sTCPServerCycleIP;
    quint16 m_nTCPServerCyclePort;

    QList<TCPServerClientData> m_TcpServerClientDatas;

    QuickSendUnit *w_QSUnit = new QuickSendUnit(this);
    InteractUnit *w_InterUnit = new InteractUnit(this);
    SeniorSendCycle *w_SSCycle = new SeniorSendCycle(this);
    TCPServerQuickSendUnit *w_TCPS_QSUnit = new TCPServerQuickSendUnit(this);
    TCPServerInteractUnit *w_TCPS_InterUnit = new TCPServerInteractUnit(this);
    TCPServerSeniorSendCycle *w_TCPS_SSCycle = new TCPServerSeniorSendCycle(this);
    Help *w_Help = new Help(this);
    About *w_About = new About(this);
    Preferences *w_Pref = new Preferences(this);
    Clothes *w_clo = new Clothes(this);
    Upgra *w_up = new Upgra(this);

    SendFile *m_sendFile = new SendFile();
    QString m_LastOpenPath;
    QString m_SendFileName;
    qint64 m_FileSize;
    qint64 m_SendSize;

    // 串口
    QList<QuickSendData> m_SerialPortQuickSendDatas;
    QStandardItemModel *m_staSerialPortQuickSend;
    int m_SerialPortQuickSendChoose;

    QList<InteractData> m_SerialPortInteractDatas;
    QStandardItemModel *m_staSerialPortInteract;
    int m_SerialPortInteractChoose;

    QList<CycleData> m_SerialPortCycleDatas;
    unsigned int m_nSerialPortCycleDelay = 0;
    QStandardItemModel *m_staSerialPortSeniorCycle;
    int m_SerialPortSeniorCycleRunNum;
    SerialPortCycle *w_SP_Cycle = new SerialPortCycle();
    QThread *w_SP_CycleThread = new QThread();

    // TCP 服务端
    QList<TCPServerQuickSendData> m_TCPServerQuickSendDatas;
    QStandardItemModel *m_staTCPServerQuickSend;
    int m_TCPServerQuickSendChoose;

    QList<TCPServerInteractData> m_TCPServerInteractDatas;
    QStandardItemModel *m_staTCPServerInteract;
    int m_TCPServerInteractChoose;

    QList<TCPServerCycleData> m_TCPServerCycleDatas;
    unsigned int m_nTCPServerCycleDelay = 0;
    QStandardItemModel *m_staTCPServerSeniorCycle;
    int m_TCPServerSeniorCycleRunNum;
    TCPServerCycle *w_TCPS_Cycle = new TCPServerCycle();
    QThread *w_TCPS_CycleThread = new QThread();

    // TCP 客户端
    QList<QuickSendData> m_TCPClientQuickSendDatas;
    QStandardItemModel *m_staTCPClientQuickSend;
    int m_TCPClientQuickSendChoose;

    QList<InteractData> m_TCPClientInteractDatas;
    QStandardItemModel *m_staTCPClientInteract;
    int m_TCPClientInteractChoose;

    QList<CycleData> m_TCPClientCycleDatas;
    unsigned int m_nTCPClientCycleDelay = 0;
    QStandardItemModel *m_staTCPClientSeniorCycle;
    int m_TCPClientSeniorCycleRunNum;
    TCPClientCycle *w_TCPC_Cycle = new TCPClientCycle();
    QThread *w_TCPC_CycleThread = new QThread();

    WidgetPar *m_selMainWidget;
    TitleBar *pTitleBar;

    bool m_bFirstOpen = false;

};
#endif // COMMDUBUGASSIST_H
