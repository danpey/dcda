/*
 * 此文件为项目底层代码的头文件
 * 定义了一些底层数据以及一些底层函数
 */

#ifndef BASECODE_H
#define BASECODE_H

#include <QDebug>
#include <QList>
#include <QMessageBox>
#include <QString>
#include <QTime>
#include <QCoreApplication>
#include <QEventLoop>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QProcess>
#include <QTextCodec>
#include <QDir>

#include <vector>
#include "widgetpar.h"

#define VERSION      "Beta 1.0.2"

extern QString g_sAppName;

///
/// \brief 判断是否为独立运行程序（无法保存设置，在双开程序时后开的程序为独立运行程序
///
extern bool g_bIndependent;

enum TextFormat
{
    Local,
    UTF8,
    GBK,
    Unicode,
    HEX,
};

///
/// \brief 连接成功提醒
///
extern bool g_bConnectSucceedRemind;
///
/// \brief 连接失败提醒
///
extern bool g_bConnectFailRemind;
///
/// \brief 连接断开提醒
///
extern bool g_bDisconnectRemind;
///
/// \brief 意外断开提醒
///
extern bool g_bAccidentDisconnectRemind;

///
/// \brief 串口读取数据字节数
///
extern unsigned int g_unSerialPortReadnums;
///
/// \brief 串口发送数据字节数
///
extern unsigned int g_unSerialPortSendnums;
///
/// \brief TCP服务端读取数据字节数
///
extern unsigned int g_unTCPServerReadnums;
///
/// \brief TCP服务端发送数据字节数
///
extern unsigned int g_unTCPServerSendnums;
///
/// \brief TCP客户端读取数据字节数
///
extern unsigned int g_unTCPClientReadnums;
///
/// \brief TCP客户端发送数据字节数
///
extern unsigned int g_unTCPClientSendnums;


///
/// \brief 启动程序串口自动连接
///
extern bool g_bSerialPortAutoConnect;
///
/// \brief 串口连接端口名
///
extern QString g_sSerialPortConnectName;
///
/// \brief 串口连接波特率
///
extern int g_nSerialPortBaudIndex;
///
/// \brief 串口连接数据位
///
extern int g_nSerialPortDataIndex;
///
/// \brief 串口连接控制方式
///
extern int g_nSerialPortControlIndex;
///
/// \brief 串口连接校验位
///
extern int g_nSerialPortParityIndex;
///
/// \brief 串口连接停止位
///
extern int g_nSerialPortStopIndex;
///
/// \brief 串口数据接收长度阈值
///
extern unsigned int g_unSerialPortReadDataLimit;
///
/// \brief 关闭软件后串口数据自动清零
///
extern bool g_bSerialPortDataAutoClear;
///
/// \brief 串口历史发送数据数量阈值
///
extern unsigned int g_unSerialPortHistoryDatanumsLimit;
///
/// \brief 串口接收文本数据格式
///
extern TextFormat g_emSerialPortReadTextFormat;
///
/// \brief 串口发送文本数据格式
///
extern TextFormat g_emSerialPortSendTextFormat;
///
/// \brief 串口连接成功弹窗提醒
///
extern bool g_bSerialPortConTrueRemind;
///
/// \brief 串口连接失败弹窗提醒
///
extern bool g_bSerialPortConFalseRemind;
///
/// \brief 串口主动断开弹窗提醒
///
extern bool g_bSerialPortDisconTrueRemind;
///
/// \brief 串口意外断开弹窗提醒
///
extern bool g_bSerialPortDisconRemind;

///
/// \brief 启动程序TCP服务端自动开启监听
///
extern bool g_bTCPServerAutoListen;
///
/// \brief TCP服务端监听IP
///
extern QString g_sTCPServerListenIP;
///
/// \brief TCP服务端监听端口
///
extern quint16 g_un16TCPServerListenPort;
///
/// \brief TCP服务端数据接收长度阈值
///
extern unsigned int g_unTCPServerReadDataLimit;
///
/// \brief TCP服务端接收文本数据格式
///
extern TextFormat g_emTCPServerReadTextFormat;
///
/// \brief TCP服务端发送文本数据格式
///
extern TextFormat g_emTCPServerSendTextFormat;
///
/// \brief 关闭软件后TCP服务端数据自动清零
///
extern bool g_bTCPServerDataAutoClear;
///
/// \brief TCP服务端历史发送数据数量阈值
///
extern unsigned int g_unTCPServerHistoryDatanumsLimit;

///
/// \brief 启动程序TCP客户端自动连接
///
extern bool g_bTCPClientAutoConnect;
///
/// \brief TCP客户端连接IP
///
extern QString g_sTCPClientConnectIP;
///
/// \brief TCP客户端连接端口
///
extern quint16 g_un16TCPClientConnectPort;
///
/// \brief TCP客户端数据接收长度阈值
///
extern unsigned int g_unTCPClientReadDataLimit;
///
/// \brief TCP客户端接收文本数据格式
///
extern TextFormat g_emTCPClientReadTextFormat;
///
/// \brief TCP客户端发送文本数据格式
///
extern TextFormat g_emTCPClientSendTextFormat;
///
/// \brief 关闭软件后TCP客户端数据自动清零
///
extern bool g_bTCPClientDataAutoClear;
///
/// \brief TCP客户端历史发送数据数量阈值
///
extern unsigned int g_unTCPClientHistoryDatanumsLimit;

///
/// \brief 自动保存所有设置
///
extern bool g_bAutoSaveSettings;

///
/// \brief 快捷发送所存储的结构体
///
struct QuickSendData
{
    QString name;
    QString writedata;
    bool isHex = false;
    bool addLine = false;
};

struct TCPServerQuickSendData
{
    QString name;
    QString writedata;
    bool isHex = false;
    bool addLine = false;
    QString ip;
    quint16 port;
};

struct InteractData
{
    QString readdata;
    QString writedata;
    //bool readhex = false;
    //bool writehex = false;
    TextFormat readTextFormat;
    TextFormat sendTextFormat;
    int mode = 0;
    QString modename;
};

struct TCPServerInteractData
{
    QString modename;
    QString readdata;
    QString writedata;
    //bool readhex = false;
    //bool writehex = false;
    TextFormat readTextFormat;
    TextFormat sendTextFormat;
    int mode = 0;
    QString readip;
    quint16 readport;
    QString writeip;
    quint16 writeport;
};

struct CycleData
{
    QString name;
    QString senddata;
    bool isHex = false;
    unsigned int delay = 0;
};

struct TCPServerCycleData
{
    QString name;
    QString ip;
    quint16 port;
    QString senddata;
    bool isHex = false;
    unsigned int delay = 0;
};

struct TCPServerClientData
{
    QString ip;
    quint16 port;
    QString ShowData;
};
extern QString g_sInitQSSPath;

bool CheckRepeatProcess();
QString PathxtoPath(QString pathx);
bool CheckAndEstablishFiles(QString path);
bool toHex(QString data);
bool toHex(char data, char &outdata);
void MySleep(unsigned int msec);
QString OUT_HEX_String(QByteArray str);
QString TextFormat2Str(TextFormat format);
TextFormat Str2TextFormat(QString str);

QByteArray utf8ToGBK(QByteArray strUtf8);
QByteArray GBK2utf8(QByteArray strgbk);
QByteArray utf8ToUnicode(QByteArray strutf8);
QByteArray unicodeToUtf8(const QString &unicode);


#endif // BASECODE_H
