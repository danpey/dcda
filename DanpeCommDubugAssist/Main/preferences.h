#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QWidget>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QPalette>
#include <QFile>
#include <QFileDialog>
#include <QProcess>

#include "basecode.h"

namespace Ui {
class Preferences;
}

class Preferences : public QWidget
{
    Q_OBJECT

public:
    explicit Preferences(QWidget *parent = nullptr);
    ~Preferences();
    void show();
    void InitQSS();
    void inform(QString strdata);
    bool AskInform(QString strdata);
    void ReadSettings();
    void close();
    void checkUpdateShow();

signals:
    void CloseSignal();
    void ExitSignal();
    void ReadSendUpdateSignal();
    void SaveSettingsSianal();
    void ResetSettingsSignal();
    void SerialPortSeniorCycleOpenSignal();
    void TCPServerSeniorCycleOpenSignal();
    void TCPClientSeniorCycleOpenSignal();

    void SerialPortQuickSendDeriveSignal(QString path);
    void SerialPortQuickSendInductSignal(QString path);
    void SerialPortInteractDeriveSignal(QString path);
    void SerialPortInteractInductSignal(QString path);

    void TCPServerQuickSendDeriveSignal(QString path);
    void TCPServerQuickSendInductSignal(QString path);
    void TCPServerInteractDeriveSignal(QString path);
    void TCPServerInteractInductSignal(QString path);

    void TCPClientQuickSendDeriveSignal(QString path);
    void TCPClientQuickSendInductSignal(QString path);
    void TCPClientInteractDeriveSignal(QString path);
    void TCPClientInteractInductSignal(QString path);

private slots:
    void slotCurrentRowChanged(const QModelIndex &current, const QModelIndex &previous);
    void ReadFromUpdate();

    void showEvent(QShowEvent *event);

    void on_btn_Close_clicked();
    void on_btn_Renovate_clicked();
    void on_btn_Setting_clicked();
    void on_btn_SerialPortDataClear_clicked();
    void on_btn_TCPServerDataClear_clicked();
    void on_btn_TCPClientDataClear_clicked();
    void on_btn_SerialPortSeniorCycleOpen_clicked();
    void on_btn_TCPServerSeniorCycleOpen_clicked();
    void on_btn_TCPClientSeniorCycleOpen_clicked();
    void on_btn_SerialPortQuickSendDerive_clicked();
    void on_btn_SerialPortQuickSendInduct_clicked();
    void on_btn_SerialPortInteractDerive_clicked();
    void on_btn_SerialPortInteractInduct_clicked();
    void on_btn_TCPServerQuickSendDerive_clicked();
    void on_btn_TCPServerQuickSendInduct_clicked();
    void on_btn_TCPServerInteractDerive_clicked();
    void on_btn_TCPServerInteractInduct_clicked();
    void on_btn_TCPClientQuickSendDerive_clicked();
    void on_btn_TCPClientQuickSendInudct_clicked();
    void on_btn_TCPClientInteractDerive_clicked();
    void on_btn_TCPClientInteractInduct_clicked();

    void on_btn_SettingsSaveDefault_clicked();

    void on_btn_ResetSettings_clicked();

    void on_btn_update_clicked();

private:
    Ui::Preferences *ui;

    void InitTitlepar();
    void InitTree();
    int ModelGetWidget(int parrow, QString name);

    void SetSettings();

    QList<QWidget *> m_widgets;

    QStandardItemModel *m_model = new QStandardItemModel();
    WidgetPar *m_selMainWidget;
    TitleBar *pTitleBar;

    QProcess *m_proUpdate = nullptr;
};

#endif // PREFERENCES_H
