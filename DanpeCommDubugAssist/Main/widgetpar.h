#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QCloseEvent>
#include <QStyleOption>
#include <QHBoxLayout>
#include <QRect>
#include <QRubberBand>
#include <QMouseEvent>
#include <QHoverEvent>
#include <QApplication>
#include <QFlags>
#include <QListView>

class FramelessHelperPrivate;

class FramelessHelper : public QWidget
{
    Q_OBJECT
public:
    explicit FramelessHelper(QWidget *parent = nullptr);
    ~FramelessHelper();

    // 激活窗体
    void activateOn(QWidget *topLevelWidget);
    // 移除窗体
    void removeFrom(QWidget *topLevelWidget);
    // 设置窗体移动
    void setWidgetMovable(bool movable);
    // 设置窗体缩放
    void setWidgetResizable(bool resizable);
    // 设置橡皮筋移动
    void setRubberBandOnMove(bool movable);
    // 设置橡皮筋缩放
    void setRubberBandOnResize(bool resizable);
    // 设置边框的宽度
    void setBorderWidth(uint width);
    // 设置标题栏高度
    void setTitleHeight(uint height);
    bool widgetResizable();
    bool widgetMovable();
    bool rubberBandOnMove();
    bool rubberBandOnResisze();
    uint borderWidth();
    uint titleHeight();

public slots:
    bool eventFilter(QObject *obj, QEvent *event);

private:
    FramelessHelperPrivate *d;
};

class TitleBar : public QWidget
{
    Q_OBJECT

 public:
    explicit TitleBar(QWidget *parent = nullptr);
    void showSkinBtn(bool state);
    void showSettingBtn(bool state);
    void showUpgradeBtn(bool state);
    void showLockBtn(bool state);
    void showQuestionBtn(bool state);
    void EnableSkinBtn(bool state);
    void EnableSettingBtn(bool state);
    void EnableUpgradeBtn(bool state);
    void EnableLockBtn(bool state);
    void EnableQuestioinBtn(bool state);

    void showMinmizeBtn(bool state);
    void showMaxmizeBtn(bool state);
    void showCloseBtn(bool state);
    void EnableMinmizeBtn(bool state);
    void EnableMaxmizeBtn(bool state);
    void EnableCloseBtn(bool state);

    bool getLockState();

 protected:

    virtual void mouseDoubleClickEvent(QMouseEvent *event);// 双击标题栏进行界面的最大化/还原
    //virtual void mousePressEvent(QMouseEvent *event);// 进行鼠界面的拖动
    virtual bool eventFilter(QObject *obj, QEvent *event);// 设置界面标题与图标

    //QTimer m_titleRollTimer;

 signals:
    void OpenClothesSignal();
    void OpenSettingsSignal();
    void UpGradeSignal();
    void QuestionSignal();

 private slots:

     // 进行最小化、最大化/还原、关闭操作
     void on_TiBar_Clicked();
     void paintEvent(QPaintEvent *event);

 private:

     // 最大化/还原
     void updateMaximize();// 最大化/还原

 private:

     QLabel *TiBar_pLeftSpacing;

     QLabel *TiBar_pIconLabel;              // 左上角图标label
     QLabel *TiBar_pTitleLabel;             // 中间标题栏label
     QPushButton *TiBar_pSkinBtn;           // 换装按钮
     QPushButton *TiBar_pSettingsBtn;       // 设置按钮
     QPushButton *TiBar_pUpgradeBtn;        // 软件升级按钮
     QPushButton *TiBar_pLockBtn;           // 窗口置顶锁定按钮
     QPushButton *TiBar_pQuestionBtn;       // 疑问按钮
     QPushButton *TiBar_pMinimizeBtn;       // 右上角缩小到底部任务栏按钮,最小化
     QPushButton *TiBar_pMaximizeBtn;       // 右上角放大缩小按钮
     QPushButton *TiBar_pCloseBtn;          // 右上角关闭按钮

};

/*
定义一个新的QFrame类,将标题栏和UI界面都放到这个类里面,定义这个类是为了方便改变主题
*/
class WidgetPar : public QFrame
{
    Q_OBJECT
public:
    explicit WidgetPar(QFrame *parent = nullptr);
    void setTitleHeight(unsigned int height);
    void setWidgetResizable(bool state);

signals:
    void CloseSignal(QCloseEvent *event);
    void ShowSignal(QShowEvent *event);

private slots:
    void showEvent(QShowEvent *event);
    void closeEvent(QCloseEvent *event);

private:
    FramelessHelper *m_pHelper;
};

#endif // TITLEBAR_H
