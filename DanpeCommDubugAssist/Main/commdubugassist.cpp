#include "commdubugassist.h"
#include "ui_commdubugassist.h"

CommDubugAssist::CommDubugAssist(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::CommDubugAssist)
{
    ui->setupUi(this);

    // 设置自定义标题栏
    InitTitlepar();

    // 判断是否为独立程序
    if (!g_bIndependent)
    {
        // 读取设置参数
        ReadSettings();
    }
    else
    {
        ResetSettings();
    }
    // 初始化数据
    InitData();
    // 初始化设置
    InitSet();
    // 设置QSS样式
    InitQSS();
    SetQSSes();
    // 初始化连接
    InitConnect();
    // 初始化打开
    InitStart();

    if (!g_bIndependent)
    {
        SaveSettings();
    }

    // 检查更新
    bool ok = ReadUpDate();
    if (ok)
    {
        pTitleBar->showUpgradeBtn(true);
    }

    m_selMainWidget->show();
}

CommDubugAssist::~CommDubugAssist()
{
    delete ui;
}

///
/// \brief 初始化标题栏
///
void CommDubugAssist::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showSkinBtn(true);
    pTitleBar->showLockBtn(true);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &CommDubugAssist::closeEvent);
    connect(pTitleBar, &TitleBar::OpenClothesSignal, this, &CommDubugAssist::OpenClothes);
    connect(pTitleBar, &TitleBar::UpGradeSignal, this, &CommDubugAssist::checkUpdateShow);

    QString titlename = QString("蛋皮通信调试助手 ") + VERSION;
    if (g_bIndependent)
    {
        titlename = QString("蛋皮通信调试助手 ") + VERSION + " (独立程序)";
    }
    m_selMainWidget->setWindowTitle(titlename);
    this->setWindowTitle(titlename);
    setWindowIcon(QIcon(":/image/DCDA.ico"));

    ui->comboBox_SerialPortName->setView(new QListView(this));
    ui->comboBox_SerialPortBaud->setView(new QListView(this));
    ui->comboBox_SerialPortData->setView(new QListView(this));
    ui->comboBox_SerialPortParity->setView(new QListView(this));
    ui->comboBox_SerialPortStop->setView(new QListView(this));
    ui->comboBox_SerialPortFlowControl->setView(new QListView(this));
    ui->comboBox_TCPServerReadTarget->setView(new QListView(this));
    ui->comboBox_TCPServerSendTarget->setView(new QListView(this));
    ui->comboBox_TCPServerSendCycleTarget->setView(new QListView(this));
}

///
/// \brief 初始化QSS
///
void CommDubugAssist::InitQSS()
{
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();
}

///
/// \brief 设置QSS
///
void CommDubugAssist::SetQSSes()
{
    InitQSS();
    w_Pref->InitQSS();
    w_About->InitQSS();
    w_Help->InitQSS();
    w_QSUnit->InitQSS();
    w_clo->InitQSS();
    w_TCPS_QSUnit->InitQSS();
    w_InterUnit->InitQSS();
    w_TCPS_InterUnit->InitQSS();
    w_SSCycle->InitQSS();
    w_TCPS_SSCycle->InitQSS();
    w_up->InitQSS();
}

///
/// \brief 初始化数据
///
void CommDubugAssist::InitData()
{
    m_bForceExit = false;
    m_nSerialPortCycleDelay = 0;
    m_timer = new QTimer(this);
    m_timerSerialPortCycle = new QTimer(this);
    m_timerTCPServerCycle = new QTimer(this);
    m_timerTCPClientCycle = new QTimer(this);
    TCPServerClientData tcpserverclientdata;
    tcpserverclientdata.ip = "ALL Connections";
    tcpserverclientdata.port = 0;
    m_TcpServerClientDatas.append(tcpserverclientdata);
}

///
/// \brief 初始化设置
///
void CommDubugAssist::InitSet()
{
    Reset();

    ui->textEdit_SerialPortRead->setProperty("class", "TestEditRead");
    ui->textEdit_TCPServerRead->setProperty("class", "TestEditRead");
    ui->textEdit_TCPClientRead->setProperty("class", "TestEditRead");

    ui->textEdit_SerialPortSend->installEventFilter(this);
    ui->textEdit_TCPServerSend->installEventFilter(this);
    ui->textEdit_TCPClientSend->installEventFilter(this);

    ui->textEdit_SerialPortSend->setAcceptDrops(false);
//    connect(ui->textEdit_SerialPortSend, SIGNAL(dragEnterEvent(QDragEnterEvent *e)), this, SLOT(textEdit_SerialPortSend_dragEnterEvent(QDragEnterEvent *event)));
//    connect(ui->textEdit_SerialPortSend, SIGNAL(dropEvent(QDropEvent *e)), this, SLOT(textEdit_SerialPortSend_dropEvent(QDropEvent *event)));
    ui->textEdit_TCPServerSend->setAcceptDrops(false);
    ui->textEdit_TCPClientSend->setAcceptDrops(false);
    this->setAcceptDrops(true);
    //this->setAccessibleName("可将内容复制到发送区");

    //ui->splitter_SerialPort->setAutoFillBackground(true);
    ui->tabWidget_main->setCurrentIndex(0);
    ui->tabWidget_SerialPort->setCurrentIndex(0);
    ui->tabWidget_TCPServer->setCurrentIndex(0);
    ui->tabWidget_TCPClient->setCurrentIndex(0);

    ui->progressBar_SerialPortSendFiled->hide();
    ui->btn_SerialPortPausedSendFile->hide();
    ui->btn_SerialPortStopSendFile->hide();

    ui->splitter_SerialPort->setStretchFactor(0, 12);   // 前面为标号，后面为比例，即，标号1的控件的拉伸比例为12
    ui->splitter_SerialPort->setStretchFactor(1, 4);   //                         标号2的控件的拉伸比例为4
    ui->splitter_TCPServer->setStretchFactor(0, 11);
    ui->splitter_TCPServer->setStretchFactor(1, 4);
    ui->splitter_TCPClient->setStretchFactor(0, 12);
    ui->splitter_TCPClient->setStretchFactor(1, 4);

    QRegExp rx("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$");
    ui->lineEdit_TCPServerLocalIP->setValidator(new QRegExpValidator(rx));
//    ui->lineEdit_TCPServerLocalIP->setInputMask("000.000.000.000;0");
//    ui->lineEdit_TCPServerLocalIP->setText("127.0.0.1");

    ui->lineEdit_TCPClientConnectIP->setValidator(new QRegExpValidator(rx));
//    ui->lineEdit_TCPClientConnectIP->setText("127.0.0.1");

    w_SP_Cycle->moveToThread(w_SP_CycleThread);
    w_TCPC_Cycle->moveToThread(w_TCPC_CycleThread);
    w_TCPS_Cycle->moveToThread(w_TCPS_CycleThread);

    // 初始化表格
    SerialPortSendHistoryInit();    // 串口历史记录
    SerialPortQuickSendInit();      // 串口快捷发送
    SerialPortInteractInit();       // 串口交互发送
    SerialPortSeniorCycleInit();    // 串口高级循环
    TCPServerSendHistoryInit();     // TCP Server 历史记录
    TCPServerQuickSendInit();       // TCP Server 快捷发送
    TCPServerInteractInit();        // TCP Server 交互发送
    TCPServerSeniorCycleInit();     // TCP Server 高级循环
    TCPClientSendHistoryInit();     // TCP Client 历史记录
    TCPClientQuickSendInit();       // TCP Client 快捷发送
    TCPClientInteractInit();        // TCP Client 交互发送
    TCPClientSeniorCycleInit();     // TCP Client 高级循环

    m_serialPort = new QSerialPort(this);
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QString showserial = info.portName() + ":" + info.description();
        ui->comboBox_SerialPortName->addItem(showserial);
    }
    if (!g_sSerialPortConnectName.isEmpty())
    {
        int num = ui->comboBox_SerialPortName->findText(g_sSerialPortConnectName);
        if (num >= 0) ui->comboBox_SerialPortName->setCurrentText(g_sSerialPortConnectName);
    }

    ui->tableView_SerialPortSendHistory->setMouseTracking(true);
    ui->tableView_SerialPortQuickSend->setMouseTracking(true);
    ui->tableView_SerialPortInteract->setMouseTracking(true);
    ui->tableView_SerialPortSeniorSendCycle->setMouseTracking(true);

    ui->tableView_TCPServerSendHistory->setMouseTracking(true);
    ui->tableView_TCPServerQuickSend->setMouseTracking(true);
    ui->tableView_TCPServerInteract->setMouseTracking(true);
    ui->tableView_TCPServerSeniorSendCycle->setMouseTracking(true);

    ui->tableView_TCPClientSendHistory->setMouseTracking(true);
    ui->tableView_TCPClientQuickSend->setMouseTracking(true);
    ui->tableView_TCPClientInteract->setMouseTracking(true);
    ui->tableView_TCPClientSeniorSendCycle->setMouseTracking(true);
}

void CommDubugAssist::Reset()
{
    ui->label_SerialPortReadValue->setText(QString::number(g_unSerialPortReadnums));
    ui->label_SerialPortSendValue->setText(QString::number(g_unSerialPortSendnums));
    ui->label_TCPServerReadValue->setText(QString::number(g_unTCPServerReadnums));
    ui->label_TCPServerSendValue->setText(QString::number(g_unTCPServerSendnums));
    ui->label_TCPClientReadValue->setText(QString::number(g_unTCPClientReadnums));
    ui->label_TCPClientSendValue->setText(QString::number(g_unTCPClientSendnums));
    //默认设置
    ui->comboBox_SerialPortData->setCurrentIndex(g_nSerialPortDataIndex);  // 设置默认8位数据位
    ui->comboBox_SerialPortBaud->setCurrentIndex(g_nSerialPortBaudIndex);  // 设置默认波特率9600
    ui->comboBox_SerialPortStop->setCurrentIndex(g_nSerialPortStopIndex);
    ui->comboBox_SerialPortParity->setCurrentIndex(g_nSerialPortParityIndex);
    ui->comboBox_SerialPortFlowControl->setCurrentIndex(g_nSerialPortControlIndex);

    ui->lineEdit_TCPServerLocalIP->setText(g_sTCPServerListenIP);
    ui->lineEdit_TCPServerLocalPort->setText(QString::number(g_un16TCPServerListenPort));

    ui->lineEdit_TCPClientConnectIP->setText(g_sTCPClientConnectIP);
    ui->lineEdit_TCPClientConnectPort->setText(QString::number(g_un16TCPClientConnectPort));

    if (ui->checkBox_SerialPortEnterSend->isChecked())
    {
        ui->btn_SerialPortSend->setToolTip("Enter");
    }
    else
    {
        ui->btn_SerialPortSend->setToolTip("Ctrl+Enter");
    }
    if (ui->checkBox_TCPServerEnterSend->isChecked())
    {
        ui->btn_TCPServerSend->setToolTip("Enter");
    }
    else
    {
        ui->btn_TCPServerSend->setToolTip("Ctrl+Enter");
    }
    if (ui->checkBox_TCPClientEnterSend->isChecked())
    {
        ui->btn_TCPClientSend->setToolTip("Enter");
    }
    else
    {
        ui->btn_TCPClientSend->setToolTip("Ctrl+Enter");
    }
}

///
/// \brief 初始化连接
///
void CommDubugAssist::InitConnect()
{
    connect(ui->action_Help, SIGNAL(triggered(bool)), this, SLOT(HelpShow()));
    connect(w_Help, &Help::CloseSignal, this, &CommDubugAssist::HelpClose);
    connect(ui->action_about, SIGNAL(triggered(bool)), this, SLOT(AboutShow()));
    connect(w_About, &About::CloseSignal, this, &CommDubugAssist::AboutClose);
    connect(ui->action_bug, SIGNAL(triggered(bool)), this, SLOT(Bug()));
    connect(ui->action_Preference, SIGNAL(triggered(bool)), this, SLOT(PrefShow()));
    connect(ui->action_ToolBox, SIGNAL(triggered(bool)), this, SLOT(Developing()));

    connect(w_clo, &Clothes::SetQSSSignal, this, &CommDubugAssist::SetQSSes);

    // SerialPort
    connect(w_Pref, &Preferences::CloseSignal, this, &CommDubugAssist::PrefClose);
    connect(w_Pref, &Preferences::ExitSignal, this, &CommDubugAssist::ForceExit);
    connect(w_Pref, &Preferences::ReadSendUpdateSignal, this, &CommDubugAssist::PrefReadSendNumsUpdate);
    connect(w_Pref, &Preferences::SaveSettingsSianal, this, &CommDubugAssist::SaveSettings);
    connect(w_Pref, &Preferences::ResetSettingsSignal, this, &CommDubugAssist::ResetSettings);
    connect(w_Pref, &Preferences::SerialPortSeniorCycleOpenSignal, this, &CommDubugAssist::on_btn_SerialPortSeniorSendCycleEditor_clicked);
    connect(w_Pref, &Preferences::TCPServerSeniorCycleOpenSignal, this, &CommDubugAssist::on_btn_TCPServerSeniorSendCycleEditor_clicked);
    connect(w_Pref, &Preferences::TCPClientSeniorCycleOpenSignal, this, &CommDubugAssist::on_btn_TCPClientSeniorSendCycleEditor_clicked);

    connect(w_Pref, &Preferences::SerialPortQuickSendDeriveSignal, this, &CommDubugAssist::SerialPortQuickSendDerive);
    connect(w_Pref, &Preferences::SerialPortQuickSendInductSignal, this, &CommDubugAssist::SerialPortQuickSendInduct);
    connect(w_Pref, &Preferences::SerialPortInteractDeriveSignal, this, &CommDubugAssist::SerialPortInteractDerive);
    connect(w_Pref, &Preferences::SerialPortInteractInductSignal, this, &CommDubugAssist::SerialPortInteractInduct);
    connect(w_Pref, &Preferences::TCPServerQuickSendDeriveSignal, this, &CommDubugAssist::TCPServerQuickSendDerive);
    connect(w_Pref, &Preferences::TCPServerQuickSendInductSignal, this, &CommDubugAssist::TCPServerQuickSendInduct);
    connect(w_Pref, &Preferences::TCPServerInteractDeriveSignal, this, &CommDubugAssist::TCPServerInteractDerive);
    connect(w_Pref, &Preferences::TCPServerInteractInductSignal, this, &CommDubugAssist::TCPServerInteractInduct);
    connect(w_Pref, &Preferences::TCPClientQuickSendDeriveSignal, this, &CommDubugAssist::TCPClientQuickSendDerive);
    connect(w_Pref, &Preferences::TCPClientQuickSendInductSignal, this, &CommDubugAssist::TCPClientQuickSendInduct);
    connect(w_Pref, &Preferences::TCPClientInteractDeriveSignal, this, &CommDubugAssist::TCPClientInteractDerive);
    connect(w_Pref, &Preferences::TCPClientInteractInductSignal, this, &CommDubugAssist::TCPClientInteractInduct);

    connect(w_QSUnit, &QuickSendUnit::AddSignal, this, &CommDubugAssist::QS_Add);
    connect(w_QSUnit, &QuickSendUnit::ReviseSignal, this, &CommDubugAssist::QS_Revise);
    connect(w_QSUnit, &QuickSendUnit::closeSignal, this, &CommDubugAssist::QS_close);

    connect(w_TCPS_QSUnit, &TCPServerQuickSendUnit::AddSignal, this, &CommDubugAssist::TCPS_QS_Add);
    connect(w_TCPS_QSUnit, &TCPServerQuickSendUnit::ReviseSignal, this, &CommDubugAssist::TCPS_QS_Revise);
    connect(w_TCPS_QSUnit, &TCPServerQuickSendUnit::closeSignal, this, &CommDubugAssist::TCPS_QS_close);

    connect(w_InterUnit, &InteractUnit::AddSignal, this, &CommDubugAssist::Inter_Add);
    connect(w_InterUnit, &InteractUnit::ReviseSignal, this, &CommDubugAssist::Inter_Revise);
    connect(w_InterUnit, &InteractUnit::CloseSignal, this, &CommDubugAssist::Inter_close);

    connect(w_TCPS_InterUnit, &TCPServerInteractUnit::AddSignal, this, &CommDubugAssist::TCPS_Inter_Add);
    connect(w_TCPS_InterUnit, &TCPServerInteractUnit::ReviseSignal, this, &CommDubugAssist::TCPS_Inter_Revise);
    connect(w_TCPS_InterUnit, &TCPServerInteractUnit::CloseSignal, this, &CommDubugAssist::TCPS_Inter_close);

    connect(w_SSCycle, &SeniorSendCycle::CloseSignal, this, &CommDubugAssist::SSC_close);
    connect(w_SSCycle, &SeniorSendCycle::FinishSignal, this, &CommDubugAssist::SSC_Finish);
    connect(w_SSCycle, &SeniorSendCycle::JudgeDataSignal, this, &CommDubugAssist::SSC_JudgeData);

    connect(w_TCPS_SSCycle, &TCPServerSeniorSendCycle::CloseSignal, this, &CommDubugAssist::TCPS_SSC_close);
    connect(w_TCPS_SSCycle, &TCPServerSeniorSendCycle::FinishSignal, this, &CommDubugAssist::TCPS_SSC_Finish);
    connect(w_TCPS_SSCycle, &TCPServerSeniorSendCycle::JudgeDataSignal, this, &CommDubugAssist::TCPS_SSC_JudgeData);

    connect(this, &CommDubugAssist::SerialPortStartCycle, w_SP_Cycle, &SerialPortCycle::StartCycle);
    connect(this, &CommDubugAssist::TCPClientStartCycle, w_TCPC_Cycle, &TCPClientCycle::StartCycle);
    connect(this, &CommDubugAssist::TCPServerStartCycle, w_TCPS_Cycle, &TCPServerCycle::StartCycle);
    connect(w_SP_CycleThread, &QThread::finished, w_SP_Cycle, &QObject::deleteLater);
    connect(w_TCPC_CycleThread, &QThread::finished, w_TCPC_Cycle, &QObject::deleteLater);
    connect(w_TCPS_CycleThread, &QThread::finished, w_TCPS_Cycle, &QObject::deleteLater);
    connect(w_SP_Cycle, &SerialPortCycle::SendDataSignal, this, &CommDubugAssist::SerialPortSeniorCycleSendData);
    connect(w_TCPC_Cycle, &TCPClientCycle::SendDataSignal, this, &CommDubugAssist::TCPClientSeniorCycleSendData);
    connect(w_TCPS_Cycle, &TCPServerCycle::SendDataSignal, this, &CommDubugAssist::TCPServerSeniorCycleSendData);

    connect(m_tcpserver, &TCPServer::NewConnectingSignal, this, &CommDubugAssist::TCPServerNewConncetion);
    connect(m_tcpserver, &TCPServer::DisConnectionSignal, this, &CommDubugAssist::TCPServerDisConnection);
    connect(m_tcpserver, &TCPServer::ReadDataSignal, this, &CommDubugAssist::TCPServerReadData);

    connect(m_tcpclient, &TCPClient::ConnectedSignal, this, &CommDubugAssist::TCPClientConnection);
    connect(m_tcpclient, &TCPClient::DisconnectedSignal, this, &CommDubugAssist::TCPClientDisconnection);
    connect(m_tcpclient, &TCPClient::ReadDataSignal, this, &CommDubugAssist::TCPClientReaddata);

    connect(m_sendFile, &SendFile::returnSendData, this, &CommDubugAssist::SendFileGetData);

    connect(ui->tableView_SerialPortQuickSend, SIGNAL(entered(const QModelIndex &)), this, SLOT(SerialPortQuickSendMoveEntered(const QModelIndex &)));
    connect(ui->tableView_SerialPortInteract, SIGNAL(entered(const QModelIndex &)), this, SLOT(SerialPortInteractMoveEntered(const QModelIndex &)));
    connect(ui->tableView_SerialPortSeniorSendCycle, SIGNAL(entered(const QModelIndex &)), this, SLOT(SerialPortSeniorSendCycleMoveEntered(const QModelIndex &)));
    connect(ui->tableView_SerialPortSendHistory, SIGNAL(entered(const QModelIndex &)), this, SLOT(SerialPortHistoryMoveEntered(const QModelIndex &)));

    connect(ui->tableView_TCPServerQuickSend, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPServerQuickSendMoveEntered(const QModelIndex &)));
    connect(ui->tableView_TCPServerInteract, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPServerInteractMoveEntered(const QModelIndex &)));
    connect(ui->tableView_TCPServerSeniorSendCycle, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPServerSeniorSendCycleMoveEntered(const QModelIndex &)));
    connect(ui->tableView_TCPServerSendHistory, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPServerHistoryMoveEntered(const QModelIndex &)));

    connect(ui->tableView_TCPClientQuickSend, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPClientQuickSendMoveEntered(const QModelIndex &)));
    connect(ui->tableView_TCPClientInteract, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPClientInteractMoveEntered(const QModelIndex &)));
    connect(ui->tableView_TCPClientSeniorSendCycle, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPClientSeniorSendCycleMoveEntered(const QModelIndex &)));
    connect(ui->tableView_TCPClientSendHistory, SIGNAL(entered(const QModelIndex &)), this, SLOT(TCPClientHistoryMoveEntered(const QModelIndex &)));

    connect(m_timer, SIGNAL(timeout()), this, SLOT(MyTimerEvent()));
    connect(m_timerSerialPortCycle, SIGNAL(timeout()), this, SLOT(SerialPortSendCycle()));
    connect(m_timerTCPServerCycle, SIGNAL(timeout()), this, SLOT(TCPServerSendCycle()));
    connect(m_timerTCPClientCycle, SIGNAL(timeout()), this, SLOT(TCPClientSendCycle()));

    m_timer->start(1000);
    w_SP_CycleThread->start();
    w_TCPC_CycleThread->start();
    w_TCPS_CycleThread->start();
}

///
/// \brief 初始化开启
///
void CommDubugAssist::InitStart()
{
    // 判断是否需要自动开启串口
    if ((!g_sSerialPortConnectName.isEmpty()) && g_sSerialPortConnectName == ui->comboBox_SerialPortName->currentText())
    {
        if (g_bSerialPortAutoConnect)
        {
            if(!m_serialPort->isOpen())
            {
                SerialPortConnect(false);
            }
        }
    }
    // 判断是否需要自动开启TCP服务端
    if (g_bTCPServerAutoListen)
    {
        if (ui->btn_TCPServerStartListening->text() == "开启监听")
        {
            TCPServerListen(true);
        }
    }
    // 判断是否需要自动开启TCP客户端
    if (g_bTCPClientAutoConnect)
    {
        if (ui->btn_TCPClientConnect->text() == "连接")
        {
            TCPClientConnect(true);
        }
    }
}

///
/// \brief 读取设置
///
void CommDubugAssist::ReadSettings()
{
    QCoreApplication::setOrganizationName("Danpe");
    QCoreApplication::setOrganizationDomain("danpe.top");
    QCoreApplication::setApplicationName("CommDubugAssist");
    QCoreApplication::setApplicationVersion(VERSION);
    QSettings settings;
    QString Version = settings.value("VERSION", VERSION).toString();
    if (Version != VERSION)
    {
        m_bFirstOpen = true;
    }

    g_bAutoSaveSettings = settings.value("universal/AutoSaveSettings", true).toBool();
    g_sInitQSSPath = settings.value("universal/QSSPath", ":/qss/bluewhite/bluewhite.qss").toString();
    g_bConnectSucceedRemind = settings.value("universal/ConnectSucceedRemind", false).toBool();
    g_bConnectFailRemind = settings.value("universal/ConnectFailRemind", true).toBool();
    g_bDisconnectRemind = settings.value("universal/DisconnectRemind", false).toBool();
    g_bAccidentDisconnectRemind = settings.value("universal/AccidentDisconnectRemind", true).toBool();

    g_bSerialPortAutoConnect = settings.value("SerialPort/AutoConnect", false).toBool();
    g_sSerialPortConnectName = settings.value("SerialPort/ConnectName").toString();
    g_nSerialPortBaudIndex = settings.value("SerialPort/BaudIndex", 3).toInt();
    g_nSerialPortDataIndex = settings.value("SerialPort/DataIndex", 3).toInt();
    g_nSerialPortControlIndex = settings.value("SerialPort/ControlIndex", 0).toInt();
    g_nSerialPortParityIndex = settings.value("SerialPort/ParityIndex", 0).toInt();
    g_nSerialPortStopIndex = settings.value("SerialPort/StopIndex", 0).toInt();
    g_unSerialPortReadDataLimit = static_cast<uint>(settings.value("SerialPort/ReadDataLimit", 0).toInt());
    g_bSerialPortDataAutoClear = settings.value("SerialPort/DataAutoClear", false).toBool();
    g_unSerialPortHistoryDatanumsLimit = static_cast<uint>(settings.value("SerialPort/HistoryDatanumsLimit", 0).toInt());
    g_unSerialPortReadnums = static_cast<uint>(settings.value("SerialPort/Readnums", 0).toInt());
    g_unSerialPortSendnums = static_cast<uint>(settings.value("SerialPort/Sendnums", 0).toInt());

    ui->checkBox_SerialPortReadHex->setChecked(settings.value("SerialPort/read/ReadHex", false).toBool());
    ui->checkBox_SerialPortReadAutoLine->setChecked(settings.value("SerialPort/read/ReadAutoLine", false).toBool());
    ui->checkBox_SerialPortStopShow->setChecked(settings.value("SerialPort/read/StopShow", false).toBool());
    ui->checkBox_SerialPortRecordTime->setChecked(settings.value("SerialPort/read/RecordTime", false).toBool());
    ui->checkBox_SerialPortRecordSend->setChecked(settings.value("SerialPort/read/RecordSend", false).toBool());
    ui->checkBox_SerialPortReadCurSor->setChecked(settings.value("SerialPort/read/ReadCurSor", false).toBool());

    ui->checkBox_SerialPortSendHex->setChecked(settings.value("SerialPort/send/SendHex", false).toBool());
    ui->checkBox_SerialPortSendAddLine->setChecked(settings.value("SerialPort/send/SendAddLine", false).toBool());
    ui->spinBox_SerialPortSendCycle->setValue(settings.value("SerialPort/send/SendCycleValue", 1000).toInt());
    ui->checkBox_SerialPortSendCyCleHex->setChecked(settings.value("SerialPort/send/SendCyCleHex", false).toBool());
    ui->lineEdit_SerialPortSendCycleText->setText(settings.value("SerialPort/send/SendCycleText").toString());
    ui->checkBox_SerialPortSendAutoClear->setChecked(settings.value("SerialPort/send/SendAutoClear", false).toBool());
    ui->checkBox_SerialPortEnterSend->setChecked(settings.value("SerialPort/send/EnterSend", false).toBool());
    ui->checkBox_SerialPortSendHistory->setChecked(settings.value("SerialPort/send/SendHistory", true).toBool());
    ui->textEdit_SerialPortSend->setText(settings.value("SerialPort/send/Send").toString());
    uint SerialPortSendTextFormat = settings.value("SerialPort/send/TextFormat", 0).toUInt();
    switch (SerialPortSendTextFormat) {
    case 0: g_emSerialPortSendTextFormat = Local; break;
    case 1: g_emSerialPortSendTextFormat = UTF8; break;
    case 2: g_emSerialPortSendTextFormat = GBK; break;
    case 3: g_emSerialPortSendTextFormat = Unicode; break;
    }

    g_bTCPServerAutoListen = settings.value("TCPServer/AutoListen", false).toBool();
    g_sTCPServerListenIP = settings.value("TCPServer/ListenIP", "127.0.0.1").toString();
    g_un16TCPServerListenPort = static_cast<quint16>(settings.value("TCPServer/ListenPort", 3620).toInt());
    ui->lineEdit_TCPServerLocalIP->setText(g_sTCPServerListenIP);
    ui->lineEdit_TCPServerLocalPort->setText(QString(g_un16TCPServerListenPort));
    g_unTCPServerReadDataLimit = static_cast<uint>(settings.value("TCPServer/ReadDataLimit", 0).toInt());
    g_bTCPServerDataAutoClear = settings.value("TCPServer/DataAutoClear", false).toBool();
    g_unTCPServerHistoryDatanumsLimit = static_cast<uint>(settings.value("TCPServer/HistoryDatanumsLimit", 0).toInt());
    g_unTCPServerReadnums = static_cast<uint>(settings.value("TCPServer/Readnums", 0).toInt());
    g_unTCPServerSendnums = static_cast<uint>(settings.value("TCPServer/Sendnums", 0).toInt());

    ui->checkBox_TCPServerReadHex->setChecked(settings.value("TCPServer/read/ReadHex", false).toBool());
    ui->checkBox_TCPServerReadAutoLine->setChecked(settings.value("TCPServer/read/ReadAutoLine", false).toBool());
    ui->checkBox_TCPServerStopShow->setChecked(settings.value("TCPServer/read/StopShow", false).toBool());
    ui->checkBox_TCPServerRecordTime->setChecked(settings.value("TCPServer/read/RecordTime", false).toBool());
    ui->checkBox_TCPServerRecordSend->setChecked(settings.value("TCPServer/read/RecordSend", false).toBool());
    ui->checkBox_TCPServerReadCurSor->setChecked(settings.value("TCPServer/read/ReadCurSor", false).toBool());

    ui->checkBox_TCPServerSendHex->setChecked(settings.value("TCPServer/send/SendHex", false).toBool());
    ui->checkBox_TCPServerSendAddLine->setChecked(settings.value("TCPServer/send/SendAddLine", false).toBool());
    ui->spinBox_TCPServerSendCycle->setValue(settings.value("TCPServer/send/SendCycleValue", 1000).toInt());
    ui->checkBox_TCPServerSendCyCleHex->setChecked(settings.value("TCPServer/send/SendCyCleHex", false).toBool());
    ui->lineEdit_TCPServerSendCycleText->setText(settings.value("TCPServer/send/SendCycleText").toString());
    ui->checkBox_TCPServerSendAutoClear->setChecked(settings.value("TCPServer/send/SendAutoClear", false).toBool());
    ui->checkBox_TCPServerEnterSend->setChecked(settings.value("TCPServer/send/EnterSend", false).toBool());
    ui->checkBox_TCPServerSendHistory->setChecked(settings.value("TCPServer/send/SendHistory", true).toBool());
    ui->textEdit_TCPServerSend->setText(settings.value("TCPServer/send/Send").toString());
    uint TCPServerSendTextFormat = settings.value("TCPServer/send/TextFormat", 0).toUInt();
    switch (TCPServerSendTextFormat) {
    case 0: g_emTCPServerSendTextFormat = Local; break;
    case 1: g_emTCPServerSendTextFormat = UTF8; break;
    case 2: g_emTCPServerSendTextFormat = GBK; break;
    case 3: g_emTCPServerSendTextFormat = Unicode; break;
    }

    g_bTCPClientAutoConnect = settings.value("TCPClient/AutoConnect", false).toBool();
    g_sTCPClientConnectIP = settings.value("TCPClient/ConnectIP", "127.0.0.1").toString();
    g_un16TCPClientConnectPort = static_cast<quint16>(settings.value("TCPClient/ConnectPort", 3620).toInt());
    //qDebug() << "Server IP:" << g_sTCPServerListenIP;
    ui->lineEdit_TCPClientConnectIP->setText(g_sTCPClientConnectIP);
    ui->lineEdit_TCPClientConnectPort->setText(QString(g_un16TCPClientConnectPort));
    g_unTCPClientReadDataLimit = static_cast<uint>(settings.value("TCPClient/ReadDataLimit", 0).toInt());
    g_bTCPClientDataAutoClear = settings.value("TCPClient/DataAutoClear", false).toBool();
    g_unTCPClientHistoryDatanumsLimit = static_cast<uint>(settings.value("TCPClient/HistoryDatanumsLimit", 0).toInt());
    g_unTCPClientReadnums = static_cast<uint>(settings.value("TCPClient/Readnums", 0).toInt());
    g_unTCPClientSendnums = static_cast<uint>(settings.value("TCPClient/Sendnums", 0).toInt());

    ui->checkBox_TCPClientReadHex->setChecked(settings.value("TCPClient/read/ReadHex", false).toBool());
    ui->checkBox_TCPClientReadAutoLine->setChecked(settings.value("TCPClient/read/ReadAutoLine", false).toBool());
    ui->checkBox_TCPClientStopShow->setChecked(settings.value("TCPClient/read/StopShow", false).toBool());
    ui->checkBox_TCPClientRecordTime->setChecked(settings.value("TCPClient/read/RecordTime", false).toBool());
    ui->checkBox_TCPClientRecordSend->setChecked(settings.value("TCPClient/read/RecordSend", false).toBool());
    ui->checkBox_TCPClientReadCurSor->setChecked(settings.value("TCPClient/read/ReadCurSor", false).toBool());

    ui->checkBox_TCPClientSendHex->setChecked(settings.value("TCPClient/send/SendHex", false).toBool());
    ui->checkBox_TCPClientSendAddLine->setChecked(settings.value("TCPClient/send/SendAddLine", false).toBool());
    ui->spinBox_TCPClientSendCycle->setValue(settings.value("TCPClient/send/SendCycleValue", 1000).toInt());
    ui->checkBox_TCPClientSendCyCleHex->setChecked(settings.value("TCPClient/send/SendCyCleHex", false).toBool());
    ui->lineEdit_TCPClientSendCycleText->setText(settings.value("TCPClient/send/SendCycleText").toString());
    ui->checkBox_TCPClientSendAutoClear->setChecked(settings.value("TCPClient/send/SendAutoClear", false).toBool());
    ui->checkBox_TCPClientEnterSend->setChecked(settings.value("TCPClient/send/EnterSend", false).toBool());
    ui->checkBox_TCPClientSendHistory->setChecked(settings.value("TCPClient/send/SendHistory", true).toBool());
    ui->textEdit_TCPClientSend->setText(settings.value("TCPClient/send/Send").toString());
    uint TCPClientSendTextFormat = settings.value("TCPServer/send/TextFormat", 0).toUInt();
    switch (TCPClientSendTextFormat) {
    case 0: g_emTCPClientSendTextFormat = Local; break;
    case 1: g_emTCPClientSendTextFormat = UTF8; break;
    case 2: g_emTCPClientSendTextFormat = GBK; break;
    case 3: g_emTCPClientSendTextFormat = Unicode; break;
    }
}

///
/// \brief 保存设置
///
void CommDubugAssist::SaveSettings()
{
    QSettings settings;
    settings.setValue("VERSION", VERSION);

    settings.beginGroup("universal");
    settings.setValue("AutoSaveSettings", g_bAutoSaveSettings);
    settings.setValue("QSSPath", g_sInitQSSPath);
    settings.setValue("ConnectSucceedRemind", g_bConnectSucceedRemind);
    settings.setValue("ConnectFailRemind", g_bConnectFailRemind);
    settings.setValue("DisconnectRemind", g_bDisconnectRemind);
    settings.setValue("AccidentDisconnectRemind", g_bAccidentDisconnectRemind);
    settings.endGroup();

    settings.beginGroup("SerialPort");
    settings.setValue("AutoConnect", g_bSerialPortAutoConnect);
    settings.setValue("ConnectName", g_sSerialPortConnectName);
    settings.setValue("BaudIndex", g_nSerialPortBaudIndex);
    settings.setValue("DataIndex", g_nSerialPortDataIndex);
    settings.setValue("ControlIndex", g_nSerialPortControlIndex);
    settings.setValue("ParityIndex", g_nSerialPortParityIndex);
    settings.setValue("StopIndex", g_nSerialPortStopIndex);
    settings.setValue("ReadDataLimit", g_unSerialPortReadDataLimit);
    settings.setValue("DataAutoClear", g_bSerialPortDataAutoClear);
    settings.setValue("HistoryDatanumsLimit", g_unSerialPortHistoryDatanumsLimit);
    settings.setValue("Readnums", g_unSerialPortReadnums);
    settings.setValue("Sendnums", g_unSerialPortSendnums);
    settings.beginGroup("read");
    settings.setValue("ReadHex", ui->checkBox_SerialPortReadHex->isChecked());
    settings.setValue("ReadAutoLine", ui->checkBox_SerialPortReadAutoLine->isChecked());
    settings.setValue("StopShow", ui->checkBox_SerialPortStopShow->isChecked());
    settings.setValue("RecordTime", ui->checkBox_SerialPortRecordTime->isChecked());
    settings.setValue("RecordSend", ui->checkBox_SerialPortRecordSend->isChecked());
    settings.setValue("ReadCurSor", ui->checkBox_SerialPortReadCurSor->isChecked());
    settings.endGroup();
    settings.beginGroup("send");
    settings.setValue("SendHex", ui->checkBox_SerialPortSendHex->isChecked());
    settings.setValue("SendAddLine", ui->checkBox_SerialPortSendAddLine->isChecked());
    settings.setValue("SendCycleValue", ui->spinBox_SerialPortSendCycle->value());
    settings.setValue("SendCyCleHex", ui->checkBox_SerialPortSendCyCleHex->isChecked());
    settings.setValue("SendCycleText", ui->lineEdit_SerialPortSendCycleText->text());
    settings.setValue("SendAutoClear", ui->checkBox_SerialPortSendAutoClear->isChecked());
    settings.setValue("EnterSend", ui->checkBox_SerialPortEnterSend->isChecked());
    settings.setValue("SendHistory", ui->checkBox_SerialPortSendHistory->isChecked());
    settings.setValue("Send", ui->textEdit_SerialPortSend->toPlainText());
    settings.setValue("TextFormat", g_emSerialPortSendTextFormat);
    settings.endGroup();
    settings.endGroup();

    settings.beginGroup("TCPServer");
    settings.setValue("AutoListen", g_bTCPServerAutoListen);
    settings.setValue("ListenIP", g_sTCPServerListenIP);
    settings.setValue("ListenPort", g_un16TCPServerListenPort);
    settings.setValue("ReadDataLimit", g_unTCPServerReadDataLimit);
    settings.setValue("DataAutoClear", g_bTCPServerDataAutoClear);
    settings.setValue("HistoryDatanumsLimit", g_unTCPServerHistoryDatanumsLimit);
    settings.setValue("Readnums", g_unTCPServerReadnums);
    settings.setValue("Sendnums", g_unTCPServerSendnums);
    settings.beginGroup("read");
    settings.setValue("ReadHex", ui->checkBox_TCPServerReadHex->isChecked());
    settings.setValue("ReadAutoLine", ui->checkBox_TCPServerReadAutoLine->isChecked());
    settings.setValue("StopShow", ui->checkBox_TCPServerStopShow->isChecked());
    settings.setValue("RecordTime", ui->checkBox_TCPServerRecordTime->isChecked());
    settings.setValue("RecordSend", ui->checkBox_TCPServerRecordSend->isChecked());
    settings.setValue("ReadCurSor", ui->checkBox_TCPServerReadCurSor->isChecked());
    settings.endGroup();
    settings.beginGroup("send");
    settings.setValue("SendHex", ui->checkBox_TCPServerSendHex->isChecked());
    settings.setValue("SendAddLine", ui->checkBox_TCPServerSendAddLine->isChecked());
    settings.setValue("SendCycleValue", ui->spinBox_TCPServerSendCycle->value());
    settings.setValue("SendCycleTarget", ui->comboBox_TCPServerSendCycleTarget->currentText());
    settings.setValue("SendCyCleHex", ui->checkBox_TCPServerSendCyCleHex->isChecked());
    settings.setValue("SendCycleText", ui->lineEdit_TCPServerSendCycleText->text());
    settings.setValue("SendAutoClear", ui->checkBox_TCPServerSendAutoClear->isChecked());
    settings.setValue("EnterSend", ui->checkBox_TCPServerEnterSend->isChecked());
    settings.setValue("SendHistory", ui->checkBox_TCPServerSendHistory->isChecked());
    settings.setValue("Send", ui->textEdit_TCPServerSend->toPlainText());
    settings.setValue("TextFormat", g_emTCPServerSendTextFormat);
    settings.endGroup();
    settings.endGroup();

    settings.beginGroup("TCPClient");
    settings.setValue("AutoConnect", g_bTCPClientAutoConnect);
    settings.setValue("ConnectIP", g_sTCPClientConnectIP);
    settings.setValue("ConnectPort", g_un16TCPClientConnectPort);
    settings.setValue("ReadDataLimit", g_unTCPClientReadDataLimit);
    settings.setValue("DataAutoClear", g_bTCPClientDataAutoClear);
    settings.setValue("HistoryDatanumsLimit", g_unTCPClientHistoryDatanumsLimit);
    settings.setValue("Readnums", g_unTCPClientReadnums);
    settings.setValue("Sendnums", g_unTCPClientSendnums);
    settings.beginGroup("read");
    settings.setValue("ReadHex", ui->checkBox_TCPClientReadHex->isChecked());
    settings.setValue("ReadAutoLine", ui->checkBox_TCPClientReadAutoLine->isChecked());
    settings.setValue("StopShow", ui->checkBox_TCPClientStopShow->isChecked());
    settings.setValue("RecordTime", ui->checkBox_TCPClientRecordTime->isChecked());
    settings.setValue("RecordSend", ui->checkBox_TCPClientRecordSend->isChecked());
    settings.setValue("ReadCurSor", ui->checkBox_TCPClientReadCurSor->isChecked());
    settings.endGroup();
    settings.beginGroup("send");
    settings.setValue("SendHex", ui->checkBox_TCPClientSendHex->isChecked());
    settings.setValue("SendAddLine", ui->checkBox_TCPClientSendAddLine->isChecked());
    settings.setValue("SendCycleValue", ui->spinBox_TCPClientSendCycle->value());
    settings.setValue("SendCyCleHex", ui->checkBox_TCPClientSendCyCleHex->isChecked());
    settings.setValue("SendCycleText", ui->lineEdit_TCPClientSendCycleText->text());
    settings.setValue("SendAutoClear", ui->checkBox_TCPClientSendAutoClear->isChecked());
    settings.setValue("EnterSend", ui->checkBox_TCPClientEnterSend->isChecked());
    settings.setValue("SendHistory", ui->checkBox_TCPClientSendHistory->isChecked());
    settings.setValue("Send", ui->textEdit_TCPClientSend->toPlainText());
    settings.setValue("TextFormat", g_emTCPClientSendTextFormat);
    settings.endGroup();
    settings.endGroup();
}

void CommDubugAssist::ResetSettings()
{
    if (g_bIndependent)
    {
        g_bAutoSaveSettings = false;
        g_sInitQSSPath = ":/qss/bluewhite/bluewhite.qss";

        g_bConnectSucceedRemind = false;
        g_bConnectFailRemind = true;
        g_bDisconnectRemind = false;
        g_bAccidentDisconnectRemind = true;

        g_bSerialPortAutoConnect = false;
        g_sSerialPortConnectName.clear();
        g_nSerialPortBaudIndex = 3;
        g_nSerialPortDataIndex = 3;
        g_nSerialPortControlIndex = 0;
        g_nSerialPortParityIndex = 0;
        g_nSerialPortStopIndex = 0;
        g_unSerialPortReadDataLimit = 0;
        g_bSerialPortDataAutoClear = false;
        g_unSerialPortHistoryDatanumsLimit = 0;
        g_unSerialPortReadnums = 0;
        g_unSerialPortSendnums = 0;

        ui->checkBox_SerialPortReadHex->setChecked(false);
        ui->checkBox_SerialPortReadAutoLine->setChecked(false);
        ui->checkBox_SerialPortStopShow->setChecked(false);
        ui->checkBox_SerialPortRecordTime->setChecked(false);
        ui->checkBox_SerialPortRecordSend->setChecked(false);
        ui->checkBox_SerialPortReadCurSor->setChecked(false);

        ui->checkBox_SerialPortSendHex->setChecked(false);
        ui->checkBox_SerialPortSendAddLine->setChecked(false);
        ui->spinBox_SerialPortSendCycle->setValue(1000);
        ui->checkBox_SerialPortSendCyCleHex->setChecked(false);
        ui->lineEdit_SerialPortSendCycleText->clear();
        ui->checkBox_SerialPortSendAutoClear->setChecked(false);
        ui->checkBox_SerialPortEnterSend->setChecked(false);
        ui->checkBox_SerialPortSendHistory->setChecked(true);
        ui->textEdit_SerialPortSend->clear();

        g_bTCPServerAutoListen = false;
        g_sTCPServerListenIP = "127.0.0.1";
        g_un16TCPServerListenPort = 3620;
        g_unTCPServerReadDataLimit = 0;
        g_bTCPServerDataAutoClear = false;
        g_unTCPServerHistoryDatanumsLimit = 0;
        g_unTCPServerReadnums = 0;
        g_unTCPServerSendnums = 0;

        ui->checkBox_TCPServerReadHex->setChecked(false);
        ui->checkBox_TCPServerReadAutoLine->setChecked(false);
        ui->checkBox_TCPServerStopShow->setChecked(false);
        ui->checkBox_TCPServerRecordTime->setChecked(false);
        ui->checkBox_TCPServerRecordSend->setChecked(false);
        ui->checkBox_TCPServerReadCurSor->setChecked(false);

        ui->checkBox_TCPServerSendHex->setChecked(false);
        ui->checkBox_TCPServerSendAddLine->setChecked(false);
        ui->spinBox_TCPServerSendCycle->setValue(1000);
        ui->checkBox_TCPServerSendCyCleHex->setChecked(false);
        ui->lineEdit_TCPServerSendCycleText->clear();
        ui->checkBox_TCPServerSendAutoClear->setChecked(false);
        ui->checkBox_TCPServerEnterSend->setChecked(false);
        ui->checkBox_TCPServerSendHistory->setChecked(true);
        ui->textEdit_TCPServerSend->clear();

        g_bTCPClientAutoConnect = false;
        g_sTCPClientConnectIP = "127.0.0.1";
        g_un16TCPClientConnectPort = 3620;
        g_unTCPClientReadDataLimit = 0;
        g_bTCPClientDataAutoClear = false;
        g_unTCPClientHistoryDatanumsLimit = 0;
        g_unTCPClientReadnums = 0;
        g_unTCPClientSendnums = 0;

        ui->checkBox_TCPClientReadHex->setChecked(false);
        ui->checkBox_TCPClientReadAutoLine->setChecked(false);
        ui->checkBox_TCPClientStopShow->setChecked(false);
        ui->checkBox_TCPClientRecordTime->setChecked(false);
        ui->checkBox_TCPClientRecordSend->setChecked(false);
        ui->checkBox_TCPClientReadCurSor->setChecked(false);

        ui->checkBox_TCPClientSendHex->setChecked(false);
        ui->checkBox_TCPClientSendAddLine->setChecked(false);
        ui->spinBox_TCPClientSendCycle->setValue(1000);
        ui->checkBox_TCPClientSendCyCleHex->setChecked(false);
        ui->lineEdit_TCPClientSendCycleText->clear();
        ui->checkBox_TCPClientSendAutoClear->setChecked(false);
        ui->checkBox_TCPClientEnterSend->setChecked(false);
        ui->checkBox_TCPClientSendHistory->setChecked(true);
        ui->textEdit_TCPClientSend->clear();
    }
    else
    {
        QSettings settings;
        settings.clear();
        ReadSettings();
        SaveSettings();
        w_Pref->ReadSettings();
    }
    Reset();
}

///
/// \brief 检查更新
/// \return
///
bool CommDubugAssist::ReadUpDate()
{
    QNetworkAccessManager manager;
    QEventLoop loop;
    QNetworkReply *reply;

    bool ReadOK = true;

    QUrl m_url;
    QString NewVer;

    //m_url.setUrl("http://danpey.github.io/UpdateFile/DCDA_Update.xml");
    m_url.setUrl("file:///C:/Users/Danpe/Desktop/DCDA_Update.xml");
    reply = manager.get(QNetworkRequest(m_url));

    QReplyTimeout *pTimeout = new QReplyTimeout(reply, 600);
    connect(pTimeout, &QReplyTimeout::timeout, [=]()mutable{
        ReadOK = false;
        //qDebug() << "Read UpDate Timeout";
    });

    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    if (ReadOK)
    {
        QString updatecode = reply->readAll();

        QDomDocument doc;
        if (doc.setContent(updatecode))
        {
            QDomElement root = doc.documentElement();
            QDomNode dcdadatas = root.childNodes().at(0);
            if (dcdadatas.hasAttributes())
            {
                QDomNamedNodeMap attrs = dcdadatas.attributes();
                //qDebug() << "attrs count:" << attrs.count();
                for (int i = 0; i < attrs.count(); i++)
                {
                    QDomNode n = attrs.item(i);
                    QString nodeName = n.nodeName();
                    QString nodeValue = n.nodeValue();
                    //qDebug() << "第" << i << "个：nodeName" << nodeName << ", nodeValue" << nodeValue;
                    if (nodeName == "VERSION")
                    {
                        if (!nodeValue.isEmpty())
                        {
                            NewVer = nodeValue;
                            if (NewVer != VERSION)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    return false;
}

///
/// \brief 菜单栏执行关闭
///
void CommDubugAssist::on_action_Close_triggered()
{
    m_selMainWidget->close();
}

///
/// \brief 强制关闭软件
///
void CommDubugAssist::ForceExit()
{
    m_bForceExit = true;
    m_selMainWidget->close();
}

///
/// \brief 关闭软件执行程序
/// \param event
///
void CommDubugAssist::closeEvent(QCloseEvent *event)
{
    bool isrun = false;
    QString strserial, strtcps, strtcpc;
    if (m_serialPort->isOpen())
    {
        isrun = true;
        strserial = "串口处于连接状态";
    }
    if (m_TcpServerClientDatas.count() > 1 || m_tcpserver->isListening())
    {
        isrun = true;
        if (m_TcpServerClientDatas.count() > 1)
        {
            strtcps = "TCP服务端尚有客户端连接";
        }
        else
        {
            strtcps = "TCP服务端正在监听";
        }
    }
    if (m_tcpclient->isOpen())
    {
        isrun = true;
        if (m_tcpclient->isConnect())
        {
            strtcpc = "TCP客户端处于连接状态";
        }
        else
        {
            strtcpc = "TCP客户端尚在等待连接";
        }
    }
    QString str;
    if (!strserial.isEmpty())
    {
        str = strserial;
    }
    if (!strtcps.isEmpty())
    {
        if (str.isEmpty())
        {
            str = strtcps;
        }
        else
        {
            str = str + "，" + strtcps;
        }
    }
    if (!strtcpc.isEmpty())
    {
        if (str.isEmpty())
        {
            str = strtcpc;
        }
        else
        {
            str = str + "，" + strtcpc;
        }
    }
    if (isrun)
    {
        bool isOK = true;
        if (!m_bForceExit)   // 是否强制关闭，不强制关闭则进行询问
        {
            isOK = AskInform(str + "！是否确认关闭软件？");
        }
        if (!isOK)
        {
            event->ignore();
            return ;
        }
        if (!strserial.isEmpty())
        {
            m_serialPort->clear();
            m_serialPort->close();
        }
        if (!strtcps.isEmpty())
        {
            m_tcpserver->DisConnectALLClients();
        }
        if (!strtcpc.isEmpty())
        {
            m_tcpclient->SetCloseOver(true);
            m_tcpclient->Disconnected();
        }
    }
    w_SP_CycleThread->exit();
    w_TCPC_CycleThread->exit();
    w_TCPS_CycleThread->exit();
    if (w_Pref->isVisible())
    {
        w_Pref->close();
    }
    if (w_About->isVisible())
    {
        w_About->close();
    }
    if (w_Help->isVisible())
    {
        w_Help->close();
    }
    if (w_clo->isVisible())
    {
        w_clo->close();
    }
    if (g_bSerialPortDataAutoClear)
    {
        g_unSerialPortReadnums = 0;
        g_unSerialPortSendnums = 0;
    }
    if (g_bTCPServerDataAutoClear)
    {
        g_unTCPServerReadnums = 0;
        g_unTCPServerSendnums = 0;
    }
    if (g_bTCPClientDataAutoClear)
    {
        g_unTCPClientReadnums = 0;
        g_unTCPClientSendnums = 0;
    }
    if (!g_bIndependent)
    {
        if (g_bAutoSaveSettings)
        {
            SaveSettings();
        }
    }
    event->accept();
}

///
/// \brief 打开换装界面
///
void CommDubugAssist::OpenClothes()
{
    if (w_clo->isVisible())
    {
        w_clo->close();
    }
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_clo->windowFlags();
        w_clo->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        w_clo->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_clo->show();
}

///
/// \brief 拖动文件事件
/// \param event
///
void CommDubugAssist::dragEnterEvent(QDragEnterEvent *event)
{
    QString filename = event->mimeData()->urls().first().fileName();

    //qDebug() << "filename:" << event->mimeData()->urls()[0].fileName();
//    if (filename.right(4).toLower() == ".txt" ||
//            filename.right(5).toLower() == ".html" ||
//            filename.right(4).toLower() == ".xml" ||
//            filename.right(4).toLower() == ".log" ||
//            filename.right(4).toLower() == ".php")
    if (event->mimeData()->hasFormat("text/uri-list"))
    {
        if (ui->tabWidget_main->currentIndex() == 0 && ui->tabWidget_SerialPort->currentIndex() == 0)
        {
            event->acceptProposedAction();  // 允许放下文件
        }
        else if (ui->tabWidget_main->currentIndex() == 1 && ui->tabWidget_TCPServer->currentIndex() == 0)
        {
            event->acceptProposedAction();
        }
        else if (ui->tabWidget_main->currentIndex() == 2 && ui->tabWidget_TCPClient->currentIndex() == 0)
        {
            event->acceptProposedAction();
        }
    }
}

///
/// \brief 放下文件事件
/// \param event
///
void CommDubugAssist::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls = event->mimeData()->urls();
    if (urls.isEmpty())
    {
        return ;
    }
    QString fileName = urls.first().toLocalFile();
    if (fileName.isEmpty())
    {
        return ;
    }
    QFile file(fileName);
    bool isOK = file.open(QFile::ReadOnly);
    if (!isOK)
    {
        return ;
    }
    QByteArray readdata = file.readAll();
    if (ui->tabWidget_main->currentIndex() == 0 && ui->tabWidget_SerialPort->currentIndex() == 0)
    {
        ui->textEdit_SerialPortSend->append(QString::fromUtf8(readdata));
    }
    else if (ui->tabWidget_main->currentIndex() == 1 && ui->tabWidget_TCPServer->currentIndex() == 0)
    {
        ui->textEdit_TCPServerSend->append(QString::fromUtf8(readdata));
    }
    else if (ui->tabWidget_main->currentIndex() == 2 && ui->tabWidget_TCPClient->currentIndex() == 0)
    {
        ui->textEdit_TCPClientSend->append(QString::fromUtf8(readdata));
    }
    file.close();
}

///
/// \brief 串口快捷发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::SerialPortQuickSendMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    QString tooltip = m_SerialPortQuickSendDatas[row].writedata;
    ui->tableView_SerialPortQuickSend->setToolTip(tooltip);
}

///
/// \brief 串口互动发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::SerialPortInteractMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    QString tooltip = m_staSerialPortInteract->item(row, column)->text();
    ui->tableView_SerialPortInteract->setToolTip(tooltip);
}

///
/// \brief 串口高级循环发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::SerialPortSeniorSendCycleMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    if (column == 0)
    {
        QString tooltip = m_SerialPortCycleDatas[row].senddata;
        ui->tableView_SerialPortSeniorSendCycle->setToolTip(tooltip);
    }
    else
    {
        QString tooltip = m_staSerialPortSeniorCycle->item(row, column)->text();
        ui->tableView_SerialPortSeniorSendCycle->setToolTip(tooltip);
    }
}

///
/// \brief 串口历史发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::SerialPortHistoryMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    QString tooltip = m_staSerialportHistoryModel->item(row, column)->text();
    ui->tableView_SerialPortSendHistory->setToolTip(tooltip);
}

///
/// \brief TCP服务端快捷发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPServerQuickSendMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    if (column == 0)
    {
        QString tooltip = m_staTCPServerQuickSend->item(row, 0)->text();
        ui->tableView_TCPServerQuickSend->setToolTip(tooltip);
    }
    else
    {
        QString tooltip = m_TCPServerQuickSendDatas[row].writedata;
        ui->tableView_TCPServerQuickSend->setToolTip(tooltip);
    }
}

///
/// \brief TCP服务端互动发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPServerInteractMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    if (column == 2)
    {
        QString tooltip;
        if (m_TCPServerInteractDatas[row].readip == "ALL Connections")
        {
            tooltip = m_TCPServerInteractDatas[row].readip;
        }
        else
        {
            tooltip = m_TCPServerInteractDatas[row].readip + ":" + QString::number(m_TCPServerInteractDatas[row].readport);
        }
        ui->tableView_TCPServerInteract->setToolTip(tooltip);
    }
    else if (column == 4)
    {
        QString tooltip;
        if (m_TCPServerInteractDatas[row].writeip == "ALL Connections")
        {
            tooltip = m_TCPServerInteractDatas[row].writeip;
        }
        else
        {
            tooltip = m_TCPServerInteractDatas[row].writeip + ":" + QString::number(m_TCPServerInteractDatas[row].writeport);
        }
        ui->tableView_TCPServerInteract->setToolTip(tooltip);
    }
    else
    {
        QString tooltip = m_staTCPServerInteract->item(row, column)->text();
        ui->tableView_TCPServerInteract->setToolTip(tooltip);
    }
}

///
/// \brief TCP服务端高级循环发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPServerSeniorSendCycleMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    if (column == 0)
    {
        QString tooltip = m_TCPServerCycleDatas[row].senddata;
        ui->tableView_TCPServerSeniorSendCycle->setToolTip(tooltip);
    }
    else
    {
        QString tooltip;
        if (m_TCPServerCycleDatas[row].ip == "ALL Connections")
        {
            tooltip = m_TCPServerCycleDatas[row].ip;
        }
        else
        {
            tooltip = m_TCPServerCycleDatas[row].ip + ":" + QString::number(m_TCPServerCycleDatas[row].port);
        }
        ui->tableView_TCPServerSeniorSendCycle->setToolTip(tooltip);
    }
}

///
/// \brief TCP服务端历史发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPServerHistoryMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    QString tooltip = m_staTCPServerHistoryModel->item(row, column)->text();
    ui->tableView_TCPServerSendHistory->setToolTip(tooltip);
}

///
/// \brief TCP客户端快捷发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPClientQuickSendMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    QString tooltip = m_TCPClientQuickSendDatas[row].writedata;
    ui->tableView_TCPClientQuickSend->setToolTip(tooltip);
}

///
/// \brief TCP客户端互动发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPClientInteractMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    QString tooltip = m_staTCPClientInteract->item(row, column)->text();
    ui->tableView_TCPClientInteract->setToolTip(tooltip);
}

///
/// \brief \brief TCP客户端高级循环发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPClientSeniorSendCycleMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    if (column == 0)
    {
        QString tooltip = m_TCPClientCycleDatas[row].senddata;
        ui->tableView_TCPClientSeniorSendCycle->setToolTip(tooltip);
    }
    else
    {
        QString tooltip = m_staTCPClientSeniorCycle->item(row, column)->text();
        ui->tableView_TCPClientSeniorSendCycle->setToolTip(tooltip);
    }
}

///
/// \brief TCP客户端历史发送表格获取鼠标位置的单元格
/// \param index
///
void CommDubugAssist::TCPClientHistoryMoveEntered(const QModelIndex & index)
{
    int row = index.row();
    int column = index.column();
    QString tooltip = m_staTCPClientHistoryModel->item(row, column)->text();
    ui->tableView_TCPClientSendHistory->setToolTip(tooltip);
}

//void CommDubugAssist::textEdit_SerialPortSend_dragEnterEvent(QDragEnterEvent *event)
//{
//    if (event->mimeData()->hasFormat("text/uri-list"))
//    {
//        event->acceptProposedAction();  // 允许放下文件
//    }
//}

//void CommDubugAssist::textEdit_SerialPortSend_dropEvent(QDropEvent *event)
//{
//    QList<QUrl> urls = event->mimeData()->urls();
//    if (urls.isEmpty())
//    {
//        return ;
//    }
//    QString fileName = urls.first().toLocalFile();
//    if (fileName.isEmpty())
//    {
//        return ;
//    }
//    QFile file(fileName);
//    bool isOK = file.open(QFile::ReadOnly);
//    if (!isOK)
//    {
//        return ;
//    }
//    QByteArray readdata = file.readAll();

//    ui->textEdit_SerialPortSend->setText(QString::fromUtf8(readdata));

//    file.close();
//}

///
/// \brief 正在开发中的界面提示
///
void CommDubugAssist::Developing()
{
    Inform("慢着，蛋皮已经在疯狂敲代(mo)码(yu)了，先试试其他功能吧~");
}

///
/// \brief 提供建议与提交bug
///
void CommDubugAssist::Bug()
{
    bool isOK = AskInform("即将跳转去往网页：https://lijianxun.cn/2020/04/22/248/，是否确定？");
    if (isOK)
    {
        QDesktopServices::openUrl(QUrl(QLatin1String("https://lijianxun.cn/2020/04/22/248/")));
    }
}

///
/// \brief 后台默认定时器
///
void CommDubugAssist::MyTimerEvent()
{
    if (!m_serialPort->isOpen())  // 当串口没有连接时
    {
        //ui->comboBox_SerialPortName->clear();
        QList<QString> serialportnames;
        bool isOK = false;
        QString choosename = ui->comboBox_SerialPortName->currentText();
        foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
            QString showserial = QString("%1:%2").arg(info.portName()).arg(info.description());
            if (showserial == choosename)
            {
                isOK = true;
            }
            serialportnames.append(showserial);
            //ui->comboBox_SerialPortName->addItem(showserial);
        }
        if (serialportnames.count() != ui->comboBox_SerialPortName->count())
        {
            //m_listSerialPortNames.clear();
            ui->comboBox_SerialPortName->clear();
            //m_listSerialPortNames.append(serialportnames);
            ui->comboBox_SerialPortName->addItems(serialportnames);
            if (isOK)
            {
                ui->comboBox_SerialPortName->setCurrentText(choosename);
            }
        }
        else
        {
            bool issame = true;
            for (int i = 0; i < serialportnames.count(); i++)
            {
                if (ui->comboBox_SerialPortName->itemText(i) != serialportnames[i])
                {
                    issame = false;
                    break;
                }
            }
            if (!issame)
            {
                //m_listSerialPortNames.clear();
                ui->comboBox_SerialPortName->clear();
                //m_listSerialPortNames.append(serialportnames);
                ui->comboBox_SerialPortName->addItems(serialportnames);
                if (isOK)
                {
                    ui->comboBox_SerialPortName->setCurrentText(choosename);
                }
            }
        }
    }
    else
    {
        bool isOK = false;
        QString choosename = ui->comboBox_SerialPortName->currentText();
        foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
            QString showserial = QString("%1:%2").arg(info.portName()).arg(info.description());
            if (showserial == choosename)
            {
                isOK = true;
                break;
            }
        }
        if (!isOK)
        {
            if (m_timerSerialPortCycle->isActive())
            {
                m_timerSerialPortCycle->stop();
                ui->checkBox_SerialPortSendCyCle->setChecked(false);
                ui->lineEdit_SerialPortSendCycleText->setReadOnly(false);
                ui->spinBox_SerialPortSendCycle->setReadOnly(false);
            }
            if (ui->btn_SerialPortSeniorSendCycleStart->text() == "终止循环")
            {
                ui->btn_SerialPortSeniorSendCycleStart->setText("开始循环");
                w_SP_Cycle->SetContinue(false);
                ui->label_SerialPortSeniorSendCycle->setText("当前无执行事件");
            }
            m_serialPort->clear();
            m_serialPort->close();
            ui->btn_SerialPortConnect->setText("连接");
            ui->label_SerialPortConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(0, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
            ui->comboBox_SerialPortName->setEnabled(true);
            ui->comboBox_SerialPortBaud->setEnabled(true);
            ui->comboBox_SerialPortData->setEnabled(true);
            ui->comboBox_SerialPortParity->setEnabled(true);
            ui->comboBox_SerialPortStop->setEnabled(true);
            ui->comboBox_SerialPortFlowControl->setEnabled(true);
            Inform("串口连接异常中断！请检查设备连接是否正常！");
        }
    }
    if (m_bFirstOpen)
    {
        m_bFirstOpen = false;
        w_up->show();
    }
}

///
/// \brief 串口发送历史记录表格初始化
///
void CommDubugAssist::SerialPortSendHistoryInit()
{
    m_staSerialportHistoryModel = new QStandardItemModel();
    m_staSerialportHistoryModel->setColumnCount(3);
    m_staSerialportHistoryModel->setHeaderData(0, Qt::Horizontal, QStringLiteral("时间"));
    m_staSerialportHistoryModel->setHeaderData(1, Qt::Horizontal, QStringLiteral("数据类型"));
    m_staSerialportHistoryModel->setHeaderData(2, Qt::Horizontal, QStringLiteral("发送内容"));
    ui->tableView_SerialPortSendHistory->setModel(m_staSerialportHistoryModel);
    ui->tableView_SerialPortSendHistory->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);  // 表头信息显示居中
    ui->tableView_SerialPortSendHistory->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed); // Fixed 为设置列宽不可变
    ui->tableView_SerialPortSendHistory->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed); // Stretch 为设置列宽可变
    ui->tableView_SerialPortSendHistory->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableView_SerialPortSendHistory->setColumnWidth(0, 160);
    ui->tableView_SerialPortSendHistory->setColumnWidth(1, 55);// 设置列的宽度
    ui->tableView_SerialPortSendHistory->setColumnWidth(2, 100);

    ui->tableView_SerialPortSendHistory->setSelectionMode(QAbstractItemView::SingleSelection); // 设定只能选择一行，不能选择多行
    ui->tableView_SerialPortSendHistory->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_SerialPortSendHistory->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief 串口发送增加一条历史记录
/// \param datatime：时间
/// \param type：数据类型
/// \param senddata：发送数据
///
void CommDubugAssist::SerialPortAddSendHistory(QString datatime, QString type, QString senddata)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(datatime));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(type));
    QStandardItem *standItem3 = new QStandardItem(tr("%1").arg(senddata));
    m_staSerialportHistoryModel->insertRow(0);
    m_staSerialportHistoryModel->setItem(0, 0, standItem1);
    m_staSerialportHistoryModel->setItem(0, 1, standItem2);
    m_staSerialportHistoryModel->setItem(0, 2, standItem3);
    m_staSerialportHistoryModel->item(0, 0)->setTextAlignment(Qt::AlignCenter);
    m_staSerialportHistoryModel->item(0, 1)->setTextAlignment(Qt::AlignCenter);
    m_staSerialportHistoryModel->item(0, 2)->setTextAlignment(Qt::AlignVCenter);
    if (g_unSerialPortHistoryDatanumsLimit > 0)
    {
        while (m_staSerialportHistoryModel->rowCount() > static_cast<int>(g_unSerialPortHistoryDatanumsLimit))
        {
            m_staSerialportHistoryModel->removeRow(m_staSerialportHistoryModel->rowCount() - 1);
        }
    }
}

///
/// \brief 串口快捷发送表格初始化
///
void CommDubugAssist::SerialPortQuickSendInit()
{
    m_staSerialPortQuickSend = new QStandardItemModel();
    m_staSerialPortQuickSend->setColumnCount(1);
    m_staSerialPortQuickSend->setHeaderData(0, Qt::Horizontal, QStringLiteral("快捷发送"));
    ui->tableView_SerialPortQuickSend->setModel(m_staSerialPortQuickSend);
    ui->tableView_SerialPortQuickSend->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);  // 表头信息显示居中
    ui->tableView_SerialPortQuickSend->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView_SerialPortQuickSend->setSelectionMode(QAbstractItemView::SingleSelection); // 设定只能选择一行，不能选择多行
    ui->tableView_SerialPortQuickSend->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_SerialPortQuickSend->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
    ui->tableView_SerialPortQuickSend->horizontalHeader()->hide();
}

///
/// \brief 串口快捷发送表格新增
/// \param name: 指令名字
///
void CommDubugAssist::SerialPortQuickSendAdd(QString name)
{
    QStandardItem *standItem = new QStandardItem(tr("%1").arg(name));
    m_staSerialPortQuickSend->appendRow(standItem);
    m_staSerialPortQuickSend->item(m_staSerialPortQuickSend->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief 串口快捷发送表格修改
/// \param row: 行号
/// \param name: 修改后的指令名字
///
void CommDubugAssist::SerialPortQuickSendRevise(int row, QString name)
{
    m_staSerialPortQuickSend->item(row)->setText(name);
}

///
/// \brief 串口快捷发送表格删除
/// \param row: 行号
///
void CommDubugAssist::SerialPortQuickSendDelete(int row)
{
    m_staSerialPortQuickSend->removeRow(row);
}

///
/// \brief 清空串口快捷发送表格
///
void CommDubugAssist::SerialPortQuickSendClear()
{
    if (m_staSerialPortQuickSend->rowCount() > 0)
    {
        m_staSerialPortQuickSend->removeRows(0, m_staSerialPortQuickSend->rowCount());
    }
}

///
/// \brief 串口交互发送表格初始化
///
void CommDubugAssist::SerialPortInteractInit()
{
    m_staSerialPortInteract = new QStandardItemModel();
    m_staSerialPortInteract->setColumnCount(5);
    m_staSerialPortInteract->setHeaderData(0, Qt::Horizontal, QStringLiteral("类型"));
    m_staSerialPortInteract->setHeaderData(1, Qt::Horizontal, QStringLiteral("接收"));
    m_staSerialPortInteract->setHeaderData(2, Qt::Horizontal, QStringLiteral("接收类型"));
    m_staSerialPortInteract->setHeaderData(3, Qt::Horizontal, QStringLiteral("发送"));
    m_staSerialPortInteract->setHeaderData(4, Qt::Horizontal, QStringLiteral("发送类型"));
    ui->tableView_SerialPortInteract->setModel(m_staSerialPortInteract);
    ui->tableView_SerialPortInteract->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView_SerialPortInteract->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    ui->tableView_SerialPortInteract->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tableView_SerialPortInteract->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
    ui->tableView_SerialPortInteract->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->tableView_SerialPortInteract->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Fixed);
    ui->tableView_SerialPortInteract->setColumnWidth(0, 80);
    ui->tableView_SerialPortInteract->setColumnWidth(1, 200);
    ui->tableView_SerialPortInteract->setColumnWidth(2, 60);
    ui->tableView_SerialPortInteract->setColumnWidth(3, 200);
    ui->tableView_SerialPortInteract->setColumnWidth(4, 60);
    ui->tableView_SerialPortInteract->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_SerialPortInteract->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_SerialPortInteract->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief 串口交互发送表格添加
/// \param si
///
void CommDubugAssist::SerialPortInteractAdd(InteractData si)
{
    QString strreadhex, strwritehex;
    if (si.readTextFormat == HEX)
    {
        strreadhex = "Hex";
    }
    else
    {
        strreadhex = "Ascii";
    }
    if (si.sendTextFormat == HEX)
    {
        strwritehex = "Hex";
    }
    else
    {
        strwritehex = "Ascii";
    }
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(si.modename));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(si.readdata));
    QStandardItem *standItem3 = new QStandardItem(tr("%1").arg(strreadhex));
    QStandardItem *standItem4 = new QStandardItem(tr("%1").arg(si.writedata));
    QStandardItem *standItem5 = new QStandardItem(tr("%1").arg(strwritehex));
    m_staSerialPortInteract->insertRow(m_staSerialPortInteract->rowCount());
    m_staSerialPortInteract->setItem(m_staSerialPortInteract->rowCount() - 1, 0, standItem1);
    m_staSerialPortInteract->setItem(m_staSerialPortInteract->rowCount() - 1, 1, standItem2);
    m_staSerialPortInteract->setItem(m_staSerialPortInteract->rowCount() - 1, 2, standItem3);
    m_staSerialPortInteract->setItem(m_staSerialPortInteract->rowCount() - 1, 3, standItem4);
    m_staSerialPortInteract->setItem(m_staSerialPortInteract->rowCount() - 1, 4, standItem5);
    m_staSerialPortInteract->item(m_staSerialPortInteract->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staSerialPortInteract->item(m_staSerialPortInteract->rowCount() - 1, 1)->setTextAlignment(Qt::AlignVCenter);
    m_staSerialPortInteract->item(m_staSerialPortInteract->rowCount() - 1, 2)->setTextAlignment(Qt::AlignCenter);
    m_staSerialPortInteract->item(m_staSerialPortInteract->rowCount() - 1, 3)->setTextAlignment(Qt::AlignVCenter);
    m_staSerialPortInteract->item(m_staSerialPortInteract->rowCount() - 1, 4)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief 串口交互发送表格修改
/// \param row
/// \param si
///
void CommDubugAssist::SerialPortInteractRevise(int row, InteractData si)
{
    QString strreadhex, strwritehex;
    if (si.readTextFormat == HEX)
    {
        strreadhex = "Hex";
    }
    else
    {
        strreadhex = "Ascii";
    }
    if (si.sendTextFormat == HEX)
    {
        strwritehex = "Hex";
    }
    else
    {
        strwritehex = "Ascii";
    }
    m_staSerialPortInteract->item(row, 0)->setText(si.modename);
    m_staSerialPortInteract->item(row, 1)->setText(si.readdata);
    m_staSerialPortInteract->item(row, 2)->setText(strreadhex);
    m_staSerialPortInteract->item(row, 3)->setText(si.writedata);
    m_staSerialPortInteract->item(row, 4)->setText(strwritehex);
}

///
/// \brief 串口交互发送表格删除
/// \param row
///
void CommDubugAssist::SerialPortInteractDelete(int row)
{
    m_staSerialPortInteract->removeRow(row);
}

///
/// \brief 清空串口交互发送表格
///
void CommDubugAssist::SerialPortInteractClear()
{
    if (m_staSerialPortInteract->rowCount() > 0)
    {
        m_staSerialPortInteract->removeRows(0, m_staSerialPortInteract->rowCount());
    }
}

///
/// \brief 串口高级循环表格初始化
///
void CommDubugAssist::SerialPortSeniorCycleInit()
{
    m_staSerialPortSeniorCycle = new QStandardItemModel();
    m_staSerialPortSeniorCycle->setColumnCount(2);
    m_staSerialPortSeniorCycle->setHeaderData(0, Qt::Horizontal, QStringLiteral("事件名称"));
    m_staSerialPortSeniorCycle->setHeaderData(1, Qt::Horizontal, QStringLiteral("延时(ms)"));
    ui->tableView_SerialPortSeniorSendCycle->setModel(m_staSerialPortSeniorCycle);
    ui->tableView_SerialPortSeniorSendCycle->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView_SerialPortSeniorSendCycle->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView_SerialPortSeniorSendCycle->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);
    ui->tableView_SerialPortSeniorSendCycle->setColumnWidth(0, 180);
    ui->tableView_SerialPortSeniorSendCycle->setColumnWidth(1, 80);
    ui->tableView_SerialPortSeniorSendCycle->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_SerialPortSeniorSendCycle->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_SerialPortSeniorSendCycle->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief 串口高级循环发送表格添加一项
/// \param sc
///
void CommDubugAssist::SerialPortSeniorCycleAdd(CycleData sc)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(sc.name));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sc.delay));
    m_staSerialPortSeniorCycle->insertRow(m_staSerialPortSeniorCycle->rowCount());
    m_staSerialPortSeniorCycle->setItem(m_staSerialPortSeniorCycle->rowCount() - 1, 0, standItem1);
    m_staSerialPortSeniorCycle->setItem(m_staSerialPortSeniorCycle->rowCount() - 1, 1, standItem2);
    m_staSerialPortSeniorCycle->item(m_staSerialPortSeniorCycle->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staSerialPortSeniorCycle->item(m_staSerialPortSeniorCycle->rowCount() - 1, 1)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief 串口高级循环发送表格插入一项
/// \param row
/// \param sc
///
void CommDubugAssist::SerialPortSeniorCycleInsert(int row, CycleData sc)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(sc.name));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sc.delay));
    m_staSerialPortSeniorCycle->insertRow(row);
    m_staSerialPortSeniorCycle->setItem(row, 0, standItem1);
    m_staSerialPortSeniorCycle->setItem(row, 1, standItem2);
    m_staSerialPortSeniorCycle->item(row, 0)->setTextAlignment(Qt::AlignCenter);
    m_staSerialPortSeniorCycle->item(row, 1)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief 串口高级循环发送修改内容
/// \param row
/// \param sc
///
void CommDubugAssist::SerialPortSeniorCycleRevise(int row, CycleData sc)
{
    m_staSerialPortSeniorCycle->item(row, 0)->setText(sc.name);
    m_staSerialPortSeniorCycle->item(row, 1)->setText(QString("%1").arg(sc.delay));
}

///
/// \brief 串口高级循环发送表格全部设置
/// \param SerialPortCycleDatas
///
void CommDubugAssist::SerialPortSeniorCycleSet(QList<CycleData> CycleDatas)
{
    for (int i = 0; i < CycleDatas.count(); i++)
    {
        SerialPortSeniorCycleAdd(CycleDatas[i]);
    }
}

///
/// \brief 串口高级循环发送表格全部清空
///
void CommDubugAssist::SerialPortSeniorCycleClear()
{
    if (m_staSerialPortSeniorCycle->rowCount() > 0)
    {
        m_staSerialPortSeniorCycle->removeRows(0, m_staSerialPortSeniorCycle->rowCount());
    }
}

///
/// \brief 开启/关闭简易循环发送
/// \param arg1
///
void CommDubugAssist::on_checkBox_SerialPortSendCyCle_stateChanged(int arg1)
{
    //qDebug() << "state:" << state;
    if (arg1 == Qt::Checked)  // 开启循环发送（简易
    {
        if (m_serialPort->isOpen())  // 判断串口是否已打开
        {
            if (ui->lineEdit_SerialPortSendCycleText->text().isEmpty())
            {
                m_nSerialPortCycleTime = -1;
                m_timerSerialPortCycle->start(1);
                //ui->checkBox_SerialPortSendCyCle->setChecked(false);   // 关闭串口循环发送
                //inform("发送内容为空！！");
            }
            else
            {
                ui->spinBox_SerialPortSendCycle->setEnabled(false);
                ui->lineEdit_SerialPortSendCycleText->setEnabled(false);
                ui->label_SerialPortSendCycleText->setEnabled(false);
                ui->label_SerialPortSendCyclems->setEnabled(false);
                m_nSerialPortCycleTime = ui->spinBox_SerialPortSendCycle->value();
                m_timerSerialPortCycle->start(m_nSerialPortCycleTime);  // 开启循环
            }
        }
        else
        {
            m_nSerialPortCycleTime = -2;
            m_timerSerialPortCycle->start(1);
            //ui->checkBox_SerialPortSendCyCle->setChecked(false);   // 关闭串口循环发送
            //inform("串口未打开！");
        }
    }
    else if (arg1 == Qt::Unchecked)  // 关闭循环发送（简易
    {
        ui->spinBox_SerialPortSendCycle->setEnabled(true);
        ui->lineEdit_SerialPortSendCycleText->setEnabled(true);
        ui->label_SerialPortSendCycleText->setEnabled(true);
        ui->label_SerialPortSendCyclems->setEnabled(true);
        if(m_timerSerialPortCycle->isActive())   // 判断定时器是否正在运行
        {
            m_timerSerialPortCycle->stop();   // 停止定时器
        }
    }
}

///
/// \brief 串口简易循环发送内容
///
void CommDubugAssist::SerialPortSendCycle()
{
    if (m_nSerialPortCycleTime == -1 || m_nSerialPortCycleTime == -2)
    {
        ui->checkBox_SerialPortSendCyCle->setChecked(false);
        m_timerSerialPortCycle->stop();
        if (m_nSerialPortCycleTime == -1)
        {
            Inform("发送内容为空！！");
        }
        else if (m_nSerialPortCycleTime == -2)
        {
            Inform("串口未打开！");
        }
        return ;
    }
    if (m_serialPort->isOpen())
    {
        QString strToSend = ui->lineEdit_SerialPortSendCycleText->text();
        // 数据转Hex发送
        if (ui->checkBox_SerialPortSendCyCleHex->isChecked())
        {
            try
            {
                bool isOK = SerialPortSendHex(strToSend);
                if (!isOK)
                {
                    m_timerSerialPortCycle->stop();
                    ui->checkBox_SerialPortSendCyCle->setChecked(false);
                    ui->lineEdit_SerialPortSendCycleText->setReadOnly(false);
                    ui->spinBox_SerialPortSendCycle->setReadOnly(false);
                    Inform("数据转换错误！请检查要发送的数据是否为16进制数据!");
                    return ;
                }
            }
            catch (...)
            {
                m_timerSerialPortCycle->stop();
                ui->checkBox_SerialPortSendCyCle->setChecked(false);
                ui->lineEdit_SerialPortSendCycleText->setReadOnly(false);
                ui->spinBox_SerialPortSendCycle->setReadOnly(false);
                Inform("数据发送错误！");
            }
        }
        else
        {
            //QString strToSend = ui->textEdit_SerialPortSend->toPlainText();
            SerialPortSendLation(strToSend, g_emSerialPortSendTextFormat);
        }
    }
    else
    {
        m_timerSerialPortCycle->stop();
        ui->checkBox_SerialPortSendCyCle->setChecked(false);
        ui->lineEdit_SerialPortSendCycleText->setReadOnly(false);
        ui->spinBox_SerialPortSendCycle->setReadOnly(false);
        Inform("串口已关闭！");
    }
}

///
/// \brief 串口连接与断开
///
void CommDubugAssist::on_btn_SerialPortConnect_clicked()
{
    if(m_serialPort->isOpen())
    {
        SerialPortDisconnect();
    }
    else
    {
        SerialPortConnect(true);
    }
}

///
/// \brief 连接串口
///
void CommDubugAssist::SerialPortConnect(bool informstate)
{
    if (m_serialPort->isOpen())
    {
        if (informstate)
        {
            if (g_bConnectFailRemind)
                Inform("串口已打开");
        }
        return ;
    }
    QString connectname = ui->comboBox_SerialPortName->currentText();
    connectname = connectname.section(':', 0, 0);
    m_serialPort->setPortName(connectname);
    if (!m_serialPort->open(QIODevice::ReadWrite))
    {
        if (informstate)
        {
            if (g_bConnectFailRemind)
                Inform("串口打开失败");
        }
        return ;
    }
    g_sSerialPortConnectName = ui->comboBox_SerialPortName->currentText();
    int baudindex = ui->comboBox_SerialPortBaud->currentIndex();
    g_nSerialPortBaudIndex = baudindex;
    switch (baudindex)   // 波特率
    {
    case 0: m_serialPort->setBaudRate(QSerialPort::Baud1200, QSerialPort::AllDirections); break;
    case 1: m_serialPort->setBaudRate(QSerialPort::Baud2400, QSerialPort::AllDirections); break;
    case 2: m_serialPort->setBaudRate(QSerialPort::Baud4800, QSerialPort::AllDirections); break;
    case 3: m_serialPort->setBaudRate(QSerialPort::Baud9600, QSerialPort::AllDirections); break;
    case 4: m_serialPort->setBaudRate(QSerialPort::Baud19200, QSerialPort::AllDirections); break;
    case 5: m_serialPort->setBaudRate(QSerialPort::Baud38400, QSerialPort::AllDirections); break;
    case 6: m_serialPort->setBaudRate(QSerialPort::Baud57600, QSerialPort::AllDirections); break;
    case 7: m_serialPort->setBaudRate(QSerialPort::Baud115200, QSerialPort::AllDirections); break;
    }
    int dataindex = ui->comboBox_SerialPortData->currentIndex();
    g_nSerialPortDataIndex = dataindex;
    switch (dataindex)   // 数据位
    {
    case 0: m_serialPort->setDataBits(QSerialPort::Data5); break;
    case 1: m_serialPort->setDataBits(QSerialPort::Data6); break;
    case 2: m_serialPort->setDataBits(QSerialPort::Data7); break;
    case 3: m_serialPort->setDataBits(QSerialPort::Data8); break;
    }
    int controlindex = ui->comboBox_SerialPortFlowControl->currentIndex();
    g_nSerialPortControlIndex = controlindex;
    switch (controlindex)  // 控制方式
    {
    case 0: m_serialPort->setFlowControl(QSerialPort::NoFlowControl); break;
    case 1: m_serialPort->setFlowControl(QSerialPort::HardwareControl); break;
    case 2: m_serialPort->setFlowControl(QSerialPort::SoftwareControl); break;
    }
    int parityindex = ui->comboBox_SerialPortParity->currentIndex();
    g_nSerialPortParityIndex = parityindex;
    switch (parityindex)  // 校验位
    {
    case 0: m_serialPort->setParity(QSerialPort::NoParity); break;
    case 1: m_serialPort->setParity(QSerialPort::OddParity); break;
    case 2: m_serialPort->setParity(QSerialPort::EvenParity); break;
    case 3: m_serialPort->setParity(QSerialPort::MarkParity); break;
    case 4: m_serialPort->setParity(QSerialPort::SpaceParity); break;
    }
    int stopindex = ui->comboBox_SerialPortStop->currentIndex();
    g_nSerialPortStopIndex = stopindex;
    switch (stopindex) // 停止位
    {
    case 0: m_serialPort->setStopBits(QSerialPort::OneStop); break;
    case 1: m_serialPort->setStopBits(QSerialPort::OneAndHalfStop); break;
    case 2: m_serialPort->setStopBits(QSerialPort::TwoStop); break;
    }
    connect(m_serialPort, SIGNAL(readyRead()), this, SLOT(SerialPortReadData()));
    //connect(m_serialPort, SIGNAL(disconnect()), this)

    w_Pref->ReadSettings();
    ui->btn_SerialPortConnect->setText("断开");
    ui->label_SerialPortConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(255, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
    ui->comboBox_SerialPortName->setEnabled(false);
    ui->comboBox_SerialPortBaud->setEnabled(false);
    ui->comboBox_SerialPortData->setEnabled(false);
    ui->comboBox_SerialPortParity->setEnabled(false);
    ui->comboBox_SerialPortStop->setEnabled(false);
    ui->comboBox_SerialPortFlowControl->setEnabled(false);
    if (informstate)
    {
        if (g_bConnectSucceedRemind)
            Inform("串口连接成功！");
    }
}

///
/// \brief 断开串口
///
void CommDubugAssist::SerialPortDisconnect()
{
    if (m_serialPort->isOpen())
    {
        bool cycleok = false, seniorcycleok = false;
        if (m_timerSerialPortCycle->isActive())
        {
            bool isOK = AskInform("当前串口循环发送功能正在执行！是否确认断开串口？");
            if (!isOK)
            {
                return ;
            }
            cycleok = true;
        }
        if (ui->btn_SerialPortSeniorSendCycleStart->text() == "终止循环")
        {
            bool isOK = AskInform("当前串口高级循环发送功能正在执行！是否确认断开串口？");
            if (!isOK)
            {
                return ;
            }
            seniorcycleok = true;
        }
        if (cycleok)
        {
            m_timerSerialPortCycle->stop();
            ui->checkBox_SerialPortSendCyCle->setChecked(false);
            ui->lineEdit_SerialPortSendCycleText->setReadOnly(false);
            ui->spinBox_SerialPortSendCycle->setReadOnly(false);
        }
        if (seniorcycleok)
        {
            ui->btn_SerialPortSeniorSendCycleStart->setText("开始循环");
            w_SP_Cycle->SetContinue(false);
            ui->label_SerialPortSeniorSendCycle->setText("当前无执行事件");
        }
        m_serialPort->clear();
        m_serialPort->close();
        ui->btn_SerialPortConnect->setText("连接");
        ui->label_SerialPortConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(0, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
        ui->comboBox_SerialPortName->setEnabled(true);
        ui->comboBox_SerialPortBaud->setEnabled(true);
        ui->comboBox_SerialPortData->setEnabled(true);
        ui->comboBox_SerialPortParity->setEnabled(true);
        ui->comboBox_SerialPortStop->setEnabled(true);
        ui->comboBox_SerialPortFlowControl->setEnabled(true);
        if (g_bDisconnectRemind)
        {
            Inform("串口连接已断开！");
        }
    }
}

///
/// \brief 接收串口数据
///
void CommDubugAssist::SerialPortReadData()
{
    QByteArray info = m_serialPort->readAll();
    QDateTime current_time = QDateTime::currentDateTime();
    g_unSerialPortReadnums += static_cast<uint>(info.length());
    ui->label_SerialPortReadValue->setText(QString::number(g_unSerialPortReadnums));
    SerialPortReadDataLimit();
    if (!ui->checkBox_SerialPortStopShow->isChecked())   // 判断是否打开显示
    {
        if (ui->checkBox_SerialPortReadHex->isChecked())
        {
            if (info.isEmpty())
            {
                return ;
            }
            char *recvBuffer = info.data();

            QString resultHex;

            if (ui->checkBox_SerialPortReadAutoLine->isChecked()) // 末尾增加行的话，前面就加上R
            {
                if (ui->checkBox_SerialPortRecordTime->isChecked())
                {
                    QString strtime = QString("[%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
                                       .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
                                       .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
                    resultHex = strtime + " R(Hex)] ";
                }
                else
                {
                    resultHex = "[R(Hex)] ";
                }
            }
            else   // 末尾不添加行则为空
            {
                resultHex.clear();
            }
            QString Hexdata;
            if (!ui->checkBox_SerialPortReadAutoLine->isChecked() && (ui->textEdit_SerialPortRead->toPlainText().length() > 0) && (ui->textEdit_SerialPortRead->toPlainText().right(1) != " "))
            {
                Hexdata += " ";
            }
            for (int i = 0; i < info.length(); i++)
            {
                Hexdata += QString("%1 ").arg(recvBuffer[i], 2, 16, QChar('0')).toUpper().remove("FFFFFFFFFFFFFF");
                //qDebug() << "Hexdata[" << i << "]: " << Hexdata;
            }
            Hexdata.chop(1);  // 删除末尾空格
            resultHex += Hexdata;
            if (ui->checkBox_SerialPortReadAutoLine->isChecked()) // 末尾是否要增加行
            {
                ui->textEdit_SerialPortRead->append(resultHex);
                //resultHex.append("\n");
            }
            else
            {
                QTextCursor tmpcursor = ui->textEdit_SerialPortRead->textCursor();
                ui->textEdit_SerialPortRead->moveCursor(QTextCursor::End);
                ui->textEdit_SerialPortRead->insertPlainText(resultHex);
                ui->textEdit_SerialPortRead->setTextCursor(tmpcursor);
            }
            if(ui->checkBox_SerialPortReadCurSor->isChecked())
            {
                ui->textEdit_SerialPortRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
            }
        }
        else
        {
            QString strinfo = QString::fromUtf8(info);
            if (strinfo.isEmpty())
            {
                return ;
            }
            if (ui->checkBox_SerialPortReadAutoLine->isChecked()) // 末尾是否要增加行
            {
                //strinfo.append("\n");
                if (ui->checkBox_SerialPortRecordTime->isChecked())
                {
                    QString strtime = QString("[%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
                                       .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
                                       .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
                    strinfo = QString(strtime + " R] %1").arg(strinfo);
                }
                else
                {
                    strinfo = QString("[R] %1").arg(strinfo);
                }
            }
            if (ui->checkBox_SerialPortReadAutoLine->isChecked()) // 末尾是否要增加行
            {
                ui->textEdit_SerialPortRead->append(strinfo);
                //resultHex.append("\n");
            }
            else
            {
                QTextCursor tmpcursor = ui->textEdit_SerialPortRead->textCursor();
                ui->textEdit_SerialPortRead->moveCursor(QTextCursor::End);
                ui->textEdit_SerialPortRead->insertPlainText(strinfo);
                ui->textEdit_SerialPortRead->setTextCursor(tmpcursor);
            }
            if(ui->checkBox_SerialPortReadCurSor->isChecked())
            {
                ui->textEdit_SerialPortRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
            }
        }
    }
    SerialPortInteract(info);    // 判断是否有交互发送的内容
}

///
/// \brief 限制串口数据数量（防止显示数据超过内存造成卡顿
///
void CommDubugAssist::SerialPortReadDataLimit()
{
    if (g_unSerialPortReadDataLimit != 0)
    {
        if (ui->textEdit_SerialPortRead->toPlainText().length() > static_cast<int>(g_unSerialPortReadDataLimit))
        {
            QString readdatas = ui->textEdit_SerialPortRead->toPlainText();
            QString middata = readdatas.mid(readdatas.length()/4);
            int indexnum = middata.indexOf('\n') + 1;
            if (indexnum <= static_cast<int>(g_unSerialPortReadDataLimit)/4)
            {
                readdatas = middata.mid(indexnum);
            }
            else
            {
                readdatas = middata;
            }
            ui->textEdit_SerialPortRead->clear();
            ui->textEdit_SerialPortRead->setText(readdatas);
        }
    }
}

///
/// \brief 串口交互发送判断与发送
/// \param info
///
void CommDubugAssist::SerialPortInteract(QByteArray info)
{
    for (int i = 0 ; i < m_SerialPortInteractDatas.count(); i++)
    {
        bool yes = false;
        QString strinfo;
        if (m_SerialPortInteractDatas[i].readTextFormat == HEX)
        {
            if (m_SerialPortInteractDatas[i].readdata == "[alldata]" && m_SerialPortInteractDatas[i].writedata == "[alldata]" && m_SerialPortInteractDatas[i].sendTextFormat == HEX)
            {
                //SerialPortSendLation(info);
                SerialPortSendHex(info);
                return ;
            }
            else if (m_SerialPortInteractDatas[i].readdata == "[alldata]")
            {
                SerialPortSendLation(m_SerialPortInteractDatas[i].writedata, m_SerialPortInteractDatas[i].sendTextFormat);
                return ;
            }
            else
            {
                strinfo = QString::fromLatin1(info);
            }
        }
        else
        {
            strinfo = QString::fromUtf8(info);
        }
        if (m_SerialPortInteractDatas[i].mode == 0)   // 等于
        {
            if (strinfo == m_SerialPortInteractDatas[i].readdata)
            {
                yes = true;
            }
        }
        else if (m_SerialPortInteractDatas[i].mode == 1)    // 包含
        {
            if (strinfo.indexOf(m_SerialPortInteractDatas[i].readdata) != -1)  // 如果查找返回结果等于-1，则表示没有查询到，如果结果不等于-1，则表示查询到，并且返回的值便是开始的位置
            {
                yes = true;
            }
        }
        else if (m_SerialPortInteractDatas[i].mode == 2)   // 首部
        {
            if (strinfo.length() >= m_SerialPortInteractDatas[i].readdata.length())
            {
                if (strinfo.left(m_SerialPortInteractDatas[i].readdata.length()) == m_SerialPortInteractDatas[i].readdata)
                {
                    yes = true;
                }
            }
        }
        else if (m_SerialPortInteractDatas[i].mode == 3)   // 尾部
        {
            if (strinfo.length() >= m_SerialPortInteractDatas[i].readdata.length())
            {
                if (strinfo.right(m_SerialPortInteractDatas[i].readdata.length()) == m_SerialPortInteractDatas[i].readdata)
                {
                    yes = true;
                }
            }
        }
        if (yes)
        {
            if (m_SerialPortInteractDatas[i].sendTextFormat == HEX)
            {
                bool isOK = SerialPortSendHex(m_SerialPortInteractDatas[i].writedata);
                if (!isOK)
                {
                    return ;
                }
            }
            else
            {
                SerialPortSendLation(m_SerialPortInteractDatas[i].writedata, m_SerialPortInteractDatas[i].sendTextFormat);
            }
        }
    }
}

///
/// \brief 弹窗通知
/// \param strdata
///
void CommDubugAssist::Inform(QString strdata)
{
    QMessageBox m_r;

    WidgetPar *wid;
    wid = new WidgetPar;
    wid->setWindowFlags(Qt::FramelessWindowHint);
    wid->setWindowModality(Qt::ApplicationModal);
    m_r.setParent(wid);
    TitleBar *titbar = new TitleBar(wid);

    titbar->showMinmizeBtn(false);
    titbar->showMaxmizeBtn(false);
    titbar->EnableCloseBtn(false);

    m_r.installEventFilter(titbar);
    QGridLayout *layout = new QGridLayout();

    layout->addWidget(titbar);  //添加标题栏
    layout->addWidget(&m_r);       //添加UI界面
    layout->setSpacing(0);         //布局之间的距离
    layout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    wid->setLayout(layout);  //将这个布局器设置在QWidget上
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);

    wid->setWidgetResizable(false);

    //qDebug() << "this x:" << m_selMainWidget->x() << ", y:" << m_selMainWidget->y() << ", width:" << m_selMainWidget->width() << ", height:" << m_selMainWidget->height();
    //qDebug() << "m_r width:" << m_r.width() << ", height:" << m_r.height() << ", titbar height:" << titbar->height();
    wid->move(m_selMainWidget->x() + (m_selMainWidget->width() - 150 - 3*strdata.length())/2, m_selMainWidget->y() + (m_selMainWidget->height() - (titbar->height() + 50))/2);
    wid->setTitleHeight(static_cast<uint>(titbar->height()));
    //qDebug() << "wid x:" << wid->x() << ", y" << wid->y() << ", width" << wid->width() << ", height" << wid->height();

    m_r.setWindowTitle("提示");
    m_r.setWindowIcon(QIcon(":/image/DCDA.ico"));
    m_r.setText(strdata);

    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        wid->setStyleSheet(stylesheet);
    }
    file.close();

    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = wid->windowFlags();
        wid->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    wid->show();
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);
    m_r.exec();
    wid->close();
    titbar->deleteLater();
    layout->deleteLater();
    wid->deleteLater();
    titbar = nullptr;
    layout = nullptr;
    wid = nullptr;
}

///
/// \brief 弹窗询问
/// \param strdata
/// \return 返回询问结果
///
bool CommDubugAssist::AskInform(QString strdata)
{
    QMessageBox m_r;

    WidgetPar *wid;
    wid = new WidgetPar;
    wid->setWindowFlags(Qt::FramelessWindowHint);
    wid->setWindowModality(Qt::ApplicationModal);
    m_r.setParent(wid);
    TitleBar *titbar = new TitleBar(wid);

    titbar->showMinmizeBtn(false);
    titbar->showMaxmizeBtn(false);
    titbar->EnableCloseBtn(false);

    m_r.installEventFilter(titbar);
    QGridLayout *layout = new QGridLayout();

    layout->addWidget(titbar);  //添加标题栏
    layout->addWidget(&m_r);       //添加UI界面
    layout->setSpacing(0);         //布局之间的距离
    layout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    wid->setLayout(layout);  //将这个布局器设置在QWidget上
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);

    wid->setWidgetResizable(false);

    //qDebug() << "this x:" << m_selMainWidget->x() << ", y:" << m_selMainWidget->y() << ", width:" << m_selMainWidget->width() << ", height:" << m_selMainWidget->height();
    //qDebug() << "m_r width:" << m_r.width() << ", height:" << m_r.height() << ", titbar height:" << titbar->height();
    wid->move(m_selMainWidget->x() + (m_selMainWidget->width() - 150 - 3*strdata.length())/2, m_selMainWidget->y() + (m_selMainWidget->height() - (titbar->height() + 50))/2);
    wid->setTitleHeight(static_cast<uint>(titbar->height()));
    //qDebug() << "wid x:" << wid->x() << ", y" << wid->y() << ", width" << wid->width() << ", height" << wid->height();

    m_r.setWindowTitle("提示");
    m_r.setWindowIcon(QIcon(":/image/DCDA.ico"));
    m_r.setText(strdata);
    m_r.addButton(tr("确认"), QMessageBox::ActionRole);
    m_r.addButton(tr("取消"), QMessageBox::ActionRole);

    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        wid->setStyleSheet(stylesheet);
    }
    file.close();

    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = wid->windowFlags();
        wid->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    wid->show();
    wid->setSizePolicy(m_r.sizePolicy());
    wid->setMaximumSize(m_r.maximumSize());
    wid->setMinimumWidth(m_r.minimumWidth() + 2);
    int isok = m_r.exec();
    wid->close();
    titbar->deleteLater();
    layout->deleteLater();
    wid->deleteLater();
    titbar = nullptr;
    layout = nullptr;
    wid = nullptr;
    if (isok == 1)
    {
        return false;
    }
    else
    {
        return true;
    }
}

///
/// \brief 发送串口数据
///
void CommDubugAssist::on_btn_SerialPortSend_clicked()
{
    if (m_serialPort->isOpen())
    {
        QString strToSend = ui->textEdit_SerialPortSend->toPlainText();

        // 数据转Hex发送
        if (ui->checkBox_SerialPortSendHex->isChecked())
        {
            try
            {
                bool isOK = SerialPortSendHex(strToSend);
                if (!isOK)
                {
                    Inform("数据转换错误！请检查要发送的数据是否为16进制数据!");
                    return ;
                }
            }
            catch (...)
            {
                Inform("数据发送错误！");
            }
        }
        else
        {
            //QString strToSend = ui->textEdit_SerialPortSend->toPlainText();
            SerialPortSendLation(strToSend, g_emSerialPortSendTextFormat);
        }
        //if (ui->textEdit_SerialPortRead->palette().color(QPalette::Base) == QColor(255, 255, 255))
        // 发送后自动清空发送框
        if (ui->checkBox_SerialPortSendAutoClear->isChecked())
        {
            ui->textEdit_SerialPortSend->clear();
        }
    }
    else
    {
        Inform("串口未打开！");
    }
}

///
/// \brief 串口高级循环发送
/// \param num
///
void CommDubugAssist::SerialPortSeniorCycleSendData(int num)
{
    if (ui->btn_SerialPortSeniorSendCycleStart->text() == "终止循环")
    {
        SerialPortSendData(m_SerialPortCycleDatas[num].senddata, m_SerialPortCycleDatas[num].isHex);
        ui->label_SerialPortSeniorSendCycle->setText("已发送到第 " + QString::number(num + 1) + " 项：" + m_SerialPortCycleDatas[num].name);
    }
}

///
/// \brief 串口发送
/// \param senddata
/// \param isHex
///
void CommDubugAssist::SerialPortSendData(QString senddata, bool isHex)
{
    if (m_serialPort->isOpen())
    {
        if (isHex)
        {
            SerialPortSendHex(senddata);
        }
        else
        {
            SerialPortSendLation(senddata, g_emSerialPortSendTextFormat);
        }
    }
    else
    {
        ui->btn_SerialPortSeniorSendCycleStart->setText("开始循环");
        w_SP_Cycle->SetContinue(false);
        Inform("串口已断开！");
    }
}

///
/// \brief 串口发送文本数据
/// \param senddata: 需要发送的数据
///
void CommDubugAssist::SerialPortSendLation(QString senddata, TextFormat sendtextformat)
{
    if (ui->checkBox_SerialPortSendAddLine->isChecked())  // 是否需要增加换行
    {
        senddata.append('\n');
    }
    g_unSerialPortSendnums += static_cast<uint>(senddata.length());
    ui->label_SerialPortSendValue->setText(QString::number(g_unSerialPortSendnums));
    if (sendtextformat == Local)
    {
        //m_serialPort->write(senddata.toStdString().data());   // utf8
        m_serialPort->write(senddata.toLocal8Bit());
    }
    else if (sendtextformat == UTF8)
    {
        m_serialPort->write(senddata.toUtf8());
    }
    else if (sendtextformat == GBK)
    {
        m_serialPort->write(utf8ToGBK(senddata.toUtf8()));
    }
    else if (sendtextformat == Unicode)
    {
        m_serialPort->write(utf8ToUnicode(senddata.toUtf8()));
    }
    else
    {
        m_serialPort->write(senddata.toLatin1());
    }
    //m_serialPort->write(senddata.toLatin1());
    QDateTime current_time = QDateTime::currentDateTime();
    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_SerialPortSendHistory->isChecked())
    {
        SerialPortAddSendHistory(strtime, "Ascii", senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_SerialPortStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_SerialPortRecordSend->isChecked())  // 显示到发送区
        {
            SerialPortReadDataLimit();
            if (ui->checkBox_SerialPortRecordTime->isChecked())   // 判断是否要添加时间
            {
                ui->textEdit_SerialPortRead->append("[" + strtime + " W] " + senddata);
            }
            else
            {
                ui->textEdit_SerialPortRead->append("[W] " + senddata);
            }
            if(ui->checkBox_SerialPortReadCurSor->isChecked())   // 光标是否跟踪到最新位置
            {
                ui->textEdit_SerialPortRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
            }
        }
    }
}

///
/// \brief 串口发送需要转Hex的数据
/// \param 需要发送的数据
///
bool CommDubugAssist::SerialPortSendHex(QString senddata)
{
    bool isOK = toHex(senddata);
    if (!isOK)
    {
        return false;
    }
    QByteArray bytesToSend;
    bytesToSend = QByteArray::fromHex( senddata.remove(" ").toLatin1() );
    if (ui->checkBox_SerialPortSendAddLine->isChecked())
    {
        bytesToSend.append('\n');
    }
    g_unSerialPortSendnums += static_cast<uint>(bytesToSend.length());
    ui->label_SerialPortSendValue->setText(QString::number(g_unSerialPortSendnums));
    m_serialPort->write(bytesToSend);
    QDateTime current_time = QDateTime::currentDateTime();
    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_SerialPortSendHistory->isChecked())
    {
        SerialPortAddSendHistory(strtime, "Hex", senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_SerialPortStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_SerialPortRecordSend->isChecked())  // 显示到发送区
        {
            SerialPortReadDataLimit();
            if (ui->checkBox_SerialPortRecordTime->isChecked())
            {
                ui->textEdit_SerialPortRead->append("[" + strtime + " W(Hex)] " + senddata);
            }
            else
            {
                ui->textEdit_SerialPortRead->append("[W(Hex)] " + senddata);
            }
            if(ui->checkBox_SerialPortReadCurSor->isChecked())
            {
                ui->textEdit_SerialPortRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
            }
        }
    }
    return true;
}

///
/// \brief 快捷键
/// \param event
///
void CommDubugAssist::keyPressEvent(QKeyEvent *event)
{
    // Esc 退出
    if (event->key() == Qt::Key_Escape)
    {

    }
}

///
/// \brief 事件过滤，抓取键盘事件
/// \param watched
/// \param event
/// \return
///
bool CommDubugAssist::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == ui->textEdit_SerialPortSend)
    {
        if (ui->checkBox_SerialPortEnterSend->isChecked())   // 判断是回车发送还是Ctrl+回车发送
        {
            if(event->type() == QEvent::KeyPress)
            {
                 QKeyEvent *k = static_cast<QKeyEvent *>(event);
                 //qDebug() << "key:" << k;
                 if((k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter) && (k->modifiers() & Qt::ControlModifier) )
                 {
                     //qDebug() << "Enter + Ctrl";
                     ui->textEdit_SerialPortSend->insertPlainText("\n");
                     return true;
                 }
                 else if (k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
                 {
                     //qDebug() << "Enter";
                     on_btn_SerialPortSend_clicked();
                     return true;
                 }
            }
        }
        else
        {
            if(event->type() == QEvent::KeyPress)
            {
                 QKeyEvent *k = static_cast<QKeyEvent *>(event);
                 //qDebug() << "key:" << k;
                 if((k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter) && (k->modifiers() & Qt::ControlModifier) )
                 {
                     //qDebug() << "Enter + Ctrl";
                     on_btn_SerialPortSend_clicked();
                     return true;
                 }
                 else if (k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
                 {
                     //qDebug() << "Enter";
                     ui->textEdit_SerialPortSend->insertPlainText("\n");
                     return true;
                 }
            }
        }
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *k = static_cast<QKeyEvent *>(event);
            if (k->key() == Qt::Key_Backspace && k->modifiers() == Qt::ShiftModifier)
            {
                ui->textEdit_SerialPortSend->clear();
                return true;
            }
        }
    }
    else if (watched == ui->textEdit_TCPServerSend)
    {
        if (ui->checkBox_TCPServerEnterSend->isChecked())   // 判断是回车发送还是Ctrl+回车发送
        {
            if(event->type() == QEvent::KeyPress)
            {
                 QKeyEvent *k = static_cast<QKeyEvent *>(event);
                 if((k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter) && (k->modifiers() & Qt::ControlModifier) )
                 {
                     //qDebug() << "Enter + Ctrl";
                     ui->textEdit_TCPServerSend->insertPlainText("\n");
                     return true;
                 }
                 else if (k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
                 {
                     //qDebug() << "Enter";
                     on_btn_TCPServerSend_clicked();
                     return true;
                 }
            }
        }
        else
        {
            if(event->type() == QEvent::KeyPress)
            {
                 QKeyEvent *k = static_cast<QKeyEvent *>(event);
                 if((k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter) && (k->modifiers() & Qt::ControlModifier) )
                 {
                     //qDebug() << "Enter + Ctrl";
                     on_btn_TCPServerSend_clicked();
                     return true;
                 }
                 else if (k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
                 {
                     //qDebug() << "Enter";
                     ui->textEdit_TCPServerSend->insertPlainText("\n");
                     return true;
                 }
            }
        }
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *k = static_cast<QKeyEvent *>(event);
            if (k->key() == Qt::Key_Backspace && k->modifiers() == Qt::ShiftModifier)
            {
                ui->textEdit_TCPServerSend->clear();
                return true;
            }
        }
    }
    else if (watched == ui->textEdit_TCPClientSend)
    {
        if (ui->checkBox_TCPClientEnterSend->isChecked())   // 判断是回车发送还是Ctrl+回车发送
        {
            if(event->type() == QEvent::KeyPress)
            {
                 QKeyEvent *k = static_cast<QKeyEvent *>(event);
                 if((k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter) && (k->modifiers() & Qt::ControlModifier) )
                 {
                     //qDebug() << "Enter + Ctrl";
                     ui->textEdit_TCPClientSend->insertPlainText("\n");
                     return true;
                 }
                 else if (k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
                 {
                     //qDebug() << "Enter";
                     on_btn_TCPClientSend_clicked();
                     return true;
                 }
            }
        }
        else
        {
            if(event->type() == QEvent::KeyPress)
            {
                 QKeyEvent *k = static_cast<QKeyEvent *>(event);
                 if((k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter) && (k->modifiers() & Qt::ControlModifier) )
                 {
                     //qDebug() << "Enter + Ctrl";
                     on_btn_TCPClientSend_clicked();
                     return true;
                 }
                 else if (k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
                 {
                     //qDebug() << "Enter";
                     ui->textEdit_TCPClientSend->insertPlainText("\n");
                     return true;
                 }
            }
        }
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *k = static_cast<QKeyEvent *>(event);
            if (k->key() == Qt::Key_Backspace && k->modifiers() == Qt::ShiftModifier)
            {
                ui->textEdit_TCPClientSend->clear();
                return true;
            }
        }
    }
    return QWidget::eventFilter(watched,event);
}

///
/// \brief 清空串口接收区
///
void CommDubugAssist::on_btn_SerialPortReadClear_clicked()
{
    ui->textEdit_SerialPortRead->clear();
}

///
/// \brief 清空串口发送区
///
void CommDubugAssist::on_btn_SerialPortSendClear_clicked()
{
    ui->textEdit_SerialPortSend->clear();
}

///
/// \brief 清空串口发送记录
///
void CommDubugAssist::on_btn_SerialPortClearSend_clicked()
{
    m_staSerialportHistoryModel->removeRows(0, m_staSerialportHistoryModel->rowCount());
}

///
/// \brief 双击串口发送记录发送信息
/// \param index
///
void CommDubugAssist::on_tableView_SerialPortSendHistory_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    //int column = index.column();
    //if (column == 2)
    //{
        if (m_serialPort->isOpen())
        {
            QString senddata = m_staSerialportHistoryModel->item(row, 2)->text();
            QString type = m_staSerialportHistoryModel->item(row, 1)->text();
            if (type == "Hex")
            {
                SerialPortSendHex(senddata);
            }
            else
            {
                SerialPortSendLation(senddata, Local);
            }
        }
        else
        {
            Inform("串口未打开！");
        }
    //}
}

///
/// \brief 保存串口接收信息
///
void CommDubugAssist::on_btn_SerialPortReadSave_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, "导出串口通信数据", "..\\", "txt(*.txt)");
    if (!path.isEmpty())
    {
        QFile file(path);
        file.open(QIODevice::ReadWrite);
        QString filedata = ui->textEdit_SerialPortRead->toPlainText();
        file.write(filedata.toUtf8());
        file.close();
    }
}

///
/// \brief 添加串口/TCPClient快捷发送内容
/// \param sp
///
void CommDubugAssist::QS_Add(QuickSendData sp, int uinum)
{
    if (uinum == 1)
    {
        m_SerialPortQuickSendDatas.append(sp);
        SerialPortQuickSendAdd(sp.name);
    }
    else if (uinum == 2)
    {
        m_TCPClientQuickSendDatas.append(sp);
        TCPClientQuickSendAdd(sp.name);
    }
}

///
/// \brief 修改串口/TCPClient快捷发送内容
/// \param sp
///
void CommDubugAssist::QS_Revise(QuickSendData sp, int uinum)
{
    if (uinum == 1)
    {
        if (m_SerialPortQuickSendChoose >= 0 && m_SerialPortQuickSendChoose < m_SerialPortQuickSendDatas.count())
        {
            m_SerialPortQuickSendDatas[m_SerialPortQuickSendChoose] = sp;
            SerialPortQuickSendRevise(m_SerialPortQuickSendChoose, sp.name);
        }
        else
        {
            w_QSUnit->inform("修改错误！");
        }
    }
    else if (uinum == 2)
    {
        if (m_TCPClientQuickSendChoose >= 0 && m_TCPClientQuickSendChoose < m_TCPClientQuickSendDatas.count())
        {
            m_TCPClientQuickSendDatas[m_TCPClientQuickSendChoose] = sp;
            TCPClientQuickSendRevise(m_TCPClientQuickSendChoose, sp.name);
        }
        else
        {
            w_QSUnit->inform("修改错误！");
        }
    }
}

///
/// \brief 关闭串口/TCPClient快捷发送项的界面
///
void CommDubugAssist::QS_close()
{
    w_QSUnit->close();
}

///
/// \brief 添加TCP Server快捷发送内容
/// \param sp
///
void CommDubugAssist::TCPS_QS_Add(TCPServerQuickSendData sp)
{
    m_TCPServerQuickSendDatas.append(sp);
    if (sp.ip == "ALL Connections")
    {
        TCPServerQuickSendAdd(sp.ip, sp.name);
    }
    else
    {
        TCPServerQuickSendAdd(sp.ip + ":" + QString::number(sp.port), sp.name);
    }

}

///
/// \brief 修改TCP Server快捷发送内容
/// \param sp
///
void CommDubugAssist::TCPS_QS_Revise(TCPServerQuickSendData sp)
{
    if (m_TCPServerQuickSendChoose >= 0 && m_TCPServerQuickSendChoose < m_TCPServerQuickSendDatas.count())
    {
        m_TCPServerQuickSendDatas[m_TCPServerQuickSendChoose] = sp;
        if (sp.ip == "ALL Connections")
        {
            TCPServerQuickSendRevise(m_TCPServerQuickSendChoose, sp.ip, sp.name);
        }
        else
        {
            TCPServerQuickSendRevise(m_TCPServerQuickSendChoose, sp.ip + ":" + QString::number(sp.port), sp.name);
        }
    }
    else
    {
        w_QSUnit->inform("修改错误！");
    }
}

///
/// \brief 关闭TCP Server快捷发送项的界面
///
void CommDubugAssist::TCPS_QS_close()
{
    w_TCPS_QSUnit->close();
}

///
/// \brief 添加串口/TCP Client交互内容
/// \param si
///
void CommDubugAssist::Inter_Add(InteractData si, int uinum)
{
    if (uinum == 1)
    {
        m_SerialPortInteractDatas.append(si);
        SerialPortInteractAdd(si);
    }
    else if (uinum == 2)
    {
        m_TCPClientInteractDatas.append(si);
        TCPClientInteractAdd(si);
    }
}

///
/// \brief 修改串口/TCP Client交互内容
/// \param si
///
void CommDubugAssist::Inter_Revise(InteractData si, int uinum)
{
    if (uinum == 1)
    {
        if (m_SerialPortInteractChoose >= 0 && m_SerialPortInteractChoose < m_SerialPortInteractDatas.count())
        {
            m_SerialPortInteractDatas[m_SerialPortInteractChoose] = si;
            SerialPortInteractRevise(m_SerialPortInteractChoose, si);
        }
        else
        {
            w_InterUnit->inform("修改错误！");
        }
    }
    else if (uinum == 2)
    {
        if (m_TCPClientInteractChoose >= 0 && m_TCPClientInteractChoose < m_TCPClientInteractDatas.count())
        {
            m_TCPClientInteractDatas[m_TCPClientInteractChoose] = si;
            TCPClientInteractRevise(m_TCPClientInteractChoose, si);
        }
        else
        {
            w_InterUnit->inform("修改错误！");
        }
    }
}

///
/// \brief 关闭串口/TCP Client交互项的页面
///
void CommDubugAssist::Inter_close()
{
    w_InterUnit->close();
}

///
/// \brief 添加TCP Server交互内容
/// \param si
///
void CommDubugAssist::TCPS_Inter_Add(TCPServerInteractData si)
{
    m_TCPServerInteractDatas.append(si);
    TCPServerInteractAdd(si);
}

///
/// \brief 修改TCP Server交互内容
/// \param si
///
void CommDubugAssist::TCPS_Inter_Revise(TCPServerInteractData si)
{
    if (m_TCPServerInteractChoose >= 0 && m_TCPServerInteractChoose < m_TCPServerInteractDatas.count())
    {
        m_TCPServerInteractDatas[m_TCPServerInteractChoose] = si;
        TCPServerInteractRevise(m_TCPServerInteractChoose, si);
    }
    else
    {
        w_TCPS_InterUnit->inform("修改错误！");
    }
}

///
/// \brief 关闭TCP Server交互项的页面
///
void CommDubugAssist::TCPS_Inter_close()
{
    w_TCPS_InterUnit->close();
}

///
/// \brief 关闭串口高级循环编辑界面
///
void CommDubugAssist::SSC_close()
{
    w_SSCycle->close();
}

///
/// \brief 串口/TCP Client高级循环编辑完成
/// \param CycleDatas
///
void CommDubugAssist::SSC_Finish(QList<CycleData> CycleDatas, unsigned int delay, int uinum)
{
    if (uinum == 1)
    {
        if (ui->btn_SerialPortSeniorSendCycleStart->text() == "终止循环")
        {
            w_SSCycle->inform("串口高级循环发送正在运行！无法保存！请先导出后再关闭串口高级循环，再导入保存！");
        }
        else
        {
            m_SerialPortCycleDatas = CycleDatas;
            m_nSerialPortCycleDelay = delay;
            SerialPortSeniorCycleClear();
            SerialPortSeniorCycleSet(m_SerialPortCycleDatas);
            //w_SSCycle->close();
        }
    }
    else if (uinum == 2)
    {
        if (ui->btn_TCPClientSeniorSendCycleStart->text() == "终止循环")
        {
            w_SSCycle->inform("TCP Client 高级循环发送正在运行！无法保存！请先导出后再关闭 TCP Client 高级循环，再导入保存！");
        }
        else
        {
            m_TCPClientCycleDatas = CycleDatas;
            m_nTCPClientCycleDelay = delay;
            TCPClientSeniorCycleClear();
            TCPClientSeniorCycleSet(m_TCPClientCycleDatas);
            //w_SSCycle->close();
        }
    }
}

///
/// \brief 判断串口/TCP Client高级循环内容是否与主界面相等
/// \param sameok
/// \param overstate
/// \param CycleDatas
///
void CommDubugAssist::SSC_JudgeData(bool &sameok, bool &overstate, QList<CycleData> CycleDatas, int delay, int uinum)
{
    if (uinum == 1)    // 串口
    {
        if (m_SerialPortCycleDatas.count() == CycleDatas.count())
        {
            for (int i = 0; i < m_SerialPortCycleDatas.count(); i++)
            {
                if (m_SerialPortCycleDatas[i].name != CycleDatas[i].name)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
                else if (m_SerialPortCycleDatas[i].delay != CycleDatas[i].delay)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
                else if (m_SerialPortCycleDatas[i].isHex != CycleDatas[i].isHex)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
                else if (m_SerialPortCycleDatas[i].senddata != CycleDatas[i].senddata)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
            }
        }
        else
        {
            sameok = false;
            overstate = true;
            return ;
        }
        if (static_cast<unsigned int>(delay) != m_nSerialPortCycleDelay)
        {
            sameok = false;
            overstate = true;
            return ;
        }
        sameok = true;
        overstate = true;
    }
    else if (uinum == 2)   // TCP Client
    {
        if (m_TCPClientCycleDatas.count() == CycleDatas.count())
        {
            for (int i = 0; i < m_TCPClientCycleDatas.count(); i++)
            {
                if (m_TCPClientCycleDatas[i].name != CycleDatas[i].name)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
                else if (m_TCPClientCycleDatas[i].delay != CycleDatas[i].delay)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
                else if (m_TCPClientCycleDatas[i].isHex != CycleDatas[i].isHex)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
                else if (m_TCPClientCycleDatas[i].senddata != CycleDatas[i].senddata)
                {
                    sameok = false;
                    overstate = true;
                    return ;
                }
            }
        }
        else
        {
            sameok = false;
            overstate = true;
            return ;
        }
        if (static_cast<unsigned int>(delay) != m_nTCPClientCycleDelay)
        {
            sameok = false;
            overstate = true;
            return ;
        }
        sameok = true;
        overstate = true;
    }
}

void CommDubugAssist::TCPS_SSC_Finish(QList<TCPServerCycleData> CycleDatas, unsigned int delay)
{
    if (ui->btn_TCPServerSeniorSendCycleStart->text() == "终止循环")
    {
        w_TCPS_SSCycle->inform("TCP Client 高级循环发送正在运行！无法保存！请先导出后再关闭 TCP Client 高级循环，再导入保存！");
    }
    else
    {
        m_TCPServerCycleDatas = CycleDatas;
        m_nTCPServerCycleDelay = delay;
        TCPServerSeniorCycleClear();
        TCPServerSeniorCycleSet(m_TCPServerCycleDatas);
        //w_TCPS_SSCycle->close();
    }
}

void CommDubugAssist::TCPS_SSC_JudgeData(bool &sameok, bool &overstate, QList<TCPServerCycleData> CycleDatas, int delay)
{
    if (m_TCPServerCycleDatas.count() == CycleDatas.count())
    {
        for (int i = 0; i < m_TCPServerCycleDatas.count(); i++)
        {
            if (m_TCPServerCycleDatas[i].name != CycleDatas[i].name)
            {
                sameok = false;
                overstate = true;
                return ;
            }
            else if (m_TCPServerCycleDatas[i].delay != CycleDatas[i].delay)
            {
                sameok = false;
                overstate = true;
                return ;
            }
            else if (m_TCPServerCycleDatas[i].isHex != CycleDatas[i].isHex)
            {
                sameok = false;
                overstate = true;
                return ;
            }
            else if (m_TCPServerCycleDatas[i].senddata != CycleDatas[i].senddata)
            {
                sameok = false;
                overstate = true;
                return ;
            }
            else if (m_TCPServerCycleDatas[i].ip != CycleDatas[i].ip)
            {
                sameok = false;
                overstate = true;
                return ;
            }
            else if (m_TCPServerCycleDatas[i].port != CycleDatas[i].port)
            {
                sameok = false;
                overstate = true;
                return ;
            }
        }
    }
    else
    {
        sameok = false;
        overstate = true;
        return ;
    }
    if (static_cast<unsigned int>(delay) != m_nTCPServerCycleDelay)
    {
        sameok = false;
        overstate = true;
        return ;
    }
    sameok = true;
    overstate = true;
}

void CommDubugAssist::TCPS_SSC_close()
{
    w_TCPS_SSCycle->close();
}

void CommDubugAssist::HelpShow()
{
    if (w_Help->isVisible())
    {
        w_Help->close();
    }
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_Help->windowFlags();
        w_Help->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_Help->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_Help->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_Help->show();
}

void CommDubugAssist::HelpClose()
{
    w_Help->close();
}

void CommDubugAssist::AboutShow()
{
    if (w_About->isVisible())
    {
        w_About->close();
    }
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_About->windowFlags();
        w_About->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_About->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_About->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_About->show();
}

void CommDubugAssist::AboutClose()
{
    w_About->close();
}

void CommDubugAssist::PrefShow()
{
    if (w_Pref->isVisible())
    {
        w_Pref->close();
    }
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_Pref->windowFlags();
        w_Pref->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_Pref->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_Pref->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_Pref->show();
}

void CommDubugAssist::PrefClose()
{
    w_Pref->close();
}

///
/// \brief 读取收发数据字节数
///
void CommDubugAssist::PrefReadSendNumsUpdate()
{
    ui->label_SerialPortReadValue->setText(QString::number(g_unSerialPortReadnums));
    ui->label_SerialPortSendValue->setText(QString::number(g_unSerialPortSendnums));
    ui->label_TCPServerReadValue->setText(QString::number(g_unTCPServerReadnums));
    ui->label_TCPServerSendValue->setText(QString::number(g_unTCPServerSendnums));
    ui->label_TCPClientReadValue->setText(QString::number(g_unTCPClientReadnums));
    ui->label_TCPClientSendValue->setText(QString::number(g_unTCPClientSendnums));
}

///
/// \brief 显示更新界面
///
void CommDubugAssist::checkUpdateShow()
{
    w_Pref->checkUpdateShow();
}

///
/// \brief 串口添加快捷发送项
///
void CommDubugAssist::on_btn_SerialPortQuickSendAdd_clicked()
{
    w_QSUnit->clear();
    w_QSUnit->setUInum(1);
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_QSUnit->windowFlags();
        w_QSUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_QSUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_QSUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_QSUnit->show();
    w_QSUnit->setAdd(true);
}

///
/// \brief 串口修改快捷发送项
///
void CommDubugAssist::on_btn_SerialPortQuickSendRevise_clicked()
{
    if (m_SerialPortQuickSendChoose >= 0 && m_SerialPortQuickSendChoose < m_SerialPortQuickSendDatas.count())
    {
        w_QSUnit->setUInum(1);
        if (pTitleBar->getLockState())
        {
            Qt::WindowFlags m_flags = w_QSUnit->windowFlags();
            w_QSUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
        }
        else
        {
            //Qt::WindowFlags m_flags = nullptr;
            //w_QSUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
            w_QSUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
        }
        w_QSUnit->show();
        w_QSUnit->setAdd(false);
        w_QSUnit->setdata(m_SerialPortQuickSendDatas[m_SerialPortQuickSendChoose]);
    }
    else
    {
        Inform("错误！请选择后再修改！");
    }
}

///
/// \brief 串口删除快捷发送项
///
void CommDubugAssist::on_btn_SerialPortQuickSendDelete_clicked()
{
    if (m_SerialPortQuickSendChoose >= 0 && m_SerialPortQuickSendChoose < m_SerialPortQuickSendDatas.count())
    {
        m_SerialPortQuickSendDatas.removeAt(m_SerialPortQuickSendChoose);
        SerialPortQuickSendDelete(m_SerialPortQuickSendChoose);
        m_SerialPortQuickSendChoose = -1;
    }
    else
    {
        Inform("错误！请选择后再删除！");
    }
}

///
/// \brief 快捷发送
/// \param index:发送的内容
///
void CommDubugAssist::on_tableView_SerialPortQuickSend_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    if (m_serialPort->isOpen())
    {
        QString senddata = m_SerialPortQuickSendDatas[row].writedata;
        if (m_SerialPortQuickSendDatas[row].addLine)
        {
            senddata.append("\n");
        }
        if (m_SerialPortQuickSendDatas[row].isHex)
        {

            SerialPortSendHex(senddata);
        }
        else
        {
            SerialPortSendLation(senddata, Local);
        }
    }
    else
    {
        Inform("串口未打开！");
    }
}

///
/// \brief 获取快捷发送单击的项
/// \param index
///
void CommDubugAssist::on_tableView_SerialPortQuickSend_clicked(const QModelIndex &index)
{
    m_SerialPortQuickSendChoose = index.row();
}

///
/// \brief 添加串口互动发送内容
///
void CommDubugAssist::on_btn_SerialPortInteractAdd_clicked()
{
    w_InterUnit->clear();
    w_InterUnit->SetUInum(1);
    w_InterUnit->SetAdd(true);
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_InterUnit->windowFlags();
        w_InterUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_InterUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_InterUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_InterUnit->show();
}

///
/// \brief 修改串口互动发送内容
///
void CommDubugAssist::on_btn_SerialPortInteractRevise_clicked()
{
    if (m_SerialPortInteractChoose >= 0 && m_SerialPortInteractChoose < m_SerialPortInteractDatas.count())
    {
        w_InterUnit->SetData(m_SerialPortInteractDatas[m_SerialPortInteractChoose]);
        w_InterUnit->SetAdd(false);
        w_InterUnit->SetUInum(1);
        if (pTitleBar->getLockState())
        {
            Qt::WindowFlags m_flags = w_InterUnit->windowFlags();
            w_InterUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
        }
        else
        {
            //Qt::WindowFlags m_flags = nullptr;
            //w_InterUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
            w_InterUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
        }
        w_InterUnit->show();
    }
    else
    {
        Inform("错误！请选择后再修改！");
    }
}

///
/// \brief 串口互动发送点击事件
/// \param index
///
void CommDubugAssist::on_tableView_SerialPortInteract_clicked(const QModelIndex &index)
{
    m_SerialPortInteractChoose = index.row();
}

///
/// \brief 串口互动发送删除内容
///
void CommDubugAssist::on_btn_SerialPortInteractDelete_clicked()
{
    if (m_SerialPortInteractChoose >= 0 && m_SerialPortInteractChoose < m_SerialPortInteractDatas.count())
    {
        m_SerialPortInteractDatas.removeAt(m_SerialPortInteractChoose);
        SerialPortInteractDelete(m_SerialPortInteractChoose);
        m_SerialPortInteractChoose = -1;
    }
    else
    {
        Inform("错误！请选择后再删除！");
    }
}

///
/// \brief 串口互动双击进入修改
/// \param index
///
void CommDubugAssist::on_tableView_SerialPortInteract_doubleClicked(const QModelIndex &index)
{
    m_SerialPortInteractChoose = index.row();
    on_btn_SerialPortInteractRevise_clicked();
}

///
/// \brief 开始串口高级循环发送
///
void CommDubugAssist::on_btn_SerialPortSeniorSendCycleStart_clicked()
{
    if (ui->btn_SerialPortSeniorSendCycleStart->text() == "开始循环")
    {
        if (m_serialPort->isOpen())
        {
            if (m_SerialPortCycleDatas.count() > 0)
            {
                ui->btn_SerialPortSeniorSendCycleStart->setText("终止循环");
                w_SP_Cycle->SetContinue(true);
                emit SerialPortStartCycle(m_SerialPortCycleDatas, m_nSerialPortCycleDelay);
            }
            else
            {
                Inform("当前无循环内容，请先添加循环内容！");
            }
        }
        else
        {
            Inform("串口尚未打开！");
        }
    }
    else if (ui->btn_SerialPortSeniorSendCycleStart->text() == "终止循环")
    {
        ui->btn_SerialPortSeniorSendCycleStart->setText("开始循环");
        w_SP_Cycle->SetContinue(false);
        ui->label_SerialPortSeniorSendCycle->setText("当前无执行事件");
    }
}

///
/// \brief 编辑串口高级循环发送
///
void CommDubugAssist::on_btn_SerialPortSeniorSendCycleEditor_clicked()
{
    if (ui->btn_SerialPortSeniorSendCycleStart->text() == "终止循环")
    {
        bool isOK = AskInform("当前串口高级循环发送功能正在执行！无法保存被修改的内容！是否确认打开？");
        if (!isOK)
        {
            return ;
        }
    }
    w_SSCycle->SetUInum(1);
    w_SSCycle->SetData(m_SerialPortCycleDatas, static_cast<int>(m_nSerialPortCycleDelay));
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_SSCycle->windowFlags();
        w_SSCycle->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_SSCycle->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_SSCycle->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_SSCycle->show();
}

///
/// \brief TCPServer 开启与关闭监听
///
void CommDubugAssist::on_btn_TCPServerStartListening_clicked()
{
    if (ui->btn_TCPServerStartListening->text() == "开启监听")
    {
        TCPServerListen(true);
    }
    else if (ui->btn_TCPServerStartListening->text() == "关闭监听")
    {
        TCPServerDislisten();
    }
}

///
/// \brief TCPServer 开启监听
/// \param informstate
///
void CommDubugAssist::TCPServerListen(bool informstate)
{
    if (ui->lineEdit_TCPServerLocalIP->text().isEmpty())
    {
        if (informstate)
        {
            Inform("请将IP填写完整");
        }
        return ;
    }
    else if (ui->lineEdit_TCPServerLocalPort->text().isEmpty())
    {
        if (informstate)
        {
            Inform("请将端口号填写完整");
        }
        return ;
    }
    QString ip = ui->lineEdit_TCPServerLocalIP->text();
    quint16 port = static_cast<quint16>(ui->lineEdit_TCPServerLocalPort->text().toInt());

    g_sTCPServerListenIP = m_tcpserver->ServerAddress();
    g_un16TCPServerListenPort = m_tcpserver->ServerPort();

    //qDebug() << "ip:" << ip << ", port:" << port;
    m_tcpserver->StartListening(ip, port);
    if (m_tcpserver->isListening())
    {
        g_sTCPServerListenIP = m_tcpserver->ServerAddress();
        g_un16TCPServerListenPort = m_tcpserver->ServerPort();
        w_Pref->ReadSettings();
        ui->btn_TCPServerStartListening->setText("关闭监听");
        ui->label_TCPServerListening->setStyleSheet("min-width: 20px;\nbackground-color: rgb(255, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
        ui->lineEdit_TCPServerLocalIP->setEnabled(false);
        ui->lineEdit_TCPServerLocalPort->setEnabled(false);
        ui->lineEdit_TCPServerLocalIP->setText(m_tcpserver->ServerAddress());
        ui->lineEdit_TCPServerLocalPort->setText(QString("%1").arg(m_tcpserver->ServerPort()));
    }
    else
    {
        if (informstate)
        {
            if (g_bDisconnectRemind)
                Inform("监听失败！请检查IP和端口号是否被占用或错误格式！");
        }
    }
}

///
/// \brief TCPServer 关闭监听
///
void CommDubugAssist::TCPServerDislisten()
{
    m_tcpserver->StopListening();
    QString clientdata = ui->comboBox_TCPServerSendTarget->currentText();
    if (clientdata != "NULL")
    {
        bool isOK = AskInform("检测到有客户端正在连接，是否需要断开客户端的连接？");
        if (isOK)
        {
            m_tcpserver->DisConnectALLClients();
            ui->comboBox_TCPServerReadTarget->clear();
            ui->comboBox_TCPServerSendTarget->clear();
            ui->comboBox_TCPServerSendCycleTarget->clear();
            ui->comboBox_TCPServerReadTarget->addItem("NULL");
            ui->comboBox_TCPServerSendTarget->addItem("NULL");
            ui->comboBox_TCPServerSendCycleTarget->addItem("NULL");
            while(m_TcpServerClientDatas.count() > 1)
            {
                m_TcpServerClientDatas.removeLast();
            }
        }
    }
    ui->btn_TCPServerStartListening->setText("开启监听");
    ui->label_TCPServerListening->setStyleSheet("min-width: 20px;\nbackground-color: rgb(0, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
    ui->lineEdit_TCPServerLocalIP->setEnabled(true);
    ui->lineEdit_TCPServerLocalPort->setEnabled(true);
    //qDebug() << "server close listening state:" << m_tcpserver->isListening();
}

///
/// \brief TCP Server 有新的连接
/// \param ip
/// \param port
///
void CommDubugAssist::TCPServerNewConncetion(QString ip, quint16 port)
{
    if (ip.left(7) == "::ffff:")
    {
        ip = ip.mid(7);
    }
    if (ui->comboBox_TCPServerReadTarget->itemText(0) == "NULL")
    {
        ui->comboBox_TCPServerReadTarget->clear();
        ui->comboBox_TCPServerSendTarget->clear();
        ui->comboBox_TCPServerSendCycleTarget->clear();
        ui->textEdit_TCPServerRead->clear();
        ui->comboBox_TCPServerReadTarget->addItem("ALL Connections");
        ui->comboBox_TCPServerSendTarget->addItem("ALL Connections");
        ui->comboBox_TCPServerSendCycleTarget->addItem("ALL Connections");
    }
//    else if (ui->comboBox_TCPServerReadTarget->count() == 1)
//    {
//        ui->comboBox_TCPServerReadTarget->insertItem(0, "ALL Connections");
//        ui->comboBox_TCPServerSendTarget->insertItem(0, "ALL Connections");
//        ui->comboBox_TCPServerSendCycleTarget->insertItem(0, "ALL Connections");
//        ui->comboBox_TCPServerReadTarget->setCurrentIndex(0);
//        ui->comboBox_TCPServerSendTarget->setCurrentIndex(0);
//        ui->comboBox_TCPServerSendCycleTarget->setCurrentIndex(0);
//    }
    ui->comboBox_TCPServerReadTarget->addItem(ip + ":" + QString::number(port));
    ui->comboBox_TCPServerSendTarget->addItem(ip + ":" + QString::number(port));
    ui->comboBox_TCPServerSendCycleTarget->addItem(ip + ":" + QString::number(port));
    TCPServerClientData tcpserverclientdata;
    tcpserverclientdata.ip = ip;
    tcpserverclientdata.port = port;
    m_TcpServerClientDatas.append(tcpserverclientdata);
}

///
/// \brief TCP Server 的某客户端断开连接
/// \param ip
/// \param port
///
void CommDubugAssist::TCPServerDisConnection(QString ip, quint16 port)
{
    if (ip.left(7) == "::ffff:")
    {
        ip = ip.mid(7);
    }
    //qDebug() << "disconnection: ip:" << ip << ", port:" << port;
    QString clientdata = ip + ":" + QString::number(port);
    for (int i = 0; i < ui->comboBox_TCPServerReadTarget->count(); i++)
    {
        if (ui->comboBox_TCPServerReadTarget->itemText(i) == clientdata)
        {
            ui->comboBox_TCPServerReadTarget->removeItem(i);
            ui->comboBox_TCPServerSendTarget->removeItem(i);
            ui->comboBox_TCPServerSendCycleTarget->removeItem(i);
            break;
        }
    }
    if (ui->comboBox_TCPServerReadTarget->count() == 0)
    {
        ui->comboBox_TCPServerReadTarget->addItem("NULL");
        ui->comboBox_TCPServerSendTarget->addItem("NULL");
        ui->comboBox_TCPServerSendCycleTarget->addItem("NULL");
    }
    else if (ui->comboBox_TCPServerReadTarget->count() == 1 && ui->comboBox_TCPServerReadTarget->itemText(0) == "ALL Connections")
    {
        ui->comboBox_TCPServerReadTarget->setItemText(0, "NULL");
        ui->comboBox_TCPServerSendTarget->setItemText(0, "NULL");
        ui->comboBox_TCPServerSendCycleTarget->setItemText(0, "NULL");
    }
//    else if (ui->comboBox_TCPServerReadTarget->count() == 2)
//    {
//        ui->comboBox_TCPServerReadTarget->removeItem(0);
//        ui->comboBox_TCPServerSendTarget->removeItem(0);
//        ui->comboBox_TCPServerSendCycleTarget->removeItem(0);
//    }
    int num = TCPServerGetClientNum(ip, port);
    if (num >= 0)
    {
        m_TcpServerClientDatas.removeAt(num);
    }
}

///
/// \brief 根据ip和port获取在m_TcpServerClientDatas中的序号位置
/// \param ip
/// \param port
/// \return
///
int CommDubugAssist::TCPServerGetClientNum(QString ip, quint16 port)
{
    for (int i = 1; i < m_TcpServerClientDatas.count(); i++)
    {
        if (m_TcpServerClientDatas[i].ip == ip && m_TcpServerClientDatas[i].port == port)
        {
            return i;
        }
    }
    return -1;
}

///
/// \brief TCPServer 读取数据
/// \param ip
/// \param port
/// \param readdata
///
void CommDubugAssist::TCPServerReadData(QString ip, quint16 port, QByteArray readdata)
{
    //qDebug() << "readdata: ip:" << ip << ", port:" << port << ", readdata:" << readdata;
    QDateTime current_time = QDateTime::currentDateTime();
    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
                       .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
                       .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    g_unTCPServerReadnums += static_cast<uint>(readdata.length());
    ui->label_TCPServerReadValue->setText(QString::number(g_unTCPServerReadnums));
    if (!ui->checkBox_TCPServerStopShow->isChecked())
    {
        if (ip.left(7) == "::ffff:")
        {
            ip = ip.mid(7);
        }
        if (ui->checkBox_TCPServerReadHex->isChecked())
        {
            //qDebug() << "tcp server read hex:" << readdata;
            if (readdata.isEmpty())
            {
                return ;
            }

            const char *recvBuffer = readdata.data();

            QString resultHex;
            QString Hexdata;
            //qDebug() << "tcp server read hex length:" << readdata.length();
            for (int i = 0; i < readdata.length(); i++)
            {
                Hexdata += QString("%1 ").arg(recvBuffer[i], 2, 16, QChar('0')).toUpper().remove("FFFFFFFFFFFFFF");
                //qDebug() << "Hexdata[" << i << "]: " << Hexdata;
            }
            Hexdata.chop(1);  // 删除末尾空格
            resultHex += Hexdata;
            QString socketaddress;
            QString resHex;

            if (ui->checkBox_TCPServerRecordTime->isChecked())   // 是否显示时间
            {
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())   // 是否换行
                {
                    socketaddress = "[" + strtime + " " + ip + ":" + QString::number(port) + " R(Hex)] ";
                    resHex = socketaddress + resultHex;
                    resultHex ="[" + strtime + " R(Hex)] " + resultHex;
                }
                else
                {
                    socketaddress = "[" + strtime + " " + ip + ":" + QString::number(port) + " R(Hex)] ";
                    resHex = socketaddress + resultHex;
                    if ((!ui->textEdit_TCPServerRead->toPlainText().isEmpty()) && ui->textEdit_TCPServerRead->toPlainText().right(1) != ' ' && ui->textEdit_TCPServerRead->toPlainText().right(1) != '\n')
                    {
                        resultHex = " " + resultHex;
                    }
                }
            }
            else
            {
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())
                {
                    socketaddress = "[" + ip + ":" + QString::number(port) + " R(Hex)] ";
                    resHex = socketaddress + resultHex;
                    resultHex = "[R(Hex)] " + resultHex;
                }
                else
                {
                    socketaddress = "[" + ip + ":" + QString::number(port) + " R(Hex)] ";
                    resHex = socketaddress + resultHex;
                    if ((!ui->textEdit_TCPServerRead->toPlainText().isEmpty()) && ui->textEdit_TCPServerRead->toPlainText().right(1) != ' ' && ui->textEdit_TCPServerRead->toPlainText().right(1) != '\n')
                    {
                        resultHex = " " + resultHex;
                    }
                }

            }
            TCPServerShowAddData(ip, port, resultHex, resHex);
        }
        else
        {
            QString socketaddress;
            QString resdata = QString::fromUtf8(readdata);
            QString data;
            if (ui->checkBox_TCPServerRecordTime->isChecked())   // 是否显示时间
            {
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())   // 是否换行
                {
                    socketaddress = "[" + strtime + " " + ip + ":" + QString::number(port) + " R] ";
                    data = socketaddress + resdata;
                    resdata ="[" + strtime + "] " + resdata;
                }
                else
                {
                    socketaddress = "[" + strtime + " " + ip + ":" + QString::number(port) + " R] ";
                    data = socketaddress + resdata;
                }
            }
            else
            {
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())
                {
                    socketaddress = "[" + ip + ":" + QString::number(port) + " R] ";
                    data = socketaddress + resdata;
                    resdata = "[R] " + resdata;
                }
                else
                {
                    socketaddress = "[" + ip + ":" + QString::number(port) + " R] ";
                    data = socketaddress + resdata;
                }

            }
            TCPServerShowAddData(ip, port, resdata, data);
        }
    }

    TCPServerInteract(ip, port, readdata);
}

///
/// \brief 限制串口数据数量（防止显示数据超过内存造成卡顿
///
void CommDubugAssist::TCPServerReadDataLimit()
{
    if (g_unTCPServerReadDataLimit != 0)
    {
        for (int i = 0; i < m_TcpServerClientDatas.count(); i++)
        {
            if (m_TcpServerClientDatas[i].ShowData.length() > static_cast<int>(g_unTCPServerReadDataLimit))
            {
                QString readdatas = m_TcpServerClientDatas[i].ShowData;
                QString middata = readdatas.mid(readdatas.length()/4);
                int indexnum = middata.indexOf('\n') + 1;
                if (indexnum <= static_cast<int>(g_unTCPServerReadDataLimit)/4)
                {
                    readdatas = middata.mid(indexnum);
                }
                else
                {
                    readdatas = middata;
                }
                m_TcpServerClientDatas[i].ShowData = readdatas;
            }
        }
        if (ui->textEdit_TCPServerRead->toPlainText().length() > static_cast<int>(g_unTCPServerReadDataLimit))
        {
            QString readdatas = ui->textEdit_TCPServerRead->toPlainText();
            QString middata = readdatas.mid(readdatas.length()/4);
            int indexnum = middata.indexOf('\n');
            if (indexnum <= static_cast<int>(g_unTCPServerReadDataLimit)/4)
            {
                readdatas = middata.mid(indexnum);
            }
            else
            {
                readdatas = middata;
            }
            ui->textEdit_TCPServerRead->clear();
            ui->textEdit_TCPServerRead->setText(readdatas);
        }
    }
}

///
/// \brief TCP Server 添加数据通信显示信息
/// \param ip: 接收/发送的IP地址
/// \param port: 接收/发送的端口号
/// \param showdata: 对每个客户端记录的信息
/// \param data: 记录的总信息
///
void CommDubugAssist::TCPServerShowAddData(QString ip, quint16 port, QString data, QString alldata)
{
    QString checkclient = ui->comboBox_TCPServerReadTarget->currentText();
    QString checkip = checkclient.section(':', 0, 0); // 显示是否是全部显示
    quint16 checkport = static_cast<quint16>(checkclient.section(':', 1, 1).toInt());

    // 数据存储
    if (ip == "ALL Connections" && (!data.isEmpty()))
    {
        for (int i = 1; i < m_TcpServerClientDatas.count(); i++)
        {
            if (ui->checkBox_TCPServerReadAutoLine->isChecked())
            {
                if (m_TcpServerClientDatas[i].ShowData.isEmpty())
                {
                    m_TcpServerClientDatas[i].ShowData.append(data);
                }
                else
                {
                    m_TcpServerClientDatas[i].ShowData.append('\n' + data);
                }
            }
        }
    }
    else
    {
        int num = TCPServerGetClientNum(ip, port);
        if (num > 0 && (!data.isEmpty()))
        {
            if (ui->checkBox_TCPServerReadAutoLine->isChecked())
            {
                if (m_TcpServerClientDatas[num].ShowData.isEmpty())
                {
                    m_TcpServerClientDatas[num].ShowData.append(data);
                }
                else
                {
                    m_TcpServerClientDatas[num].ShowData.append('\n' + data);
                }
            }
            else
            {
                m_TcpServerClientDatas[num].ShowData.append(data);
            }
        }
    }
    if (m_TcpServerClientDatas[0].ShowData.isEmpty())
    {
        m_TcpServerClientDatas[0].ShowData.append(alldata);
    }
    else
    {
        m_TcpServerClientDatas[0].ShowData.append('\n' + alldata);
    }

    // 界面显示
    if (checkip == "ALL Connections")
    {
        ui->textEdit_TCPServerRead->append(alldata);
        if(ui->checkBox_TCPServerReadCurSor->isChecked())   // 光标是否跟踪到最新位置
        {
            ui->textEdit_TCPServerRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
        }
    }
    else if (checkip == ip && port == checkport && (!data.isEmpty()))
    {
        if (ui->checkBox_TCPServerReadAutoLine->isChecked())
        {
            ui->textEdit_TCPServerRead->append(data);
        }
        else
        {
            ui->textEdit_TCPServerRead->moveCursor(QTextCursor::End);
            ui->textEdit_TCPServerRead->insertPlainText(data);
        }
        if(ui->checkBox_TCPServerReadCurSor->isChecked())   // 光标是否跟踪到最新位置
        {
            ui->textEdit_TCPServerRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
        }
    }
    else if (ip == "ALL Connections" && (!data.isEmpty()))
    {
        if (ui->checkBox_TCPServerReadAutoLine->isChecked())
        {
            ui->textEdit_TCPServerRead->append(data);
        }
        if(ui->checkBox_TCPServerReadCurSor->isChecked())   // 光标是否跟踪到最新位置
        {
            ui->textEdit_TCPServerRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
        }
    }
}

///
/// \brief TCP Server 发送信息
///
void CommDubugAssist::on_btn_TCPServerSend_clicked()
{
    QString clientdata = ui->comboBox_TCPServerSendTarget->currentText();
    if (clientdata != "NULL")
    {
        QString ip;
        quint16 port;
        if (clientdata == "ALL Connections")
        {
            ip = clientdata;
            port = 0;
        }
        else
        {
            ip = clientdata.section(':', 0, 0);
            port = static_cast<quint16>(clientdata.section(':', 1, 1).toInt());
        }
        QString data = ui->textEdit_TCPServerSend->toPlainText();
        if (data.isEmpty())
        {
            Inform("当前发送内容为空！");
            return ;
        }

        if (ui->checkBox_TCPServerSendHex->isChecked())
        {
            bool isOK = TCPServerSendHex(ip, port, data);
            if (!isOK)
            {
                Inform("数据转换错误！请检查要发送的数据是否为16进制数据!");
            }
        }
        else
        {
            TCPServerSendLation(ip, port, data, g_emTCPServerSendTextFormat);
        }
        if (ui->checkBox_TCPServerSendAutoClear->isChecked())  // 判断发送后是否清空
        {
            ui->textEdit_TCPServerSend->clear();
        }
    }
    else
    {
        Inform("当前无客户端连接！");
    }
}

///
/// \brief TCPServer发送文本数据
/// \param ip
/// \param port
/// \param senddata
/// \return 发送成功与否
///
bool CommDubugAssist::TCPServerSendLation(QString ip, quint16 port, QString senddata, TextFormat sendtextformat)
{
    QByteArray bytesdata = senddata.toUtf8();
    if (ui->checkBox_TCPServerSendAddLine->isChecked())
    {
        bytesdata.append('\n');
    }
    QDateTime current_time = QDateTime::currentDateTime();
    QString SendType;
    g_unTCPServerSendnums += static_cast<uint>(bytesdata.length());
    ui->label_TCPServerSendValue->setText(QString::number(g_unTCPServerSendnums));
    switch (sendtextformat) {
    case HEX:
    case Local: bytesdata = senddata.toLocal8Bit(); SendType = "Local"; break;
    case UTF8: bytesdata = senddata.toUtf8(); SendType = "UTF8"; break;
    case GBK: bytesdata = utf8ToGBK(senddata.toUtf8()); SendType = "GBK"; break;
    case Unicode: bytesdata = utf8ToUnicode(senddata.toUtf8()); SendType = "Unicode"; break;
    }
    m_tcpserver->SendData(ip, port, bytesdata);
    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_TCPServerSendHistory->isChecked())
    {
        TCPServerAddSendHistory(strtime, ip, port, SendType, senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_TCPServerStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_TCPServerRecordSend->isChecked())  // 显示到发送区
        {
            QString sendtarget;
            if (ip == "ALL Connections")
            {
                sendtarget = ip;
            }
            else
            {
                sendtarget = ip + ":" + QString::number(port);
            }
            QString data, alldata;
            //data.clear();
            if (ui->checkBox_TCPServerRecordTime->isChecked())   // 判断是否要添加时间
            {
                alldata = "[" + strtime + " " + sendtarget + " W] " + senddata;
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())  // 确认换行后才可以在接收界面显示，否则只能在All界面内显示
                {
                    data = "[" + strtime + " W] " + senddata;
                }
            }
            else
            {
                alldata = "[" + sendtarget + " W] " + senddata;
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())
                {
                    data = "[W] " + senddata;
                }
            }
            TCPServerShowAddData(ip, port, data, alldata);
        }
    }
    return true;
}

///
/// \brief TCPServer发送Hex数据
/// \param ip
/// \param port
/// \param senddata
/// \return 发送成功与否
///
bool CommDubugAssist::TCPServerSendHex(QString ip, quint16 port, QString senddata)
{
    if (senddata.left(2) == "0x" || senddata.left(2) == "0X")
    {
        senddata = senddata.mid(2);
    }
    bool isOK = toHex(senddata);
    if (!isOK)
    {
        return false;
    }
    QByteArray bytesToSend;
    bytesToSend = QByteArray::fromHex( senddata.remove(" ").toLatin1() );
    if (ui->checkBox_TCPServerSendAddLine->isChecked())
    {
        bytesToSend.append('\n');
    }
    m_tcpserver->SendData(ip, port, bytesToSend);
    QDateTime current_time = QDateTime::currentDateTime();
    //qDebug() << "tcp server send hex:" << bytesToSend;
    g_unTCPServerSendnums += static_cast<uint>(bytesToSend.length());
    ui->label_TCPServerSendValue->setText(QString::number(g_unTCPServerSendnums));

    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_TCPServerSendHistory->isChecked())
    {
        TCPServerAddSendHistory(strtime, ip, port, "Hex", senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_TCPServerStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_TCPServerRecordSend->isChecked())  // 记录到发送区
        {
            QString sendtarget;
            if (ip == "ALL Connections")
            {
                sendtarget = ip;
            }
            else
            {
                sendtarget = ip + ":" + QString::number(port);
            }

            QString data, alldata;
            if (ui->checkBox_TCPServerRecordTime->isChecked())   // 判断是否要添加时间
            {
                alldata = "[" + strtime + " " + sendtarget + " W(Hex)] " + senddata;
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())  // 确认换行后才可以在接收界面显示，否则只能在All界面内显示
                {
                    data = "[" + strtime + " W(Hex)] " + senddata;
                }
            }
            else
            {
                alldata = "[" + sendtarget + " W(Hex)] " + senddata;
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())
                {
                    data = "[W(Hex)] " + senddata;
                }
            }
            TCPServerShowAddData(ip, port, data, alldata);
        }
    }

    return true;
}

bool CommDubugAssist::TCPServerSendByteArray(QString ip, quint16 port, QByteArray senddata)
{
    QByteArray bytesdata = senddata;

    QDateTime current_time = QDateTime::currentDateTime();
    QString SendType;
    g_unTCPServerSendnums += static_cast<uint>(bytesdata.length());
    ui->label_TCPServerSendValue->setText(QString::number(g_unTCPServerSendnums));

    m_tcpserver->SendData(ip, port, bytesdata);
    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_TCPServerSendHistory->isChecked())
    {
        TCPServerAddSendHistory(strtime, ip, port, SendType, senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_TCPServerStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_TCPServerRecordSend->isChecked())  // 显示到发送区
        {
            QString sendtarget;
            if (ip == "ALL Connections")
            {
                sendtarget = ip;
            }
            else
            {
                sendtarget = ip + ":" + QString::number(port);
            }
            QString data, alldata;
            //data.clear();
            if (ui->checkBox_TCPServerRecordTime->isChecked())   // 判断是否要添加时间
            {
                alldata = "[" + strtime + " " + sendtarget + " W] " + senddata;
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())  // 确认换行后才可以在接收界面显示，否则只能在All界面内显示
                {
                    data = "[" + strtime + " W] " + senddata;
                }
            }
            else
            {
                alldata = "[" + sendtarget + " W] " + senddata;
                if (ui->checkBox_TCPServerReadAutoLine->isChecked())
                {
                    data = "[W] " + senddata;
                }
            }
            TCPServerShowAddData(ip, port, data, alldata);
        }
    }
    return true;
}

///
/// \brief 清空 TCP Server 接收
///
void CommDubugAssist::on_btn_TCPServerReadClear_clicked()
{
    ui->textEdit_TCPServerRead->clear();
    if (ui->comboBox_TCPServerReadTarget->currentText() == "NULL" || ui->comboBox_TCPServerReadTarget->currentText() == "ALL Connections")
    {
        m_TcpServerClientDatas[0].ShowData.clear();
    }
    else
    {
        QString checkclient = ui->comboBox_TCPServerReadTarget->currentText();
        QString checkip = checkclient.section(':', 0, 0);
        quint16 checkport = static_cast<quint16>(checkclient.section(':', 1, 1).toInt());
        int num = TCPServerGetClientNum(checkip, checkport);
        if (num > 0)
        {
            m_TcpServerClientDatas[num].ShowData.clear();
        }
    }
}

///
/// \brief 保存 Tcp Server 接收
///
void CommDubugAssist::on_btn_TCPServerReadSave_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, "导出TCP Server通讯数据", "..\\", "txt(*.txt)");
    if (!path.isEmpty())
    {
        QFile file(path);
        file.open(QIODevice::ReadWrite);
        QString filedata = ui->textEdit_SerialPortRead->toPlainText();
        file.write(filedata.toUtf8());
        file.close();
    }
}

///
/// \brief 清空 TCP Server 发送区
///
void CommDubugAssist::on_btn_TCPServerSendClear_clicked()
{
    ui->textEdit_TCPServerSend->clear();
}

///
/// \brief TCP Server 简易循环发送
///
void CommDubugAssist::TCPServerSendCycle()
{
    if (m_nTCPServerCycleTime == -1 || m_nTCPServerCycleTime == -2)
    {
        ui->checkBox_TCPServerSendCyCle->setChecked(false);
        m_timerTCPServerCycle->stop();
        if (m_nTCPServerCycleTime == -1)
        {
            Inform("发送内容为空！！");
        }
        else if (m_nTCPServerCycleTime == -2)
        {
            Inform("当前无客户端连接！");
        }
        return ;
    }
    if (m_sTCPServerCycleObject != ui->comboBox_TCPServerSendCycleTarget->currentText())
    {
        ui->checkBox_TCPServerSendCyCle->setChecked(false);
        m_timerTCPServerCycle->stop();
        ui->spinBox_TCPServerSendCycle->setEnabled(true);
        ui->comboBox_TCPServerSendCycleTarget->setEnabled(true);
        ui->lineEdit_TCPServerSendCycleText->setEnabled(true);
        Inform("客户端连接已断开！");
        return ;
    }
    QString senddata = ui->lineEdit_TCPServerSendCycleText->text();
    if (ui->checkBox_TCPServerSendCyCleHex->isChecked())
    {
        bool isOK = TCPServerSendHex(m_sTCPServerCycleIP, m_nTCPServerCyclePort, senddata);
        if (!isOK)
        {
            ui->checkBox_TCPServerSendCyCle->setChecked(false);
            ui->spinBox_TCPServerSendCycle->setEnabled(true);
            ui->comboBox_TCPServerSendCycleTarget->setEnabled(true);
            ui->lineEdit_TCPServerSendCycleText->setEnabled(true);
            m_timerTCPServerCycle->stop();
            Inform("Hex数据转换错误！");
            return ;
        }
    }
    else
    {
        TCPServerSendLation(m_sTCPServerCycleIP, m_nTCPServerCyclePort, senddata, g_emTCPServerSendTextFormat);
    }
}

///
/// \brief TCP Server 发送高级循环数据
/// \param num
///
void CommDubugAssist::TCPServerSeniorCycleSendData(int num)
{
    if (ui->btn_TCPServerSeniorSendCycleStart->text() == "终止循环")
    {
        TCPServerSendData(m_TCPServerCycleDatas[num].ip, m_TCPServerCycleDatas[num].port, m_TCPServerCycleDatas[num].senddata, m_TCPServerCycleDatas[num].isHex);
        ui->label_TCPServerSeniorSendCycle->setText("已发送到第 " + QString::number(num + 1) + " 项：" + m_TCPServerCycleDatas[num].name);
    }
}

///
/// \brief TCP Server 发送数据
/// \param ip
/// \param port
/// \param senddata
/// \param isHex
///
void CommDubugAssist::TCPServerSendData(QString ip, quint16 port, QString senddata, bool isHex)
{
    if (isHex)
    {
        TCPServerSendHex(ip, port, senddata);
    }
    else
    {
        TCPServerSendLation(ip, port, senddata, g_emTCPServerSendTextFormat);
    }
}

///
/// \brief TCP Server 发送历史记录初始化
///
void CommDubugAssist::TCPServerSendHistoryInit()
{
    m_staTCPServerHistoryModel = new QStandardItemModel();
    m_staTCPServerHistoryModel->setColumnCount(4);
    m_staTCPServerHistoryModel->setHeaderData(0, Qt::Horizontal, QStringLiteral("时间"));
    m_staTCPServerHistoryModel->setHeaderData(1, Qt::Horizontal, QStringLiteral("发送对象"));
    m_staTCPServerHistoryModel->setHeaderData(2, Qt::Horizontal, QStringLiteral("数据类型"));
    m_staTCPServerHistoryModel->setHeaderData(3, Qt::Horizontal, QStringLiteral("发送内容"));
    ui->tableView_TCPServerSendHistory->setModel(m_staTCPServerHistoryModel);
    ui->tableView_TCPServerSendHistory->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);  // 表头信息显示居中
    ui->tableView_TCPServerSendHistory->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed); // Fixed 为设置列宽不可变
    ui->tableView_TCPServerSendHistory->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch); // Stretch 为设置列宽可变
    ui->tableView_TCPServerSendHistory->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
    ui->tableView_TCPServerSendHistory->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->tableView_TCPServerSendHistory->setColumnWidth(0, 160);
    ui->tableView_TCPServerSendHistory->setColumnWidth(1, 100);// 设置列的宽度
    ui->tableView_TCPServerSendHistory->setColumnWidth(2, 55);
    ui->tableView_TCPServerSendHistory->setColumnWidth(3, 100);

    ui->tableView_TCPServerSendHistory->setSelectionMode(QAbstractItemView::SingleSelection); // 设定只能选择一行，不能选择多行
    ui->tableView_TCPServerSendHistory->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPServerSendHistory->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief TCP Server 增加一条发送记录
/// \param datatime
/// \param ip
/// \param port
/// \param type
/// \param senddata
///
void CommDubugAssist::TCPServerAddSendHistory(QString datatime, QString ip, quint16 port, QString type, QString senddata)
{
    QString sendobject;
    if (ip == "ALL Connections")
    {
        sendobject = ip;
    }
    else
    {
        sendobject = ip + ":" + QString::number(port);
    }
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(datatime));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sendobject));
    QStandardItem *standItem3 = new QStandardItem(tr("%1").arg(type));
    QStandardItem *standItem4 = new QStandardItem(tr("%1").arg(senddata));
    m_staTCPServerHistoryModel->insertRow(0);
    m_staTCPServerHistoryModel->setItem(0, 0, standItem1);
    m_staTCPServerHistoryModel->setItem(0, 1, standItem2);
    m_staTCPServerHistoryModel->setItem(0, 2, standItem3);
    m_staTCPServerHistoryModel->setItem(0, 3, standItem4);
    m_staTCPServerHistoryModel->item(0, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerHistoryModel->item(0, 1)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerHistoryModel->item(0, 2)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerHistoryModel->item(0, 3)->setTextAlignment(Qt::AlignVCenter);
    if (g_unTCPServerHistoryDatanumsLimit > 0)
    {
        while (m_staTCPServerHistoryModel->rowCount() > static_cast<int>(g_unTCPServerHistoryDatanumsLimit))
        {
            m_staTCPServerHistoryModel->removeRow(m_staTCPServerHistoryModel->rowCount() - 1);
        }
    }
}

///
/// \brief TCP Server 快捷发送表格初始化
///
void CommDubugAssist::TCPServerQuickSendInit()
{
    m_staTCPServerQuickSend = new QStandardItemModel();
    m_staTCPServerQuickSend->setColumnCount(2);
    m_staTCPServerQuickSend->setHeaderData(0, Qt::Horizontal, QStringLiteral("客户端"));
    m_staTCPServerQuickSend->setHeaderData(1, Qt::Horizontal, QStringLiteral("快捷发送"));
    ui->tableView_TCPServerQuickSend->setModel(m_staTCPServerQuickSend);
    ui->tableView_TCPServerQuickSend->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);  // 表头信息显示居中
    ui->tableView_TCPServerQuickSend->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    ui->tableView_TCPServerQuickSend->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tableView_TCPServerQuickSend->setColumnWidth(0, 120);
    ui->tableView_TCPServerQuickSend->setColumnWidth(1, 100);// 设置列的宽度
    ui->tableView_TCPServerQuickSend->setSelectionMode(QAbstractItemView::SingleSelection); // 设定只能选择一行，不能选择多行
    ui->tableView_TCPServerQuickSend->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPServerQuickSend->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief TCP Server 快捷发送表格添加
/// \param name
///
void CommDubugAssist::TCPServerQuickSendAdd(QString obj, QString name)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(obj));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(name));
    m_staTCPServerQuickSend->insertRow(m_staTCPServerQuickSend->rowCount());
    m_staTCPServerQuickSend->setItem(m_staTCPServerQuickSend->rowCount() - 1, 0, standItem1);
    m_staTCPServerQuickSend->setItem(m_staTCPServerQuickSend->rowCount() - 1, 1, standItem2);
    m_staTCPServerQuickSend->item(m_staTCPServerQuickSend->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerQuickSend->item(m_staTCPServerQuickSend->rowCount() - 1, 1)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief TCP Server 快捷发送表格修改
/// \param row
/// \param name
///
void CommDubugAssist::TCPServerQuickSendRevise(int row, QString obj, QString name)
{
    m_staTCPServerQuickSend->item(row, 0)->setText(obj);
    m_staTCPServerQuickSend->item(row, 1)->setText(name);
}

///
/// \brief TCP Server 快捷发送表格删除
/// \param row
///
void CommDubugAssist::TCPServerQuickSendDelete(int row)
{
    m_staTCPServerQuickSend->removeRow(row);
}

///
/// \brief 清空 TCP Server 快捷发送表格
///
void CommDubugAssist::TCPServerQuickSendClear()
{
    if (m_staTCPServerQuickSend->rowCount() > 0)
    {
        m_staTCPServerQuickSend->removeRows(0, m_staTCPServerQuickSend->rowCount());
    }
}

///
/// \brief TCP Server 快捷发送添加
///
void CommDubugAssist::on_btn_TCPServerQuickSendAdd_clicked()
{
    w_TCPS_QSUnit->clear();
    w_TCPS_QSUnit->SetObjects(m_TcpServerClientDatas);
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_TCPS_QSUnit->windowFlags();
        w_TCPS_QSUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_TCPS_QSUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_TCPS_QSUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_TCPS_QSUnit->show();
    w_TCPS_QSUnit->SetAdd(true);
}

///
/// \brief TCP Server 快捷发送修改
///
void CommDubugAssist::on_btn_TCPServerQuickSendRevise_clicked()
{
    if (m_TCPServerQuickSendChoose >= 0 && m_TCPServerQuickSendChoose < m_TCPServerQuickSendDatas.count())
    {
        w_TCPS_QSUnit->clear();
        w_TCPS_QSUnit->SetObjects(m_TcpServerClientDatas);
        if (pTitleBar->getLockState())
        {
            Qt::WindowFlags m_flags = w_TCPS_QSUnit->windowFlags();
            w_TCPS_QSUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
        }
        else
        {
            //Qt::WindowFlags m_flags = nullptr;
            //w_TCPS_QSUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
            w_TCPS_QSUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
        }
        w_TCPS_QSUnit->show();
        w_TCPS_QSUnit->SetAdd(false);
        w_TCPS_QSUnit->SetData(m_TCPServerQuickSendDatas[m_TCPServerQuickSendChoose]);
    }
    else
    {
        Inform("错误！请选择后再修改！");
    }
}

///
/// \brief TCP Server 快捷发送删除
///
void CommDubugAssist::on_btn_TCPServerQuickSendDelete_clicked()
{
    if (m_TCPServerQuickSendChoose >= 0 && m_TCPServerQuickSendChoose < m_TCPServerQuickSendDatas.count())
    {
        m_TCPServerQuickSendDatas.removeAt(m_TCPServerQuickSendChoose);
        TCPServerQuickSendDelete(m_TCPServerQuickSendChoose);
        m_TCPServerQuickSendChoose = -1;
    }
    else
    {
        Inform("错误！请选择后再删除！");
    }
}

///
/// \brief TCP Server 交互发送表格初始化
///
void CommDubugAssist::TCPServerInteractInit()
{
    m_staTCPServerInteract = new QStandardItemModel();
    m_staTCPServerInteract->setColumnCount(5);
    m_staTCPServerInteract->setHeaderData(0, Qt::Horizontal, QStringLiteral("类型"));
    m_staTCPServerInteract->setHeaderData(1, Qt::Horizontal, QStringLiteral("接收"));
    m_staTCPServerInteract->setHeaderData(2, Qt::Horizontal, QStringLiteral("接收类型"));
    m_staTCPServerInteract->setHeaderData(3, Qt::Horizontal, QStringLiteral("发送"));
    m_staTCPServerInteract->setHeaderData(4, Qt::Horizontal, QStringLiteral("发送类型"));
    ui->tableView_TCPServerInteract->setModel(m_staTCPServerInteract);
    ui->tableView_TCPServerInteract->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView_TCPServerInteract->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    ui->tableView_TCPServerInteract->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tableView_TCPServerInteract->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
    ui->tableView_TCPServerInteract->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->tableView_TCPServerInteract->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Fixed);
    ui->tableView_TCPServerInteract->setColumnWidth(0, 80);
    ui->tableView_TCPServerInteract->setColumnWidth(1, 200);
    ui->tableView_TCPServerInteract->setColumnWidth(2, 60);
    ui->tableView_TCPServerInteract->setColumnWidth(3, 200);
    ui->tableView_TCPServerInteract->setColumnWidth(4, 60);
    ui->tableView_TCPServerInteract->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_TCPServerInteract->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPServerInteract->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief TCP Server 交互发送表格增加
/// \param si
///
void CommDubugAssist::TCPServerInteractAdd(TCPServerInteractData si)
{
    QString strreadhex, strwritetype;
    if (si.readTextFormat == HEX)
    {
        strreadhex = "Hex";
    }
    else
    {
        strreadhex = "Local";
    }
    strwritetype = TextFormat2Str(si.sendTextFormat);
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(si.modename));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(si.readdata));
    QStandardItem *standItem3 = new QStandardItem(tr("%1").arg(strreadhex));
    QStandardItem *standItem4 = new QStandardItem(tr("%1").arg(si.writedata));
    QStandardItem *standItem5 = new QStandardItem(tr("%1").arg(strwritetype));
    m_staTCPServerInteract->insertRow(m_staTCPServerInteract->rowCount());
    m_staTCPServerInteract->setItem(m_staTCPServerInteract->rowCount() - 1, 0, standItem1);
    m_staTCPServerInteract->setItem(m_staTCPServerInteract->rowCount() - 1, 1, standItem2);
    m_staTCPServerInteract->setItem(m_staTCPServerInteract->rowCount() - 1, 2, standItem3);
    m_staTCPServerInteract->setItem(m_staTCPServerInteract->rowCount() - 1, 3, standItem4);
    m_staTCPServerInteract->setItem(m_staTCPServerInteract->rowCount() - 1, 4, standItem5);
    m_staTCPServerInteract->item(m_staTCPServerInteract->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerInteract->item(m_staTCPServerInteract->rowCount() - 1, 1)->setTextAlignment(Qt::AlignVCenter);
    m_staTCPServerInteract->item(m_staTCPServerInteract->rowCount() - 1, 2)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerInteract->item(m_staTCPServerInteract->rowCount() - 1, 3)->setTextAlignment(Qt::AlignVCenter);
    m_staTCPServerInteract->item(m_staTCPServerInteract->rowCount() - 1, 4)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief TCP Server 交互发送表格修改
/// \param row
/// \param si
///
void CommDubugAssist::TCPServerInteractRevise(int row, TCPServerInteractData si)
{
    QString strreadhex, strwritehex;
    if (si.readTextFormat == HEX)
    {
        strreadhex = "Hex";
    }
    else
    {
        strreadhex = "Local";
    }
    strwritehex = TextFormat2Str(si.sendTextFormat);
    m_staTCPServerInteract->item(row, 0)->setText(si.modename);
    m_staTCPServerInteract->item(row, 1)->setText(si.readdata);
    m_staTCPServerInteract->item(row, 2)->setText(strreadhex);
    m_staTCPServerInteract->item(row, 3)->setText(si.writedata);
    m_staTCPServerInteract->item(row, 4)->setText(strwritehex);
}

///
/// \brief TCP Server 交互发送表格删除
/// \param row
///
void CommDubugAssist::TCPServerInteractDelete(int row)
{
    m_staTCPServerInteract->removeRow(row);
}

///
/// \brief 清空 TCP Server 交互发送表格
///
void CommDubugAssist::TCPServerInteractClear()
{
    if (m_staTCPServerInteract->rowCount() > 0)
    {
        m_staTCPServerInteract->removeRows(0, m_staTCPServerInteract->rowCount());
    }
}

///
/// \brief TCP Server 交互发送
/// \param ip
/// \param port
/// \param info
///
void CommDubugAssist::TCPServerInteract(QString ip, quint16 port, QByteArray info)
{
    for (int i = 0 ; i < m_TCPServerInteractDatas.count(); i++)
    {
        if (m_TCPServerInteractDatas[i].readip != "ALL Connections")
        {
            if (m_TCPServerInteractDatas[i].readip != ip || m_TCPServerInteractDatas[i].readport != port)
            {
                continue;
            }
        }
        if (m_TCPServerInteractDatas[i].writeip != "ALL Connections")   // 判断要发送的对象是否还在
        {
            bool isOK = false;
            for (int j = 1; j < m_TcpServerClientDatas.count(); j++)
            {
                if (m_TcpServerClientDatas[j].ip == m_TCPServerInteractDatas[i].writeip && m_TcpServerClientDatas[j].port == m_TCPServerInteractDatas[i].writeport)
                {
                    isOK = true;
                    break;
                }
            }
            if (isOK == false)
            {
                continue;
            }
        }
        bool yes = false;
        QString strinfo;
        if (m_TCPServerInteractDatas[i].readTextFormat == HEX)
        {
            if (m_TCPServerInteractDatas[i].readdata == "[alldata]" && m_TCPServerInteractDatas[i].writedata == "[alldata]" && m_TCPServerInteractDatas[i].sendTextFormat == HEX)
            {
                //TCPServerSendLation(m_TCPServerInteractDatas[i].writeip, m_TCPServerInteractDatas[i].writeport, info, m_TCPServerInteractDatas[i].sendTextFormat);
                //TCPServerSendHex(m_TCPServerInteractDatas[i].writeip, m_TCPServerInteractDatas[i].writeport, info);
                TCPServerSendByteArray(m_TCPServerInteractDatas[i].writeip, m_TCPServerInteractDatas[i].writeport, info);
                return ;
            }
            else if (m_TCPServerInteractDatas[i].readdata == "[alldata]")
            {
                TCPServerSendLation(m_TCPServerInteractDatas[i].writeip, m_TCPServerInteractDatas[i].writeport, m_TCPServerInteractDatas[i].writedata, m_TCPServerInteractDatas[i].sendTextFormat);
                return ;
            }
            else
            {
                strinfo = QString::fromLatin1(info);
            }
        }
        else
        {
            strinfo = QString::fromUtf8(info);
        }
        if (m_TCPServerInteractDatas[i].mode == 0)   // 等于
        {
            if (strinfo == m_TCPServerInteractDatas[i].readdata)
            {
                yes = true;
            }
        }
        else if (m_TCPServerInteractDatas[i].mode == 1)    // 包含
        {
            if (strinfo.indexOf(m_TCPServerInteractDatas[i].readdata) != -1)  // 如果查找返回结果等于-1，则表示没有查询到，如果结果不等于-1，则表示查询到，并且返回的值便是开始的位置
            {
                yes = true;
            }
        }
        else if (m_TCPServerInteractDatas[i].mode == 2)   // 首部
        {
            if (strinfo.length() >= m_TCPServerInteractDatas[i].readdata.length())
            {
                if (strinfo.left(m_TCPServerInteractDatas[i].readdata.length()) == m_TCPServerInteractDatas[i].readdata)
                {
                    yes = true;
                }
            }
        }
        else if (m_TCPServerInteractDatas[i].mode == 3)   // 尾部
        {
            if (strinfo.length() >= m_TCPServerInteractDatas[i].readdata.length())
            {
                if (strinfo.right(m_TCPServerInteractDatas[i].readdata.length()) == m_TCPServerInteractDatas[i].readdata)
                {
                    yes = true;
                }
            }
        }
        if (yes)
        {
            if (m_TCPServerInteractDatas[i].sendTextFormat == HEX)
            {
                bool isOK = TCPServerSendHex(m_TCPServerInteractDatas[i].writeip, m_TCPServerInteractDatas[i].writeport, m_TCPServerInteractDatas[i].writedata);
                if (!isOK)
                {
                    return ;
                }
            }
            else
            {
                TCPServerSendLation(m_TCPServerInteractDatas[i].writeip, m_TCPServerInteractDatas[i].writeport, m_TCPServerInteractDatas[i].writedata, m_TCPServerInteractDatas[i].sendTextFormat);
            }
        }
    }
}

///
/// \brief TCP Server 交互发送增加项
///
void CommDubugAssist::on_btn_TCPServerInteractAdd_clicked()
{
    w_TCPS_InterUnit->clear();
    w_TCPS_InterUnit->SetObjects(m_TcpServerClientDatas);
    w_TCPS_InterUnit->SetAdd(true);
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_TCPS_InterUnit->windowFlags();
        w_TCPS_InterUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_TCPS_InterUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_TCPS_InterUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_TCPS_InterUnit->show();
}

///
/// \brief TCP Server 交互发送修改项
///
void CommDubugAssist::on_btn_TCPServerInteractRevise_clicked()
{
    if (m_TCPServerInteractChoose >= 0 && m_TCPServerInteractChoose < m_TCPServerInteractDatas.count())
    {
        w_TCPS_InterUnit->clear();
        w_TCPS_InterUnit->SetObjects(m_TcpServerClientDatas);
        w_TCPS_InterUnit->SetData(m_TCPServerInteractDatas[m_TCPServerInteractChoose]);
        w_TCPS_InterUnit->SetAdd(false);
        if (pTitleBar->getLockState())
        {
            Qt::WindowFlags m_flags = w_TCPS_InterUnit->windowFlags();
            w_TCPS_InterUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
        }
        else
        {
            //Qt::WindowFlags m_flags = nullptr;
            //w_TCPS_InterUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
            w_TCPS_InterUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
        }
        w_TCPS_InterUnit->show();
    }
    else
    {
        Inform("错误！请选择后再修改！");
    }
}

///
/// \brief TCP Server 交互发送删除项
///
void CommDubugAssist::on_btn_TCPServerInteractDelete_clicked()
{
    if (m_TCPServerInteractChoose >= 0 && m_TCPServerInteractChoose < m_TCPServerInteractDatas.count())
    {
        m_TCPServerInteractDatas.removeAt(m_TCPServerInteractChoose);
        TCPServerInteractDelete(m_TCPServerInteractChoose);
        m_TCPServerInteractChoose = -1;
    }
    else
    {
        Inform("错误！请选择后再删除！");
    }
}

void CommDubugAssist::TCPServerSeniorCycleInit()
{
    m_staTCPServerSeniorCycle = new QStandardItemModel();
    m_staTCPServerSeniorCycle->setColumnCount(2);
    m_staTCPServerSeniorCycle->setHeaderData(0, Qt::Horizontal, QStringLiteral("事件名称"));
    m_staTCPServerSeniorCycle->setHeaderData(1, Qt::Horizontal, QStringLiteral("延时(ms)"));
    ui->tableView_TCPServerSeniorSendCycle->setModel(m_staTCPServerSeniorCycle);
    ui->tableView_TCPServerSeniorSendCycle->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView_TCPServerSeniorSendCycle->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView_TCPServerSeniorSendCycle->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);
    ui->tableView_TCPServerSeniorSendCycle->setColumnWidth(0, 180);
    ui->tableView_TCPServerSeniorSendCycle->setColumnWidth(1, 80);
    ui->tableView_TCPServerSeniorSendCycle->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_TCPServerSeniorSendCycle->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPServerSeniorSendCycle->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}
void CommDubugAssist::TCPServerSeniorCycleAdd(TCPServerCycleData sc)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(sc.name));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sc.delay));
    m_staTCPServerSeniorCycle->insertRow(m_staTCPServerSeniorCycle->rowCount());
    m_staTCPServerSeniorCycle->setItem(m_staTCPServerSeniorCycle->rowCount() - 1, 0, standItem1);
    m_staTCPServerSeniorCycle->setItem(m_staTCPServerSeniorCycle->rowCount() - 1, 1, standItem2);
    m_staTCPServerSeniorCycle->item(m_staTCPServerSeniorCycle->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerSeniorCycle->item(m_staTCPServerSeniorCycle->rowCount() - 1, 1)->setTextAlignment(Qt::AlignCenter);
}
void CommDubugAssist::TCPServerSeniorCycleInsert(int row, TCPServerCycleData sc)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(sc.name));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sc.delay));
    m_staTCPServerSeniorCycle->insertRow(row);
    m_staTCPServerSeniorCycle->setItem(row, 0, standItem1);
    m_staTCPServerSeniorCycle->setItem(row, 1, standItem2);
    m_staTCPServerSeniorCycle->item(row, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPServerSeniorCycle->item(row, 1)->setTextAlignment(Qt::AlignCenter);
}
void CommDubugAssist::TCPServerSeniorCycleRevise(int row, TCPServerCycleData sc)
{
    m_staTCPServerSeniorCycle->item(row, 0)->setText(sc.name);
    m_staTCPServerSeniorCycle->item(row, 1)->setText(QString("%1").arg(sc.delay));
}
void CommDubugAssist::TCPServerSeniorCycleSet(QList<TCPServerCycleData> CycleDatas)
{
    for (int i = 0; i < CycleDatas.count(); i++)
    {
        TCPServerSeniorCycleAdd(CycleDatas[i]);
    }
}
void CommDubugAssist::TCPServerSeniorCycleClear()
{
    if (m_staTCPServerSeniorCycle->rowCount() > 0)
    {
        m_staTCPServerSeniorCycle->removeRows(0, m_staTCPServerSeniorCycle->rowCount());
    }
}

///
/// \brief TCPClient 连接与断开
///
void CommDubugAssist::on_btn_TCPClientConnect_clicked()
{
    if (ui->btn_TCPClientConnect->text() == "连接")
    {
        TCPClientConnect(true);
    }
    else if (ui->btn_TCPClientConnect->text() == "退出等待连接")
    {
        TCPClientDisconnect();
    }
    else if (ui->btn_TCPClientConnect->text() == "断开")
    {
        TCPClientDisconnect();
    }
}

void CommDubugAssist::TCPClientConnect(bool informstate)
{
    QString ip = ui->lineEdit_TCPClientConnectIP->text();
    if (ip.isEmpty())
    {
        if (informstate)
        {
            Inform("请输入IP地址");
        }
        return ;
    }
    quint16 port = static_cast<quint16>(ui->lineEdit_TCPClientConnectPort->text().toInt());

    m_tcpclient->Connect(ip, port);
    ui->btn_TCPClientConnect->setText("退出等待连接");
    ui->label_TCPClientConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(120, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
    ui->lineEdit_TCPClientConnectIP->setEnabled(false);
    ui->lineEdit_TCPClientConnectPort->setEnabled(false);

    g_sTCPClientConnectIP = ui->lineEdit_TCPClientConnectIP->text();
    g_un16TCPClientConnectPort = static_cast<quint16>(ui->lineEdit_TCPClientConnectPort->text().toInt());
}
void CommDubugAssist::TCPClientDisconnect()
{
    m_tcpclient->Disconnected();
    ui->btn_TCPClientConnect->setText("连接");
    ui->label_TCPClientConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(0, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
    ui->lineEdit_TCPClientConnectIP->setEnabled(true);
    ui->lineEdit_TCPClientConnectPort->setEnabled(true);
}

///
/// \brief TCPClient 连接成功
///
void CommDubugAssist::TCPClientConnection()
{
    g_sTCPClientConnectIP = ui->lineEdit_TCPClientConnectIP->text();
    g_un16TCPClientConnectPort = static_cast<quint16>(ui->lineEdit_TCPClientConnectPort->text().toInt());
    w_Pref->ReadSettings();
    ui->btn_TCPClientConnect->setText("断开");
    ui->label_TCPClientConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(255, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
    if (g_bConnectSucceedRemind)
        Inform("TCP Client 连接成功！");
}

///
/// \brief TCPClient 连接断开
///
void CommDubugAssist::TCPClientDisconnection()
{
    ui->btn_TCPClientConnect->setText("连接");
    ui->label_TCPClientConnect->setStyleSheet("min-width: 20px;\nbackground-color: rgb(0, 0, 0);\nmin-height: 20px;\nmax-width: 20px;\nmax-height: 20px;\nborder-radius: 10px;\n");
    ui->lineEdit_TCPClientConnectIP->setEnabled(true);
    ui->lineEdit_TCPClientConnectPort->setEnabled(true);
    if (g_bDisconnectRemind)
        Inform("TCP Client 连接已断开");
}

///
/// \brief TCPClient 读取数据
///
void CommDubugAssist::TCPClientReaddata(QByteArray readdata)
{
    QDateTime current_time = QDateTime::currentDateTime();
    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
                       .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
                       .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    //qDebug() << "get client data:" << readdata;
    g_unTCPClientReadnums += static_cast<uint>(readdata.length());
    ui->label_TCPClientReadValue->setText(QString::number(g_unTCPClientReadnums));
    if (g_unTCPClientReadDataLimit != 0)
    {
        if (ui->textEdit_TCPClientRead->toPlainText().length() > static_cast<int>(g_unTCPClientReadDataLimit))
        {
            QString TCPClientreaddatas = ui->textEdit_TCPClientRead->toPlainText();
            TCPClientreaddatas = TCPClientreaddatas.mid(TCPClientreaddatas.length()/2);
            ui->textEdit_TCPClientRead->setText(TCPClientreaddatas);
        }
    }
    if (!ui->checkBox_TCPClientStopShow->isChecked())    // 是否停止显示
    {
        if (ui->checkBox_TCPClientReadHex->isChecked())   // 是否为Hex数据
        {
            if (readdata.isEmpty())   // 如果读取数据为空，则返回退出
            {
                return ;
            }
            //qDebug() << "tcp client read hex:" << readdata;

            const char *recvBuffer = readdata.data();

            QString resultHex;
            QString Hexdata;
            //qDebug() << "tcp client read hex length:" << readdata.length();
            for (int i = 0; i < readdata.length(); i++)
            {
                Hexdata += QString("%1 ").arg(recvBuffer[i], 2, 16, QChar('0')).toUpper().remove("FFFFFFFFFFFFFF");
                //qDebug() << "Hexdata[" << i << "]: " << Hexdata;
            }
            Hexdata.chop(1);  // 删除末尾空格
            resultHex += Hexdata;
            QString socketaddress;
            if (ui->checkBox_TCPClientReadAutoLine->isChecked())   // 换行
            {
                if (ui->checkBox_TCPClientRecordTime->isChecked())
                {
                    socketaddress = "[" + strtime + " R(Hex)] ";
                }
                else
                {
                    socketaddress = "[R(Hex)] ";
                }
            }
            else
            {
                if ((!ui->textEdit_TCPClientRead->toPlainText().isEmpty()) && ui->textEdit_TCPClientRead->toPlainText().right(1) != ' ' && ui->textEdit_TCPClientRead->toPlainText().right(1) != '\n')
                {
                    socketaddress = " ";
                }
            }
            resultHex = socketaddress + resultHex;
            TCPClientShowAddData(resultHex);
        }
        else
        {
            QString strReadData;
            switch (g_emTCPClientReadTextFormat) {
            case HEX:
            case Local: strReadData = QString::fromLocal8Bit(readdata); break;
            case UTF8: strReadData = QString::fromUtf8(readdata); break;
            case GBK: strReadData = QString::fromUtf8(GBK2utf8(readdata)); break;
            case Unicode: strReadData = QString::fromUtf8(unicodeToUtf8(readdata)); break;
            }

            QString socketaddress;
            QString data;
            if (ui->checkBox_TCPClientReadAutoLine->isChecked())
            {
                if (ui->checkBox_TCPClientRecordTime->isChecked())
                {
                    socketaddress = "[" + strtime + " R] ";
                }
                else
                {
                    socketaddress = "[R] ";
                }
                data = socketaddress + strReadData;
            }
            else
            {
                data = QString::fromUtf8(readdata);
            }
            TCPClientShowAddData(data);
        }
    }
    TCPClientInteract(readdata);
}

///
/// \brief 限制串口数据数量（防止显示数据超过内存造成卡顿
///
void CommDubugAssist::TCPClientReadDataLimit()
{
    if (g_unTCPClientReadDataLimit != 0)
    {
        if (ui->textEdit_TCPClientRead->toPlainText().length() > static_cast<int>(g_unTCPClientReadDataLimit))
        {
            QString readdatas = ui->textEdit_TCPClientRead->toPlainText();
            QString middata = readdatas.mid(readdatas.length()/4);
            int indexnum = middata.indexOf('\n') + 1;
            if (indexnum <= static_cast<int>(g_unTCPClientReadDataLimit)/4)
            {
                readdatas = middata.mid(indexnum);
            }
            else
            {
                readdatas = middata;
            }
            ui->textEdit_TCPClientRead->clear();
            ui->textEdit_TCPClientRead->setText(readdatas);
        }
    }
}

///
/// \brief TCP Client 添加数据通信显示信息
/// \param data
///
void CommDubugAssist::TCPClientShowAddData(QString data)
{
    if (data.isEmpty())
    {
        return ;
    }
    TCPClientReadDataLimit();

    if (ui->checkBox_TCPClientReadAutoLine->isChecked())
    {
        ui->textEdit_TCPClientRead->append(data);
    }
    else
    {
        ui->textEdit_TCPClientRead->moveCursor(QTextCursor::End);
        ui->textEdit_TCPClientRead->insertPlainText(data);
    }

    if(ui->checkBox_TCPClientReadCurSor->isChecked())   // 光标是否跟踪到最新位置
    {
        ui->textEdit_TCPClientRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
    }
}

///
/// \brief TCP Client 发送数据
///
void CommDubugAssist::on_btn_TCPClientSend_clicked()
{
    if (m_tcpclient->isConnect())
    {
        QString senddata = ui->textEdit_TCPClientSend->toPlainText();
        if (senddata.isEmpty())
        {
            Inform("发送内容为空！");
            return ;
        }
        if (ui->checkBox_TCPClientSendHex->isChecked())
        {
            bool isOK = TCPClientSendHex(senddata);
            if (!isOK)
            {
                Inform("数据转换错误！请检查要发送的数据是否为16进制数据!");
            }
        }
        else
        {
            TCPClientSendLation(senddata, g_emTCPClientSendTextFormat);
        }
        if (ui->checkBox_TCPClientSendAutoClear->isChecked())  // 判断发送后是否清空
        {
            ui->textEdit_TCPClientSend->clear();
        }
    }
    else
    {
        Inform("TCP Client尚未连接！");
    }
}

///
/// \brief TCP Client 简易循环发送
///
void CommDubugAssist::TCPClientSendCycle()
{
    if (m_nTCPClientCycleTime == -1 || m_nTCPClientCycleTime == -2)
    {
        ui->checkBox_TCPClientSendCyCle->setChecked(false);
        m_timerTCPClientCycle->stop();
        if (m_nTCPClientCycleTime == -1)
        {
            Inform("发送内容为空！！");
        }
        else if (m_nTCPClientCycleTime == -2)
        {
            Inform("当前未连接服务器！");
        }
        return ;
    }
    if (m_tcpclient->isOpen())
    {
        QString senddata = ui->lineEdit_TCPClientSendCycleText->text();
        if (ui->checkBox_TCPClientSendCyCleHex->isChecked())
        {
            if (!TCPClientSendHex(senddata))
            {
                ui->spinBox_TCPClientSendCycle->setEnabled(false);
                ui->lineEdit_TCPClientSendCycleText->setEnabled(false);
                ui->checkBox_TCPClientSendCyCle->setChecked(false);
                m_timerTCPClientCycle->stop();
                Inform("Hex数据转换错误！");
            }
        }
        else
        {
            TCPClientSendLation(senddata, g_emTCPClientSendTextFormat);
        }
    }
    else
    {
        ui->checkBox_TCPClientSendCyCle->setChecked(false);
        m_timerTCPClientCycle->stop();
        Inform("与服务器连接已断开！");
    }
}

///
/// \brief TCP Client 高级循环发送数据
/// \param num
///
void CommDubugAssist::TCPClientSeniorCycleSendData(int num)
{
    if (ui->btn_TCPClientSeniorSendCycleStart->text() == "终止循环")
    {
        TCPClientSendData(m_TCPClientCycleDatas[num].senddata, m_TCPClientCycleDatas[num].isHex);
        ui->label_TCPClientSeniorSendCycle->setText("已发送到第 " + QString::number(num + 1) + " 项：" + m_TCPClientCycleDatas[num].name);
    }
}

///
/// \brief TCP Client 发送数据
/// \param senddata
/// \param isHex
///
void CommDubugAssist::TCPClientSendData(QString senddata, bool isHex)
{
    if (isHex)
    {
        TCPClientSendHex(senddata);
    }
    else
    {
        TCPClientSendLation(senddata, g_emTCPClientSendTextFormat);
    }
}

///
/// \brief TCP Client发送文本数据
/// \param senddata
/// \return
///
bool CommDubugAssist::TCPClientSendLation(QString senddata, TextFormat sendtextformat)
{
    QByteArray bytesdata = senddata.toUtf8();
    if (ui->checkBox_TCPClientSendAddLine->isChecked())
    {
        bytesdata.append('\n');
    }
    //qDebug() << "tcp client senddata:" << bytesdata;

    QDateTime current_time = QDateTime::currentDateTime();
    g_unTCPClientSendnums += static_cast<uint>(bytesdata.length());
    ui->label_TCPClientSendValue->setText(QString::number(g_unTCPClientSendnums));
    switch (sendtextformat) {
    case HEX:
    case Local: bytesdata = senddata.toLocal8Bit(); break;
    case UTF8: bytesdata = senddata.toUtf8(); break;
    case GBK: bytesdata = utf8ToGBK(senddata.toUtf8()); break;
    case Unicode: bytesdata = utf8ToUnicode(senddata.toUtf8()); break;
    }
    m_tcpclient->WriteData(bytesdata);

    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_TCPClientSendHistory->isChecked())
    {
        TCPClientAddSendHistory(strtime, "Ascii", senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_TCPClientStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_TCPClientRecordSend->isChecked())  // 是否需要显示到发送区
        {
            if (ui->checkBox_TCPClientReadAutoLine->isChecked())   // 是否自动换行
            {
                if (ui->checkBox_TCPClientRecordTime->isChecked())   // 判断是否要添加时间
                {
                    ui->textEdit_TCPClientRead->append("[" + strtime + " W] " + senddata);
                }
                else
                {
                    ui->textEdit_TCPClientRead->append("[W] " + senddata);
                }
                if(ui->checkBox_TCPClientReadCurSor->isChecked())   // 光标是否跟踪到最新位置
                {
                    ui->textEdit_TCPClientRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
                }
            }
        }
    }
    return true;
}

///
/// \brief TCP Client发送Hex数据
/// \param senddata
/// \return
///
bool CommDubugAssist::TCPClientSendHex(QString senddata)
{
    if (senddata.left(2) == "0x" || senddata.left(2) == "0X")
    {
        senddata = senddata.mid(2);
    }
    bool isOK = toHex(senddata);
    if (!isOK)
    {
        return false;
    }
    QByteArray bytesToSend;
    bytesToSend = QByteArray::fromHex( senddata.remove(" ").toLatin1() );
    if (ui->checkBox_TCPClientSendAddLine->isChecked())
    {
        bytesToSend.append('\n');
    }
    //qDebug() << "tcp client send hex:" << bytesToSend;
    m_tcpclient->WriteData(bytesToSend);
    QDateTime current_time = QDateTime::currentDateTime();
    g_unTCPClientSendnums += static_cast<uint>(bytesToSend.length());
    ui->label_TCPClientSendValue->setText(QString::number(g_unTCPClientSendnums));

    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_TCPClientSendHistory->isChecked())
    {
        TCPClientAddSendHistory(strtime, "Hex", senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_TCPClientStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_TCPClientRecordSend->isChecked())  // 显示到发送区
        {
            if (ui->checkBox_TCPClientRecordTime->isChecked())   // 判断是否要添加时间
            {
                ui->textEdit_TCPClientRead->append("[" + strtime + " W(Hex)] " + senddata);
            }
            else
            {
                ui->textEdit_TCPClientRead->append("[W(Hex)] " + senddata);
            }
            if(ui->checkBox_TCPClientReadCurSor->isChecked())   // 光标是否跟踪到最新位置
            {
                ui->textEdit_TCPClientRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
            }
        }
    }
    return true;
}

bool CommDubugAssist::TCPClientSendByteArray(QByteArray senddata)
{
    QByteArray bytesdata = senddata;
    //qDebug() << "tcp client senddata:" << bytesdata;

    QDateTime current_time = QDateTime::currentDateTime();
    g_unTCPClientSendnums += static_cast<uint>(bytesdata.length());
    ui->label_TCPClientSendValue->setText(QString::number(g_unTCPClientSendnums));
    m_tcpclient->WriteData(bytesdata);

    QString strtime = QString("%1-%2-%3 %4:%5:%6.%7").arg(current_time.toString("yyyy")).arg(current_time.toString("MM"))
            .arg(current_time.toString("dd")).arg(current_time.toString("hh")).arg(current_time.toString("mm"))
            .arg(current_time.toString("ss")).arg(current_time.toString("zzz"));
    if (ui->checkBox_TCPClientSendHistory->isChecked())
    {
        TCPClientAddSendHistory(strtime, "Ascii", senddata);  // 增加一条发送记录
    }
    if (!ui->checkBox_TCPClientStopShow->isChecked())  // 判断是否开启停止显示，未开启则显示
    {
        if (ui->checkBox_TCPClientRecordSend->isChecked())  // 是否需要显示到发送区
        {
            if (ui->checkBox_TCPClientReadAutoLine->isChecked())   // 是否自动换行
            {
                if (ui->checkBox_TCPClientRecordTime->isChecked())   // 判断是否要添加时间
                {
                    ui->textEdit_TCPClientRead->append("[" + strtime + " W] " + senddata);
                }
                else
                {
                    ui->textEdit_TCPClientRead->append("[W] " + senddata);
                }
                if(ui->checkBox_TCPClientReadCurSor->isChecked())   // 光标是否跟踪到最新位置
                {
                    ui->textEdit_TCPClientRead->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);//光标自动跟踪
                }
            }
        }
    }
    return true;
}

///
/// \brief TCP Client 清空接收
///
void CommDubugAssist::on_btn_TCPClientReadClear_clicked()
{
    ui->textEdit_TCPClientRead->clear();
}

///
/// \brief TCP Client 清空发送
///
void CommDubugAssist::on_btn_TCPClientSendClear_clicked()
{
    ui->textEdit_TCPClientSend->clear();
}

///
/// \brief TCP Client 发送记录初始化
///
void CommDubugAssist::TCPClientSendHistoryInit()
{
    m_staTCPClientHistoryModel = new QStandardItemModel();
    m_staTCPClientHistoryModel->setColumnCount(3);
    m_staTCPClientHistoryModel->setHeaderData(0, Qt::Horizontal, QStringLiteral("时间"));
    m_staTCPClientHistoryModel->setHeaderData(1, Qt::Horizontal, QStringLiteral("数据类型"));
    m_staTCPClientHistoryModel->setHeaderData(2, Qt::Horizontal, QStringLiteral("发送内容"));
    ui->tableView_TCPClientSendHistory->setModel(m_staTCPClientHistoryModel);
    ui->tableView_TCPClientSendHistory->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);  // 表头信息显示居中
    ui->tableView_TCPClientSendHistory->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed); // Fixed 为设置列宽不可变
    ui->tableView_TCPClientSendHistory->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed); // Stretch 为设置列宽可变
    ui->tableView_TCPClientSendHistory->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableView_TCPClientSendHistory->setColumnWidth(0, 160);
    ui->tableView_TCPClientSendHistory->setColumnWidth(1, 55);// 设置列的宽度
    ui->tableView_TCPClientSendHistory->setColumnWidth(2, 100);

    ui->tableView_TCPClientSendHistory->setSelectionMode(QAbstractItemView::SingleSelection); // 设定只能选择一行，不能选择多行
    ui->tableView_TCPClientSendHistory->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPClientSendHistory->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief TCP Client 增加一条发送记录
/// \param datatime
/// \param type
/// \param senddata
///
void CommDubugAssist::TCPClientAddSendHistory(QString datatime, QString type, QString senddata)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(datatime));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(type));
    QStandardItem *standItem3 = new QStandardItem(tr("%1").arg(senddata));
    m_staTCPClientHistoryModel->insertRow(0);
    m_staTCPClientHistoryModel->setItem(0, 0, standItem1);
    m_staTCPClientHistoryModel->setItem(0, 1, standItem2);
    m_staTCPClientHistoryModel->setItem(0, 2, standItem3);
    m_staTCPClientHistoryModel->item(0, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPClientHistoryModel->item(0, 1)->setTextAlignment(Qt::AlignCenter);
    m_staTCPClientHistoryModel->item(0, 2)->setTextAlignment(Qt::AlignVCenter);
    if (g_unTCPClientHistoryDatanumsLimit > 0)
    {
        while (m_staTCPClientHistoryModel->rowCount() > static_cast<int>(g_unTCPClientHistoryDatanumsLimit))
        {
            m_staTCPClientHistoryModel->removeRow(m_staTCPClientHistoryModel->rowCount() - 1);
        }
    }
}

///
/// \brief TCP Client 快捷发送表格初始化
///
void CommDubugAssist::TCPClientQuickSendInit()
{
    m_staTCPClientQuickSend = new QStandardItemModel();
    m_staTCPClientQuickSend->setColumnCount(1);
    m_staTCPClientQuickSend->setHeaderData(0, Qt::Horizontal, QStringLiteral("快捷发送"));
    ui->tableView_TCPClientQuickSend->setModel(m_staTCPClientQuickSend);
    ui->tableView_TCPClientQuickSend->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);  // 表头信息显示居中
    ui->tableView_TCPClientQuickSend->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView_TCPClientQuickSend->setSelectionMode(QAbstractItemView::SingleSelection); // 设定只能选择一行，不能选择多行
    ui->tableView_TCPClientQuickSend->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPClientQuickSend->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
    ui->tableView_TCPClientQuickSend->horizontalHeader()->hide();
}

///
/// \brief TCP Client 快捷发送表格添加
/// \param name
///
void CommDubugAssist::TCPClientQuickSendAdd(QString name)
{
    QStandardItem *standItem = new QStandardItem(tr("%1").arg(name));
    m_staTCPClientQuickSend->appendRow(standItem);
    m_staTCPClientQuickSend->item(m_staTCPClientQuickSend->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief TCP Client 快捷发送表格修改
/// \param row
/// \param name
///
void CommDubugAssist::TCPClientQuickSendRevise(int row, QString name)
{
    m_staTCPClientQuickSend->item(row)->setText(name);
}

///
/// \brief TCP Client 快捷发送表格删除
/// \param row
///
void CommDubugAssist::TCPClientQuickSendDelete(int row)
{
    m_staTCPClientQuickSend->removeRow(row);
}

///
/// \brief 清空 TCP Client 快捷发送表格
///
void CommDubugAssist::TCPClientQuickSendClear()
{
    if (m_staTCPClientQuickSend->rowCount() > 0)
    {
        m_staTCPClientQuickSend->removeRows(0, m_staTCPClientQuickSend->rowCount());
    }
}

///
/// \brief TCP Client 快捷发送添加
///
void CommDubugAssist::on_btn_TCPClientQuickSendAdd_clicked()
{
    w_QSUnit->clear();
    w_QSUnit->setUInum(2);
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_QSUnit->windowFlags();
        w_QSUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_QSUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_QSUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_QSUnit->show();
    w_QSUnit->setAdd(true);
}

///
/// \brief TCP Client 快捷发送修改
///
void CommDubugAssist::on_btn_TCPClientQuickSendRevise_clicked()
{
    if (m_TCPClientQuickSendChoose >= 0 && m_TCPClientQuickSendChoose < m_TCPClientQuickSendDatas.count())
    {
        w_QSUnit->setUInum(2);
        if (pTitleBar->getLockState())
        {
            Qt::WindowFlags m_flags = w_QSUnit->windowFlags();
            w_QSUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
        }
        else
        {
            //Qt::WindowFlags m_flags = nullptr;
            //w_QSUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
            w_QSUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
        }
        w_QSUnit->show();
        w_QSUnit->setAdd(false);
        w_QSUnit->setdata(m_TCPClientQuickSendDatas[m_TCPClientQuickSendChoose]);
    }
    else
    {
        Inform("错误！请选择后再修改！");
    }
}

///
/// \brief TCP Client 快捷发送删除
///
void CommDubugAssist::on_btn_TCPClientQuickSendDelete_clicked()
{
    if (m_TCPClientQuickSendChoose >= 0 && m_TCPClientQuickSendChoose < m_TCPClientQuickSendDatas.count())
    {
        m_TCPClientQuickSendDatas.removeAt(m_TCPClientQuickSendChoose);
        TCPClientQuickSendDelete(m_TCPClientQuickSendChoose);
        m_TCPClientQuickSendChoose = -1;
    }
    else
    {
        Inform("错误！请选择后再删除！");
    }
}

///
/// \brief TCP Client 交互发送表格初始化
///
void CommDubugAssist::TCPClientInteractInit()
{
    m_staTCPClientInteract = new QStandardItemModel();
    m_staTCPClientInteract->setColumnCount(5);
    m_staTCPClientInteract->setHeaderData(0, Qt::Horizontal, QStringLiteral("类型"));
    m_staTCPClientInteract->setHeaderData(1, Qt::Horizontal, QStringLiteral("接收"));
    m_staTCPClientInteract->setHeaderData(2, Qt::Horizontal, QStringLiteral("接收类型"));
    m_staTCPClientInteract->setHeaderData(3, Qt::Horizontal, QStringLiteral("发送"));
    m_staTCPClientInteract->setHeaderData(4, Qt::Horizontal, QStringLiteral("发送类型"));
    ui->tableView_TCPClientInteract->setModel(m_staTCPClientInteract);
    ui->tableView_TCPClientInteract->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView_TCPClientInteract->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    ui->tableView_TCPClientInteract->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tableView_TCPClientInteract->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
    ui->tableView_TCPClientInteract->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    ui->tableView_TCPClientInteract->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Fixed);
    ui->tableView_TCPClientInteract->setColumnWidth(0, 80);
    ui->tableView_TCPClientInteract->setColumnWidth(1, 200);
    ui->tableView_TCPClientInteract->setColumnWidth(2, 60);
    ui->tableView_TCPClientInteract->setColumnWidth(3, 200);
    ui->tableView_TCPClientInteract->setColumnWidth(4, 60);
    ui->tableView_TCPClientInteract->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_TCPClientInteract->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPClientInteract->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}

///
/// \brief TCP Client 交互发送表格添加
/// \param si
///
void CommDubugAssist::TCPClientInteractAdd(InteractData si)
{
    QString strreadhex, strwritehex;
    if (si.readTextFormat == HEX)
    {
        strreadhex = "Hex";
    }
    else
    {
        strreadhex = "Ascii";
    }
    strwritehex = TextFormat2Str(si.sendTextFormat);
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(si.modename));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(si.readdata));
    QStandardItem *standItem3 = new QStandardItem(tr("%1").arg(strreadhex));
    QStandardItem *standItem4 = new QStandardItem(tr("%1").arg(si.writedata));
    QStandardItem *standItem5 = new QStandardItem(tr("%1").arg(strwritehex));
    m_staTCPClientInteract->insertRow(m_staTCPClientInteract->rowCount());
    m_staTCPClientInteract->setItem(m_staTCPClientInteract->rowCount() - 1, 0, standItem1);
    m_staTCPClientInteract->setItem(m_staTCPClientInteract->rowCount() - 1, 1, standItem2);
    m_staTCPClientInteract->setItem(m_staTCPClientInteract->rowCount() - 1, 2, standItem3);
    m_staTCPClientInteract->setItem(m_staTCPClientInteract->rowCount() - 1, 3, standItem4);
    m_staTCPClientInteract->setItem(m_staTCPClientInteract->rowCount() - 1, 4, standItem5);
    m_staTCPClientInteract->item(m_staTCPClientInteract->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPClientInteract->item(m_staTCPClientInteract->rowCount() - 1, 1)->setTextAlignment(Qt::AlignVCenter);
    m_staTCPClientInteract->item(m_staTCPClientInteract->rowCount() - 1, 2)->setTextAlignment(Qt::AlignCenter);
    m_staTCPClientInteract->item(m_staTCPClientInteract->rowCount() - 1, 3)->setTextAlignment(Qt::AlignVCenter);
    m_staTCPClientInteract->item(m_staTCPClientInteract->rowCount() - 1, 4)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief TCP Client 交互发送表格修改
/// \param row
/// \param si
///
void CommDubugAssist::TCPClientInteractRevise(int row, InteractData si)
{
    QString strreadhex, strwritehex;
    if (si.readTextFormat == HEX)
    {
        strreadhex = "Hex";
    }
    else
    {
        strreadhex = "Ascii";
    }
    strwritehex = TextFormat2Str(si.sendTextFormat);
    m_staTCPClientInteract->item(row, 0)->setText(si.modename);
    m_staTCPClientInteract->item(row, 1)->setText(si.readdata);
    m_staTCPClientInteract->item(row, 2)->setText(strreadhex);
    m_staTCPClientInteract->item(row, 3)->setText(si.writedata);
    m_staTCPClientInteract->item(row, 4)->setText(strwritehex);
}

///
/// \brief TCP Client 交互发送表格删除
/// \param row
///
void CommDubugAssist::TCPClientInteractDelete(int row)
{
    m_staTCPClientInteract->removeRow(row);
}

///
/// \brief 清空 TCP Client 交互发送表格
///
void CommDubugAssist::TCPClientInteractClear()
{
    if(m_staTCPClientInteract->rowCount() > 0)
    {
        m_staTCPClientInteract->removeRows(0, m_staTCPClientInteract->rowCount());
    }
}

///
/// \brief TCP Client 交互发送判断
/// \param info
///
void CommDubugAssist::TCPClientInteract(QByteArray info)
{
    for (int i = 0 ; i < m_TCPClientInteractDatas.count(); i++)
    {
        bool yes = false;
        QString strinfo;
        if (m_TCPClientInteractDatas[i].readTextFormat == HEX)
        {
            if (m_TCPClientInteractDatas[i].readdata == "[alldata]" && m_TCPClientInteractDatas[i].writedata == "[alldata]" && m_TCPClientInteractDatas[i].sendTextFormat == HEX)
            {
                TCPClientSendByteArray(info);
                return ;
            }
            else if (m_TCPClientInteractDatas[i].readdata == "[alldata]")
            {
                TCPClientSendLation(m_TCPClientInteractDatas[i].writedata, m_TCPClientInteractDatas[i].sendTextFormat);
                return ;
            }
            else
            {
                strinfo = QString::fromLatin1(info);
            }
        }
        else
        {
            strinfo = QString::fromUtf8(info);
        }
        if (m_TCPClientInteractDatas[i].mode == 0)   // 等于
        {
            if (strinfo == m_TCPClientInteractDatas[i].readdata)
            {
                yes = true;
            }
        }
        else if (m_TCPClientInteractDatas[i].mode == 1)    // 包含
        {
            if (strinfo.indexOf(m_TCPClientInteractDatas[i].readdata) != -1)  // 如果查找返回结果等于-1，则表示没有查询到，如果结果不等于-1，则表示查询到，并且返回的值便是开始的位置
            {
                yes = true;
            }
        }
        else if (m_TCPClientInteractDatas[i].mode == 2)   // 首部
        {
            if (strinfo.length() >= m_TCPClientInteractDatas[i].readdata.length())
            {
                if (strinfo.left(m_TCPClientInteractDatas[i].readdata.length()) == m_TCPClientInteractDatas[i].readdata)
                {
                    yes = true;
                }
            }
        }
        else if (m_TCPClientInteractDatas[i].mode == 3)   // 尾部
        {
            if (strinfo.length() >= m_TCPClientInteractDatas[i].readdata.length())
            {
                if (strinfo.right(m_TCPClientInteractDatas[i].readdata.length()) == m_TCPClientInteractDatas[i].readdata)
                {
                    yes = true;
                }
            }
        }
        if (yes)
        {
            if (m_TCPClientInteractDatas[i].sendTextFormat == HEX)
            {
                bool isOK = TCPClientSendHex(m_TCPClientInteractDatas[i].writedata);
                if (!isOK)
                {
                    return ;
                }
            }
            else
            {
                TCPClientSendLation(m_TCPClientInteractDatas[i].writedata, m_TCPClientInteractDatas[i].sendTextFormat);
            }
        }
    }
}

///
/// \brief TCP Client 交互发送添加项
///
void CommDubugAssist::on_btn_TCPClientInteractAdd_clicked()
{
    w_InterUnit->clear();
    w_InterUnit->SetUInum(2);
    w_InterUnit->SetAdd(true);
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_InterUnit->windowFlags();
        w_InterUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_InterUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_InterUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_InterUnit->show();
}

///
/// \brief TCP Client 交互发送修改项
///
void CommDubugAssist::on_btn_TCPClientInteractRevise_clicked()
{
    if (m_TCPClientInteractChoose >= 0 && m_TCPClientInteractChoose < m_TCPClientInteractDatas.count())
    {
        w_InterUnit->SetData(m_TCPClientInteractDatas[m_TCPClientInteractChoose]);
        w_InterUnit->SetAdd(false);
        w_InterUnit->SetUInum(2);
        if (pTitleBar->getLockState())
        {
            Qt::WindowFlags m_flags = w_InterUnit->windowFlags();
            w_InterUnit->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
        }
        else
        {
            //Qt::WindowFlags m_flags = nullptr;
            //w_InterUnit->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
            w_InterUnit->window()->setWindowFlags(Qt::FramelessWindowHint);
        }
        w_InterUnit->show();
    }
    else
    {
        Inform("错误！请选择后再修改！");
    }
}

///
/// \brief TCP Client 交互发送删除项
///
void CommDubugAssist::on_btn_TCPClientInteractDelete_clicked()
{
    if (m_TCPClientInteractChoose >= 0 && m_TCPClientInteractChoose < m_TCPClientInteractDatas.count())
    {
        m_TCPClientInteractDatas.removeAt(m_TCPClientInteractChoose);
        TCPClientInteractDelete(m_TCPClientInteractChoose);
        m_TCPClientInteractChoose = -1;
    }
    else
    {
        Inform("错误！请选择后再删除！");
    }
}

///
/// \brief TCP Client 高级循环表格初始化
///
void CommDubugAssist::TCPClientSeniorCycleInit()
{
    m_staTCPClientSeniorCycle = new QStandardItemModel();
    m_staTCPClientSeniorCycle->setColumnCount(2);
    m_staTCPClientSeniorCycle->setHeaderData(0, Qt::Horizontal, QStringLiteral("事件名称"));
    m_staTCPClientSeniorCycle->setHeaderData(1, Qt::Horizontal, QStringLiteral("延时(ms)"));
    ui->tableView_TCPClientSeniorSendCycle->setModel(m_staTCPClientSeniorCycle);
    ui->tableView_TCPClientSeniorSendCycle->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView_TCPClientSeniorSendCycle->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView_TCPClientSeniorSendCycle->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);
    ui->tableView_TCPClientSeniorSendCycle->setColumnWidth(0, 180);
    ui->tableView_TCPClientSeniorSendCycle->setColumnWidth(1, 80);
    ui->tableView_TCPClientSeniorSendCycle->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_TCPClientSeniorSendCycle->setSelectionBehavior(QAbstractItemView::SelectRows);  // 设置选中时整行选中
    ui->tableView_TCPClientSeniorSendCycle->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置表格属性只读，不能编辑
}
///
/// \brief TCP Client 高级循环表格添加
/// \param sc
///
void CommDubugAssist::TCPClientSeniorCycleAdd(CycleData sc)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(sc.name));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sc.delay));
    m_staTCPClientSeniorCycle->insertRow(m_staTCPClientSeniorCycle->rowCount());
    m_staTCPClientSeniorCycle->setItem(m_staTCPClientSeniorCycle->rowCount() - 1, 0, standItem1);
    m_staTCPClientSeniorCycle->setItem(m_staTCPClientSeniorCycle->rowCount() - 1, 1, standItem2);
    m_staTCPClientSeniorCycle->item(m_staTCPClientSeniorCycle->rowCount() - 1, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPClientSeniorCycle->item(m_staTCPClientSeniorCycle->rowCount() - 1, 1)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief TCP Client 高级循环表格插入
/// \param row
/// \param sc
///
void CommDubugAssist::TCPClientSeniorCycleInsert(int row, CycleData sc)
{
    QStandardItem *standItem1 = new QStandardItem(tr("%1").arg(sc.name));
    QStandardItem *standItem2 = new QStandardItem(tr("%1").arg(sc.delay));
    m_staTCPClientSeniorCycle->insertRow(row);
    m_staTCPClientSeniorCycle->setItem(row, 0, standItem1);
    m_staTCPClientSeniorCycle->setItem(row, 1, standItem2);
    m_staTCPClientSeniorCycle->item(row, 0)->setTextAlignment(Qt::AlignCenter);
    m_staTCPClientSeniorCycle->item(row, 1)->setTextAlignment(Qt::AlignCenter);
}

///
/// \brief TCP Client 高级循环表格修改
/// \param row
/// \param sc
///
void CommDubugAssist::TCPClientSeniorCycleRevise(int row, CycleData sc)
{
    m_staTCPClientSeniorCycle->item(row, 0)->setText(sc.name);
    m_staTCPClientSeniorCycle->item(row, 1)->setText(QString("%1").arg(sc.delay));
}

///
/// \brief TCP Client 高级循环数据设置
/// \param CycleDatas
///
void CommDubugAssist::TCPClientSeniorCycleSet(QList<CycleData> CycleDatas)
{
    for (int i = 0; i < CycleDatas.count(); i++)
    {
        TCPClientSeniorCycleAdd(CycleDatas[i]);
    }
}

///
/// \brief TCP Client 高级循环数据清空
///
void CommDubugAssist::TCPClientSeniorCycleClear()
{
    if (m_staTCPClientSeniorCycle->rowCount() > 0)
    {
        m_staTCPClientSeniorCycle->removeRows(0, m_staTCPClientSeniorCycle->rowCount());
    }
}

///
/// \brief TCP Server 读取变动客户端接收数据的槽函数
/// \param arg1
///
void CommDubugAssist::on_comboBox_TCPServerReadTarget_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "NULL")
    {
        ui->textEdit_TCPServerRead->setText(m_TcpServerClientDatas[0].ShowData);
        return ;
    }
    else if (arg1 == "ALL Connections")
    {
        ui->textEdit_TCPServerRead->setText(m_TcpServerClientDatas[0].ShowData);
        return ;
    }
    QString checkip = arg1.section(':', 0, 0);
    quint16 checkport = static_cast<quint16>(arg1.section(':', 1, 1).toInt());
    int num = TCPServerGetClientNum(checkip, checkport);
    if (num > 0)
    {
        ui->textEdit_TCPServerRead->setText(m_TcpServerClientDatas[num].ShowData);
    }
}

///
/// \brief 开启/关闭 TCP Server 简易循环发送
/// \param arg1
///
void CommDubugAssist::on_checkBox_TCPServerSendCyCle_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        if (ui->comboBox_TCPServerSendCycleTarget->currentText() != "NULL")
        {
            if (ui->lineEdit_TCPServerSendCycleText->text().isEmpty())
            {
                m_nTCPServerCycleTime = -1;
                m_timerTCPServerCycle->start(1);
            }
            else
            {
                m_sTCPServerCycleObject = ui->comboBox_TCPServerSendCycleTarget->currentText();
                if (m_sTCPServerCycleObject == "ALL Connections")
                {
                    m_sTCPServerCycleIP = m_sTCPServerCycleObject;
                    m_nTCPServerCyclePort = 0;
                }
                else
                {
                    m_sTCPServerCycleIP = m_sTCPServerCycleObject.section(':', 0, 0);
                    m_nTCPServerCyclePort = static_cast<quint16>(m_sTCPServerCycleObject.section(':', 1, 1).toInt());
                }
                ui->spinBox_TCPServerSendCycle->setEnabled(false);
                ui->comboBox_TCPServerSendCycleTarget->setEnabled(false);
                ui->lineEdit_TCPServerSendCycleText->setEnabled(false);
                ui->label_TCPServerSendCycleText->setEnabled(false);
                ui->label_TCPServerSendCyclems->setEnabled(false);
                m_nTCPServerCycleTime = ui->spinBox_TCPServerSendCycle->value();
                m_timerTCPServerCycle->start(m_nTCPServerCycleTime);
            }
        }
        else
        {
            m_nTCPServerCycleTime = -2;
            m_timerTCPServerCycle->start(1);
        }
    }
    else if (arg1 == Qt::Unchecked)
    {
        ui->spinBox_TCPServerSendCycle->setEnabled(true);
        ui->comboBox_TCPServerSendCycleTarget->setEnabled(true);
        ui->lineEdit_TCPServerSendCycleText->setEnabled(true);
        ui->label_TCPServerSendCycleText->setEnabled(true);
        ui->label_TCPServerSendCyclems->setEnabled(true);
        if (m_timerTCPServerCycle->isActive())
        {
            m_timerTCPServerCycle->stop();
        }
    }
}

///
/// \brief 开启/关闭 TCP Client 简易循环发送
/// \param arg1
///
void CommDubugAssist::on_checkBox_TCPClientSendCyCle_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        if (m_tcpclient->isOpen())
        {
            if (ui->lineEdit_TCPClientSendCycleText->text().isEmpty())
            {
                m_nTCPClientCycleTime = -1;
                m_timerTCPClientCycle->start(1);
            }
            else
            {
                ui->spinBox_TCPClientSendCycle->setEnabled(false);
                ui->lineEdit_TCPClientSendCycleText->setEnabled(false);
                ui->label_TCPClientSendCyclems->setEnabled(false);
                ui->label_TCPClientSendCycleText->setEnabled(false);
                m_nTCPClientCycleTime = ui->spinBox_TCPClientSendCycle->value();
                m_timerTCPClientCycle->start(m_nTCPClientCycleTime);
            }
        }
        else
        {
            m_nTCPClientCycleTime = -2;
            m_timerTCPClientCycle->start(1);
        }
    }
    else if (arg1 == Qt::Unchecked)
    {
        ui->spinBox_TCPClientSendCycle->setEnabled(true);
        ui->lineEdit_TCPClientSendCycleText->setEnabled(true);
        ui->label_TCPClientSendCyclems->setEnabled(true);
        ui->label_TCPClientSendCycleText->setEnabled(true);
        if (m_timerTCPClientCycle->isActive())
        {
            m_timerTCPClientCycle->stop();
        }
    }
}

///
/// \brief 双击TCP Server 发送记录进行发送
/// \param index
///
void CommDubugAssist::on_tableView_TCPServerSendHistory_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    QString sendobject = m_staTCPServerHistoryModel->item(row, 1)->text();
    QString ip;
    quint16 port;
    if (sendobject == "ALL Connections")
    {
        ip = sendobject;
        port = 0;
        if (ui->comboBox_TCPServerReadTarget->currentText() == "NULL")
        {
            Inform("当前无客户端连接！");
            return ;
        }
    }
    else
    {
        ip = sendobject.section(':', 0, 0);
        port = static_cast<quint16>(sendobject.section(':', 1, 1).toInt());
        bool isOK = false;
        for (int i = 1; i < m_TcpServerClientDatas.count(); i++)
        {
            if (ip == m_TcpServerClientDatas[i].ip && port == m_TcpServerClientDatas[i].port)
            {
                isOK = true;
            }
        }
        if (!isOK)
        {
            Inform("未找到当前客户端！");
        }
    }
    QString type = m_staTCPServerHistoryModel->item(row, 2)->text();
    QString senddata = m_staTCPServerHistoryModel->item(row, 3)->text();
    if (type == "Hex" || type == "HEX")
    {
        TCPServerSendHex(ip, port, senddata);
    }
    else
    {
        TCPServerSendLation(ip, port, senddata, Str2TextFormat(type));
    }
}

///
/// \brief 双击TCP Client 发送记录进行发送
/// \param index
///
void CommDubugAssist::on_tableView_TCPClientSendHistory_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    if (m_tcpclient->isOpen())
    {
        QString senddata = m_staTCPClientHistoryModel->item(row, 2)->text();
        QString type = m_staTCPClientHistoryModel->item(row, 1)->text();
        if (type == "Hex")
        {
            TCPClientSendHex(senddata);
        }
        else
        {
            TCPClientSendLation(senddata, Str2TextFormat(type));
        }
    }
    else
    {
        Inform("未连接服务端！");
    }
}

///
/// \brief TCP Server 快捷发送的选择
/// \param index
///
void CommDubugAssist::on_tableView_TCPServerQuickSend_clicked(const QModelIndex &index)
{
    m_TCPServerQuickSendChoose = index.row();
}

///
/// \brief TCP Server 快捷发送
/// \param index
///
void CommDubugAssist::on_tableView_TCPServerQuickSend_doubleClicked(const QModelIndex &index)
{
    //m_TCPServerQuickSendChoose = index.row();
    int row = index.row();
    if (ui->comboBox_TCPServerReadTarget->currentText() != "NULL")
    {
        QString ip = m_TCPServerQuickSendDatas[row].ip;
        quint16 port = m_TCPServerQuickSendDatas[row].port;
        if (ip != "ALL Connections")
        {
            bool pd = false;
            for (int i = 1; i < m_TcpServerClientDatas.count(); i++)
            {
                if (ip == m_TcpServerClientDatas[i].ip && port == m_TcpServerClientDatas[i].port)
                {
                    pd = true;
                    break;
                }
            }
            if (!pd)
            {
                Inform("当前的客户端没有连接！");
                return ;
            }
        }
        QString senddata = m_TCPServerQuickSendDatas[row].writedata;
        if (m_TCPServerQuickSendDatas[row].addLine)
        {
            senddata.append("\n");
        }
        if (m_TCPServerQuickSendDatas[row].isHex)
        {
            TCPServerSendHex(ip, port, senddata);
        }
        else
        {
            TCPServerSendLation(ip, port, senddata, Local);
        }
    }
    else
    {
        Inform("当前无客户端连接！");
    }
}

///
/// \brief TCP Client 快捷发送的选择
/// \param index
///
void CommDubugAssist::on_tableView_TCPClientQuickSend_clicked(const QModelIndex &index)
{
    m_TCPClientQuickSendChoose = index.row();
}

///
/// \brief TCP Client 快捷发送
/// \param index
///
void CommDubugAssist::on_tableView_TCPClientQuickSend_doubleClicked(const QModelIndex &index)
{
    //m_TCPClientQuickSendChoose = index.row();
    int row = index.row();
    if (m_tcpclient->isOpen())
    {
        QString senddata = m_TCPClientQuickSendDatas[row].writedata;
        if (m_TCPClientQuickSendDatas[row].addLine)
        {
            senddata.append("\n");
        }
        if (m_TCPClientQuickSendDatas[row].isHex)
        {
            TCPClientSendHex(senddata);
        }
        else
        {
            TCPClientSendLation(senddata, Local);
        }
    }
    else
    {
        Inform("尚未连接TCP服务端！");
    }
}

///
/// \brief TCP Client 互动发送点击选择事件
/// \param index
///
void CommDubugAssist::on_tableView_TCPClientInteract_clicked(const QModelIndex &index)
{
    m_TCPClientInteractChoose = index.row();
}

///
/// \brief TCP Server 互动发送点击选择事件
/// \param index
///
void CommDubugAssist::on_tableView_TCPServerInteract_clicked(const QModelIndex &index)
{
    m_TCPServerInteractChoose = index.row();
}

///
/// \brief TCP Client 高级循环发送
///
void CommDubugAssist::on_btn_TCPClientSeniorSendCycleStart_clicked()
{
    if (ui->btn_TCPClientSeniorSendCycleStart->text() == "开始循环")
    {
        if (m_tcpclient->isOpen())
        {
            if (m_TCPClientCycleDatas.count() > 0)
            {
                ui->btn_TCPClientSeniorSendCycleStart->setText("终止循环");
                w_TCPC_Cycle->SetContinue(true);
                emit TCPClientStartCycle(m_TCPClientCycleDatas, m_nTCPClientCycleDelay);
            }
            else
            {
                Inform("当前无循环内容，请先添加循环内容！");
            }
        }
        else
        {
            Inform("尚未连接服务端！");
        }
    }
    else if (ui->btn_TCPClientSeniorSendCycleStart->text() == "终止循环")
    {
        ui->btn_TCPClientSeniorSendCycleStart->setText("开始循环");
        w_TCPC_Cycle->SetContinue(false);
        ui->label_TCPClientSeniorSendCycle->setText("当前无执行事件");
    }
}

///
/// \brief 编辑 TCP Client 高级循环
///
void CommDubugAssist::on_btn_TCPClientSeniorSendCycleEditor_clicked()
{
    if (ui->btn_TCPClientSeniorSendCycleStart->text() == "终止循环")
    {
        bool isOK = AskInform("当前高级循环发送功能正在执行！无法保存被修改的内容！是否确认打开？");
        if (!isOK)
        {
            return ;
        }
    }
    w_SSCycle->SetUInum(2);
    w_SSCycle->SetData(m_TCPClientCycleDatas, static_cast<int>(m_nTCPClientCycleDelay));
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_SSCycle->windowFlags();
        w_SSCycle->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_SSCycle->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_SSCycle->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_SSCycle->show();
}

///
/// \brief 编辑 TCP Server 高级循环
///
void CommDubugAssist::on_btn_TCPServerSeniorSendCycleEditor_clicked()
{
    if (ui->btn_TCPServerSeniorSendCycleStart->text() == "终止循环")
    {
        bool isOK = AskInform("当前高级循环发送功能正在执行！无法保存被修改的内容！是否确认打开？");
        if (!isOK)
        {
            return ;
        }
    }
    w_TCPS_SSCycle->SetObjects(m_TcpServerClientDatas);
    w_TCPS_SSCycle->SetData(m_TCPServerCycleDatas, static_cast<int>(m_nTCPServerCycleDelay));
    if (pTitleBar->getLockState())
    {
        Qt::WindowFlags m_flags = w_TCPS_SSCycle->windowFlags();
        w_TCPS_SSCycle->window()->setWindowFlags(m_flags | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    }
    else
    {
        //Qt::WindowFlags m_flags = nullptr;
        //w_TCPS_SSCycle->window()->setWindowFlags(m_flags | Qt::FramelessWindowHint);
        w_TCPS_SSCycle->window()->setWindowFlags(Qt::FramelessWindowHint);
    }
    w_TCPS_SSCycle->show();
}

///
/// \brief TCP Server 高级循环发送
///
void CommDubugAssist::on_btn_TCPServerSeniorSendCycleStart_clicked()
{
    if (ui->btn_TCPServerSeniorSendCycleStart->text() == "开始循环")
    {
        if (ui->comboBox_TCPServerReadTarget->currentText() != "NULL")
        {
            if (m_TCPServerCycleDatas.count() > 0)
            {
                ui->btn_TCPServerSeniorSendCycleStart->setText("终止循环");
                w_TCPS_Cycle->SetContinue(true);
                emit TCPServerStartCycle(m_TCPServerCycleDatas, m_nTCPServerCycleDelay);
            }
            else
            {
                Inform("当前无循环内容，请先添加循环内容！");
            }
        }
        else
        {
            Inform("当前无客户端连接！");
        }
    }
    else if (ui->btn_TCPServerSeniorSendCycleStart->text() == "终止循环")
    {
        ui->btn_TCPServerSeniorSendCycleStart->setText("开始循环");
        w_TCPS_Cycle->SetContinue(false);
        ui->label_TCPServerSeniorSendCycle->setText("当前无执行事件");
    }
}

///
/// \brief 清空 TCP Server 发送记录
///
void CommDubugAssist::on_btn_TCPServerClearSend_clicked()
{
    m_staTCPServerHistoryModel->removeRows(0, m_staTCPServerHistoryModel->rowCount());
}

///
/// \brief 清空 TCP Client 发送记录
///
void CommDubugAssist::on_btn_TCPClientClearSend_clicked()
{
    m_staTCPClientHistoryModel->removeRows(0, m_staTCPClientHistoryModel->rowCount());
}

///
/// \brief 双击 TCP Server 互动发送进入修改
/// \param index
///
void CommDubugAssist::on_tableView_TCPServerInteract_doubleClicked(const QModelIndex &index)
{
    m_TCPServerInteractChoose = index.row();
    on_btn_TCPServerInteractRevise_clicked();
}

///
/// \brief 双击 TCP Client 互动发送进入修改
/// \param index
///
void CommDubugAssist::on_tableView_TCPClientInteract_doubleClicked(const QModelIndex &index)
{
    m_TCPClientInteractChoose = index.row();
    on_btn_TCPClientInteractRevise_clicked();
}

///
/// \brief 串口修改回车键发送框
/// \param arg1
///
void CommDubugAssist::on_checkBox_SerialPortEnterSend_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        ui->btn_SerialPortSend->setToolTip("Enter");
    }
    else if (arg1 == Qt::Unchecked)
    {
        ui->btn_SerialPortSend->setToolTip("Ctrl+Enter");
    }
}

///
/// \brief TCP Server 修改回车键发送框
/// \param arg1
///
void CommDubugAssist::on_checkBox_TCPServerEnterSend_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        ui->btn_TCPServerSend->setToolTip("Enter");
    }
    else if (arg1 == Qt::Unchecked)
    {
        ui->btn_TCPServerSend->setToolTip("Ctrl+Enter");
    }
}

///
/// \brief TCP Client 修改回车键发送框
/// \param arg1
///
void CommDubugAssist::on_checkBox_TCPClientEnterSend_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        ui->btn_TCPClientSend->setToolTip("Enter");
    }
    else if (arg1 == Qt::Unchecked)
    {
        ui->btn_TCPClientSend->setToolTip("Ctrl+Enter");
    }
}

///
/// \brief 导出串口快捷发送
///
void CommDubugAssist::SerialPortQuickSendDerive(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file;
    file.setFileName(path);
    if (!file.open(QFile::WriteOnly|QFile::Truncate))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    QDomProcessingInstruction xmlInstruction = doc.createProcessingInstruction("xml", "version=\"1.0.1\" encoding=\"UTF-8\"");
    QDomComment comment = doc.createComment(QString("快捷发送"));
    doc.appendChild(xmlInstruction);
    doc.appendChild(comment);

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement quicksend = doc.createElement("quicksend");
    root.appendChild(quicksend);

    for (int i = 0; i < m_SerialPortQuickSendDatas.count(); i++)
    {
        QDomElement send = doc.createElement(QString("send_%1").arg(i+1));
        send.setAttribute("Name", m_SerialPortQuickSendDatas[i].name);
        send.setAttribute("WriteData", m_SerialPortQuickSendDatas[i].writedata);
        send.setAttribute("IsHex", m_SerialPortQuickSendDatas[i].isHex);
        send.setAttribute("AddLine", m_SerialPortQuickSendDatas[i].addLine);
        quicksend.appendChild(send);
    }

    QTextStream out_stream(&file);
    doc.save(out_stream, 4);
    file.close();
    return ;
}
///
/// \brief 导入串口快捷发送
///
void CommDubugAssist::SerialPortQuickSendInduct(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    if (!doc.setContent(&file))
    {
        file.close();
        return ;
    }
    file.close();

    QDomElement root = doc.documentElement();
    int nodes = root.childNodes().count();
    int i = 0;
    while(i < nodes)
    {
        QDomNode quicksend = root.childNodes().at(i);
        if (quicksend.hasChildNodes() && quicksend.nodeName() == "quicksend")
        {
            m_SerialPortQuickSendDatas.clear();
            SerialPortQuickSendClear();
            QDomNodeList sendList = quicksend.childNodes();
            for (int i = 0; i < sendList.count(); i++)
            {
                QDomNode send = sendList.at(i);
                if (send.hasAttributes())
                {
                    QuickSendData quicksenddata;
                    QDomNamedNodeMap attrs = send.attributes();
                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode n = attrs.item(i);
                        QString nodeName = n.nodeName();
                        QString nodeValue = n.nodeValue();
                        if (nodeName == "Name")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.name = nodeValue;
                            }
                        }
                        else if (nodeName == "WriteData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.writedata = nodeValue;
                            }
                        }
                        else if (nodeName == "IsHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.isHex = static_cast<bool>(nodeValue.toInt());
                            }
                        }
                        else if (nodeName == "AddLine")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.addLine = static_cast<bool>(nodeValue.toInt());
                            }
                        }
                    }
                    m_SerialPortQuickSendDatas.append(quicksenddata);
                    SerialPortQuickSendAdd(quicksenddata.name);
                }
            }
            break;
        }
        i++;
    }
    return ;
}
///
/// \brief 导出串口交互发送
///
void CommDubugAssist::SerialPortInteractDerive(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file;
    file.setFileName(path);
    if (!file.open(QFile::WriteOnly|QFile::Truncate))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    QDomProcessingInstruction xmlInstruction = doc.createProcessingInstruction("xml", "version=\"1.0.1\" encoding=\"UTF-8\"");
    QDomComment comment = doc.createComment(QString("交互发送"));
    doc.appendChild(xmlInstruction);
    doc.appendChild(comment);

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement interact = doc.createElement("interact");
    root.appendChild(interact);

    for (int i = 0; i < m_SerialPortInteractDatas.count(); i++)
    {
        QDomElement inter = doc.createElement(QString("inter_%1").arg(i+1));
        inter.setAttribute("Mode", m_SerialPortInteractDatas[i].mode);
        inter.setAttribute("ModeName", m_SerialPortInteractDatas[i].modename);
        inter.setAttribute("ReadData", m_SerialPortInteractDatas[i].readdata);
        inter.setAttribute("ReadHex", TextFormat2Str(m_SerialPortInteractDatas[i].readTextFormat));
        inter.setAttribute("WriteData", m_SerialPortInteractDatas[i].writedata);
        inter.setAttribute("WriteHex", TextFormat2Str(m_SerialPortInteractDatas[i].sendTextFormat));
        interact.appendChild(inter);
    }

    QTextStream out_stream(&file);
    doc.save(out_stream, 4);
    file.close();
    return ;
}
///
/// \brief 导入串口交互发送
///
void CommDubugAssist::SerialPortInteractInduct(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    if (!doc.setContent(&file))
    {
        file.close();
        return ;
    }
    file.close();

    QDomElement root = doc.documentElement();
    int nodes = root.childNodes().count();
    int i = 0;
    while(i < nodes)
    {
        QDomNode interact = root.childNodes().at(i);
        if (interact.hasChildNodes() && interact.nodeName() == "interact")
        {
            m_SerialPortInteractDatas.clear();
            SerialPortInteractClear();
            QDomNodeList interList = interact.childNodes();
            for (int i = 0; i < interList.count(); i++)
            {
                QDomNode inter = interList.at(i);
                if (inter.hasAttributes())
                {
                    InteractData interactdata;
                    QDomNamedNodeMap attrs = inter.attributes();
                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode n = attrs.item(i);
                        QString nodeName = n.nodeName();
                        QString nodeValue = n.nodeValue();
                        if (nodeName == "Mode")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.mode = nodeValue.toInt();
                            }
                        }
                        else if (nodeName == "ModeName")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.modename = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readdata = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readTextFormat = Str2TextFormat(nodeValue);
                            }
                        }
                        else if (nodeName == "WriteData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.writedata = nodeValue;
                            }
                        }
                        else if (nodeName == "WriteHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.sendTextFormat = Str2TextFormat(nodeValue);
                            }
                        }
                    }
                    m_SerialPortInteractDatas.append(interactdata);
                    SerialPortInteractAdd(interactdata);
                }
            }
            break;
        }
        i++;
    }
    return ;
}

///
/// \brief 导出TCP服务端快捷发送
///
void CommDubugAssist::TCPServerQuickSendDerive(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file;
    file.setFileName(path);
    if (!file.open(QFile::WriteOnly|QFile::Truncate))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    QDomProcessingInstruction xmlInstruction = doc.createProcessingInstruction("xml", "version=\"1.0.1\" encoding=\"UTF-8\"");
    QDomComment comment = doc.createComment(QString("快捷发送"));
    doc.appendChild(xmlInstruction);
    doc.appendChild(comment);

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement quicksend = doc.createElement("quicksend");
    root.appendChild(quicksend);

    for (int i = 0; i < m_TCPServerQuickSendDatas.count(); i++)
    {
        QDomElement send = doc.createElement(QString("send_%1").arg(i+1));
        send.setAttribute("Name", m_TCPServerQuickSendDatas[i].name);
        send.setAttribute("IP", m_TCPServerQuickSendDatas[i].ip);
        send.setAttribute("Port", m_TCPServerQuickSendDatas[i].port);
        send.setAttribute("WriteData", m_TCPServerQuickSendDatas[i].writedata);
        send.setAttribute("IsHex", m_TCPServerQuickSendDatas[i].isHex);
        send.setAttribute("AddLine", m_TCPServerQuickSendDatas[i].addLine);
        quicksend.appendChild(send);
    }
    QTextStream out_stream(&file);
    doc.save(out_stream, 4);
    file.close();
    return ;
}
///
/// \brief 导入TCP服务端快捷发送
///
void CommDubugAssist::TCPServerQuickSendInduct(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    if (!doc.setContent(&file))
    {
        file.close();
        return ;
    }
    file.close();

    QDomElement root = doc.documentElement();
    int nodes = root.childNodes().count();
    int i = 0;
    while(i < nodes)
    {
        QDomNode quicksend = root.childNodes().at(i);
        if (quicksend.hasChildNodes() && quicksend.nodeName() == "quicksend")
        {
            m_TCPServerQuickSendDatas.clear();
            TCPServerQuickSendClear();
            QDomNodeList sendList = quicksend.childNodes();
            for (int i = 0; i < sendList.count(); i++)
            {
                QDomNode send = sendList.at(i);
                if (send.hasAttributes())
                {
                    TCPServerQuickSendData quicksenddata;
                    quicksenddata.ip = "ALL Connections";
                    quicksenddata.port = 0;
                    QDomNamedNodeMap attrs = send.attributes();
                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode n = attrs.item(i);
                        QString nodeName = n.nodeName();
                        QString nodeValue = n.nodeValue();
                        if (nodeName == "Name")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.name = nodeValue;
                            }
                        }
                        else if (nodeName == "IP")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.ip = nodeValue;
                            }
                        }
                        else if (nodeName == "Port")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.port = static_cast<quint16>(nodeValue.toInt());
                            }
                        }
                        else if (nodeName == "WriteData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.writedata = nodeValue;
                            }
                        }
                        else if (nodeName == "IsHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.isHex = static_cast<bool>(nodeValue.toInt());
                            }
                        }
                        else if (nodeName == "AddLine")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.addLine = static_cast<bool>(nodeValue.toInt());
                            }
                        }
                    }
                    m_TCPServerQuickSendDatas.append(quicksenddata);
                    if (quicksenddata.ip == "ALL Connections")
                    {
                        TCPServerQuickSendAdd(quicksenddata.ip, quicksenddata.name);
                    }
                    else
                    {
                        TCPServerQuickSendAdd(quicksenddata.ip + ":" + QString::number(quicksenddata.port), quicksenddata.name);
                    }
                }
            }
            break;
        }
        i++;
    }
    return ;
}
///
/// \brief 导出TCP服务端交互发送
///
void CommDubugAssist::TCPServerInteractDerive(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file;
    file.setFileName(path);
    if (!file.open(QFile::WriteOnly|QFile::Truncate))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    QDomProcessingInstruction xmlInstruction = doc.createProcessingInstruction("xml", "version=\"1.0.1\" encoding=\"UTF-8\"");
    QDomComment comment = doc.createComment(QString("交互发送"));
    doc.appendChild(xmlInstruction);
    doc.appendChild(comment);

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement interact = doc.createElement("interact");
    root.appendChild(interact);

    for (int i = 0; i < m_TCPClientInteractDatas.count(); i++)
    {
        QDomElement inter = doc.createElement(QString("inter_%1").arg(i+1));
        inter.setAttribute("Mode", m_TCPServerInteractDatas[i].mode);
        inter.setAttribute("ModeName", m_TCPServerInteractDatas[i].modename);
        inter.setAttribute("ReadIP", m_TCPServerInteractDatas[i].readip);
        inter.setAttribute("ReadPort", m_TCPServerInteractDatas[i].readport);
        inter.setAttribute("ReadData", m_TCPServerInteractDatas[i].readdata);
        inter.setAttribute("ReadHex", TextFormat2Str(m_TCPServerInteractDatas[i].readTextFormat));
        inter.setAttribute("WriteIP", m_TCPServerInteractDatas[i].writeip);
        inter.setAttribute("WritePort", m_TCPServerInteractDatas[i].writeport);
        inter.setAttribute("WriteData", m_TCPServerInteractDatas[i].writedata);
        inter.setAttribute("WriteHex", TextFormat2Str(m_TCPServerInteractDatas[i].sendTextFormat));
        interact.appendChild(inter);
    }

    QTextStream out_stream(&file);
    doc.save(out_stream, 4);
    file.close();
    return ;
}
///
/// \brief 导入TCP服务端交互发送
///
void CommDubugAssist::TCPServerInteractInduct(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    if (!doc.setContent(&file))
    {
        file.close();
        return ;
    }
    file.close();

    QDomElement root = doc.documentElement();
    int nodes = root.childNodes().count();
    int i = 0;
    while(i < nodes)
    {
        QDomNode interact = root.childNodes().at(i);
        if (interact.hasChildNodes() && interact.nodeName() == "interact")
        {
            m_TCPServerInteractDatas.clear();
            TCPServerInteractClear();
            QDomNodeList interList = interact.childNodes();
            for (int i = 0; i < interList.count(); i++)
            {
                QDomNode inter = interList.at(i);
                if (inter.hasAttributes())
                {
                    TCPServerInteractData interactdata;
                    interactdata.readip = "ALL Connections";
                    interactdata.readport = 0;
                    interactdata.writeip = "ALL Connections";
                    interactdata.writeport = 0;
                    QDomNamedNodeMap attrs = inter.attributes();
                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode n = attrs.item(i);
                        QString nodeName = n.nodeName();
                        QString nodeValue = n.nodeValue();
                        if (nodeName == "Mode")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.mode = nodeValue.toInt();
                            }
                        }
                        else if (nodeName == "ModeName")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.modename = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadIP")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readip = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadPort")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readport = static_cast<quint16>(nodeValue.toInt());
                            }
                        }
                        else if (nodeName == "ReadData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readdata = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readTextFormat = Str2TextFormat(nodeValue);
                            }
                        }
                        else if (nodeName == "WriteIP")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.writeip = nodeValue;
                            }
                        }
                        else if (nodeName == "WritePort")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.writeport = static_cast<quint16>(nodeValue.toInt());
                            }
                        }
                        else if (nodeName == "WriteData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.writedata = nodeValue;
                            }
                        }
                        else if (nodeName == "WriteHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.sendTextFormat = Str2TextFormat(nodeValue);
                            }
                        }
                    }
                    m_TCPServerInteractDatas.append(interactdata);
                    TCPServerInteractAdd(interactdata);
                }
            }
            break;
        }
        i++;
    }
    return ;
}

///
/// \brief 导出TCP客户端快捷发送
///
void CommDubugAssist::TCPClientQuickSendDerive(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file;
    file.setFileName(path);
    if (!file.open(QFile::WriteOnly|QFile::Truncate))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    QDomProcessingInstruction xmlInstruction = doc.createProcessingInstruction("xml", "version=\"1.0.1\" encoding=\"UTF-8\"");
    QDomComment comment = doc.createComment(QString("快捷发送"));
    doc.appendChild(xmlInstruction);
    doc.appendChild(comment);

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement quicksend = doc.createElement("quicksend");
    root.appendChild(quicksend);

    for (int i = 0; i < m_TCPClientQuickSendDatas.count(); i++)
    {
        QDomElement send = doc.createElement(QString("send_%1").arg(i+1));
        send.setAttribute("Name", m_TCPClientQuickSendDatas[i].name);
        send.setAttribute("WriteData", m_TCPClientQuickSendDatas[i].writedata);
        send.setAttribute("IsHex", m_TCPClientQuickSendDatas[i].isHex);
        send.setAttribute("AddLine", m_TCPClientQuickSendDatas[i].addLine);
        quicksend.appendChild(send);
    }

    QTextStream out_stream(&file);
    doc.save(out_stream, 4);
    file.close();
    return ;
}
///
/// \brief 导入TCP客户端快捷发送
///
void CommDubugAssist::TCPClientQuickSendInduct(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    if (!doc.setContent(&file))
    {
        file.close();
        return ;
    }
    file.close();

    QDomElement root = doc.documentElement();
    int nodes = root.childNodes().count();
    int i = 0;
    while(i < nodes)
    {
        QDomNode quicksend = root.childNodes().at(i);
        if (quicksend.hasChildNodes() && quicksend.nodeName() == "quicksend")
        {
            m_TCPClientQuickSendDatas.clear();
            TCPClientQuickSendClear();
            QDomNodeList sendList = quicksend.childNodes();
            for (int i = 0; i < sendList.count(); i++)
            {
                QDomNode send = sendList.at(i);
                if (send.hasAttributes())
                {
                    QuickSendData quicksenddata;
                    QDomNamedNodeMap attrs = send.attributes();
                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode n = attrs.item(i);
                        QString nodeName = n.nodeName();
                        QString nodeValue = n.nodeValue();
                        if (nodeName == "Name")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.name = nodeValue;
                            }
                        }
                        else if (nodeName == "WriteData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.writedata = nodeValue;
                            }
                        }
                        else if (nodeName == "IsHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.isHex = static_cast<bool>(nodeValue.toInt());
                            }
                        }
                        else if (nodeName == "AddLine")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                quicksenddata.addLine = static_cast<bool>(nodeValue.toInt());
                            }
                        }
                    }
                    m_TCPClientQuickSendDatas.append(quicksenddata);
                    TCPClientQuickSendAdd(quicksenddata.name);
                }
            }
            break;
        }
        i++;
    }
    return ;
}
///
/// \brief 导出TCP客户端交互发送
///
void CommDubugAssist::TCPClientInteractDerive(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file;
    file.setFileName(path);
    if (!file.open(QFile::WriteOnly|QFile::Truncate))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    QDomProcessingInstruction xmlInstruction = doc.createProcessingInstruction("xml", "version=\"1.0.1\" encoding=\"UTF-8\"");
    QDomComment comment = doc.createComment(QString("交互发送"));
    doc.appendChild(xmlInstruction);
    doc.appendChild(comment);

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement interact = doc.createElement("interact");
    root.appendChild(interact);

    for (int i = 0; i < m_TCPClientInteractDatas.count(); i++)
    {
        QDomElement inter = doc.createElement(QString("inter_%1").arg(i+1));
        inter.setAttribute("Mode", m_TCPClientInteractDatas[i].mode);
        inter.setAttribute("ModeName", m_TCPClientInteractDatas[i].modename);
        inter.setAttribute("ReadData", m_TCPClientInteractDatas[i].readdata);
        inter.setAttribute("ReadHex", TextFormat2Str(m_TCPClientInteractDatas[i].readTextFormat));
        inter.setAttribute("WriteData", m_TCPClientInteractDatas[i].writedata);
        inter.setAttribute("WriteHex", TextFormat2Str(m_TCPClientInteractDatas[i].sendTextFormat));
        interact.appendChild(inter);
    }

    QTextStream out_stream(&file);
    doc.save(out_stream, 4);
    file.close();
    return ;
}
///
/// \brief 导入TCP客户端交互发送
///
void CommDubugAssist::TCPClientInteractInduct(QString path)
{
    if (path.isEmpty())
    {
        return ;
    }
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
    {
        file.close();
        return ;
    }
    QDomDocument doc;
    if (!doc.setContent(&file))
    {
        file.close();
        return ;
    }
    file.close();

    QDomElement root = doc.documentElement();
    int nodes = root.childNodes().count();
    int i = 0;
    while(i < nodes)
    {
        QDomNode interact = root.childNodes().at(i);
        if (interact.hasChildNodes() && interact.nodeName() == "interact")
        {
            m_TCPClientInteractDatas.clear();
            TCPClientInteractClear();
            QDomNodeList interList = interact.childNodes();
            for (int i = 0; i < interList.count(); i++)
            {
                QDomNode inter = interList.at(i);
                if (inter.hasAttributes())
                {
                    InteractData interactdata;
                    QDomNamedNodeMap attrs = inter.attributes();
                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode n = attrs.item(i);
                        QString nodeName = n.nodeName();
                        QString nodeValue = n.nodeValue();
                        if (nodeName == "Mode")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.mode = nodeValue.toInt();
                            }
                        }
                        else if (nodeName == "ModeName")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.modename = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readdata = nodeValue;
                            }
                        }
                        else if (nodeName == "ReadHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.readTextFormat = Str2TextFormat(nodeValue);
                            }
                        }
                        else if (nodeName == "WriteData")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.writedata = nodeValue;
                            }
                        }
                        else if (nodeName == "WriteHex")
                        {
                            if (!nodeValue.isEmpty())
                            {
                                interactdata.sendTextFormat = Str2TextFormat(nodeValue);
                            }
                        }
                    }
                    m_TCPClientInteractDatas.append(interactdata);
                    TCPClientInteractAdd(interactdata);
                }
            }
            break;
        }
        i++;
    }
    return ;
}

///
/// \brief 接收需要发送的文件数据
/// \param sendID
/// \param buf
/// \param len
///
void CommDubugAssist::SendFileGetData(unsigned char sendID, char *buf, qint64 len)
{
    if (buf == nullptr)
    {
        return ;
    }
    if (sendID == 1)   // 串口
    {
        if (m_serialPort->isOpen())
        {
            m_serialPort->write(buf, len);
            m_SendSize += len;
            ui->progressBar_SerialPortSendFiled->setValue((m_SendSize * 100/m_FileSize));
            if (m_SendSize == m_FileSize)
            {
                Inform("文件发送成功");
                ui->progressBar_SerialPortSendFiled->hide();
                ui->btn_SerialPortPausedSendFile->hide();
                ui->btn_SerialPortStopSendFile->hide();
                ui->btn_SerialPortChooseSendFile->setEnabled(true);
                ui->btn_SerialPortSendFile->setEnabled(true);
            }
        }
        else
        {
            m_sendFile->stop();
            Inform("串口连接已断开！文件发送失败！");
            ui->progressBar_SerialPortSendFiled->hide();
            ui->btn_SerialPortPausedSendFile->hide();
            ui->btn_SerialPortStopSendFile->hide();
            ui->btn_SerialPortChooseSendFile->setEnabled(true);
            ui->btn_SerialPortSendFile->setEnabled(true);
        }
    }
}

///
/// \brief 串口选择发送文件
///
void CommDubugAssist::on_btn_SerialPortChooseSendFile_clicked()
{
    QString defaultPath;
    if (m_LastOpenPath.isEmpty())
    {
        defaultPath = "/";
    }
    else if (CheckAndEstablishFiles(m_LastOpenPath))//检查路径是否存在
    {
        defaultPath = m_LastOpenPath;
    }
    else
    {
        defaultPath = "/";
    }
    QString path = QFileDialog::getOpenFileName(this, "选择串口发送文件", defaultPath, "DATA(*.bin *.HEX);;TXT(*.txt);;other(*.*)");
    if (!path.isEmpty())
    {
        ui->lineEdit_SerialPortSendFilePath->setText(path);
        m_LastOpenPath = PathxtoPath(path);
    }
}

///
/// \brief 串口发送文件
///
void CommDubugAssist::on_btn_SerialPortSendFile_clicked()
{
    if (m_serialPort->isOpen())
    {
        QString path = ui->lineEdit_SerialPortSendFilePath->text();
        if (path.isEmpty())
        {
            Inform("发送内容为空！");
            return ;
        }
        QFile file(path);
        if (!file.open(QIODevice::ReadOnly))
        {
            Inform("文件读取失败！");
            return ;
        }
        QFileInfo fileinfo(path);
        m_SendFileName = fileinfo.fileName();
        m_FileSize = fileinfo.size();
        m_SendSize = 0;
        ui->progressBar_SerialPortSendFiled->setValue(0);
        ui->progressBar_SerialPortSendFiled->show();
        ui->btn_SerialPortPausedSendFile->show();
        ui->btn_SerialPortStopSendFile->show();
        ui->btn_SerialPortChooseSendFile->setEnabled(false);
        ui->btn_SerialPortSendFile->setEnabled(false);
        m_sendFile->initStart(1, path);
    }
    else
    {
        Inform("串口尚未连接！");
    }
}

///
/// \brief 暂停发送/继续发送
///
void CommDubugAssist::on_btn_SerialPortPausedSendFile_clicked()
{
    if (ui->btn_SerialPortPausedSendFile->text() == "暂停发送")
    {
        m_sendFile->paused(false);
        ui->btn_SerialPortPausedSendFile->setText("继续发送");
        ui->progressBar_SerialPortSendFiled->setEnabled(false);
    }
    else
    {
        m_sendFile->paused(true);
        ui->btn_SerialPortPausedSendFile->setText("暂停发送");
        ui->progressBar_SerialPortSendFiled->setEnabled(true);
    }
}

///
/// \brief 终止发送文件
///
void CommDubugAssist::on_btn_SerialPortStopSendFile_clicked()
{
    m_sendFile->stop();
    Inform("文件发送已终止");
    ui->progressBar_SerialPortSendFiled->hide();
    ui->btn_SerialPortPausedSendFile->hide();
    ui->btn_SerialPortStopSendFile->hide();
    ui->btn_SerialPortPausedSendFile->setText("暂停发送");
    ui->btn_SerialPortChooseSendFile->setEnabled(true);
    ui->btn_SerialPortSendFile->setEnabled(true);
}
