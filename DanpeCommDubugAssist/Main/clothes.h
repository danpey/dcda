#ifndef CLOTHES_H
#define CLOTHES_H

#include <QWidget>
#include <QButtonGroup>
#include "basecode.h"

namespace Ui {
class Clothes;
}

class Clothes : public QWidget
{
    Q_OBJECT

public:
    explicit Clothes(QWidget *parent = nullptr);
    ~Clothes();

    void show();
    void close();
    void InitQSS();

signals:
    void SetQSSSignal();

private slots:
    void on_changeClothesGroup(int num);

private:
    Ui::Clothes *ui;

    void InitTitlepar();
    void Init();

    QButtonGroup *m_btnGroup;
    WidgetPar *m_selMainWidget;
};

#endif // CLOTHES_H
