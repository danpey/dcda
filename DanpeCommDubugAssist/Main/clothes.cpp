#include "clothes.h"
#include "ui_clothes.h"

Clothes::Clothes(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Clothes)
{
    ui->setupUi(this);

    InitTitlepar();
    Init();
}

Clothes::~Clothes()
{
    delete ui;
}

void Clothes::InitTitlepar()
{
    QDesktopWidget* desktop = QApplication::desktop();

    m_selMainWidget = new WidgetPar; //创建一个QWidget容器
    m_selMainWidget->setWindowFlags(Qt::FramelessWindowHint);//将这个QWidget的边框去掉

    this->setParent(m_selMainWidget);//重新设置这个UI界面的父对象为QWidget
    TitleBar *pTitleBar = new TitleBar(m_selMainWidget); //定义一个标题栏类

    pTitleBar->showMinmizeBtn(false);
    pTitleBar->showMaxmizeBtn(false);

    this->installEventFilter(pTitleBar);//安装事件过滤器
    QGridLayout *pLayout = new QGridLayout();//创建一个整体布局器
    pLayout->addWidget(pTitleBar);  //添加标题栏
    pLayout->addWidget(this);       //添加UI界面
    pLayout->setSpacing(0);         //布局之间的距离
    pLayout->setContentsMargins(0, 0, 0, 0); //布局器的四周边距
    m_selMainWidget->setLayout(pLayout);  //将这个布局器设置在QWidget上
    m_selMainWidget->setSizePolicy(this->sizePolicy());
    m_selMainWidget->setMaximumSize(this->maximumSize());
    m_selMainWidget->setMinimumWidth(this->minimumWidth() + 2);
    if (this->minimumHeight() + pTitleBar->height() + 10 > 60)
    {
        m_selMainWidget->setMinimumHeight(this->minimumHeight() + pTitleBar->height() + 10);
    }
    else
    {
        m_selMainWidget->setMinimumHeight(pTitleBar->height() + 2);
    }
    m_selMainWidget->setGeometry((desktop->width() - this->width())/2, (desktop->height() - this->height() - pTitleBar->height())/2, this->width(), this->height() + pTitleBar->height() + 10);
    m_selMainWidget->setTitleHeight(static_cast<uint>(pTitleBar->height()));

    connect(m_selMainWidget, &WidgetPar::CloseSignal, this, &Clothes::closeEvent);
    m_selMainWidget->setWidgetResizable(false);  // 设置窗体不可缩放

    ui->btn_bluewhite->setProperty("class", "MyClothes");
    ui->btn_darkorange->setProperty("class", "MyClothes");
    if (g_sInitQSSPath == ":/qss/bluewhite/bluewhite.qss")
    {
        ui->btn_bluewhite->setProperty("class", "ChooseClothes");
    }
    else if (g_sInitQSSPath == ":/qss/darkorange/darkorange.qss")
    {
        ui->btn_darkorange->setProperty("class", "ChooseClothes");
    }

    m_selMainWidget->setWindowTitle(QString("换肤"));
    this->setWindowTitle(QString("换肤"));
    setWindowIcon(QIcon(":/image/DCDA.ico"));
}

void Clothes::InitQSS()
{
    if (g_sInitQSSPath == ":/qss/bluewhite/bluewhite.qss")
    {
        ui->btn_bluewhite->setProperty("class", "ChooseClothes");
        ui->btn_darkorange->setProperty("class", "MyClothes");
    }
    else if (g_sInitQSSPath == ":/qss/darkorange/darkorange.qss")
    {
        ui->btn_bluewhite->setProperty("class", "MyClothes");
        ui->btn_darkorange->setProperty("class", "ChooseClothes");
    }
    QFile file(g_sInitQSSPath);
    bool isOK = file.open(QFile::ReadOnly);
    if (isOK)
    {
        QTextStream filetext(&file);
        QString stylesheet = filetext.readAll();
        m_selMainWidget->setStyleSheet(stylesheet);
    }
    file.close();


}

void Clothes::Init()
{
    ui->btn_bluewhite->setCheckable(true);
    ui->btn_darkorange->setCheckable(true);
    m_btnGroup = new QButtonGroup(this);
    m_btnGroup->addButton(ui->btn_bluewhite);
    m_btnGroup->addButton(ui->btn_darkorange);
    m_btnGroup->setExclusive(true);
    connect(m_btnGroup, SIGNAL(buttonClicked(int)), this, SLOT(on_changeClothesGroup(int)));
}

void Clothes::show()
{
    m_selMainWidget->show();
}

void Clothes::close()
{
    m_selMainWidget->close();
}

void Clothes::on_changeClothesGroup(int num)
{
    Q_UNUSED(num)
    //qDebug() << "num:" << num;
    QPushButton *checkedButton = qobject_cast<QPushButton*>(m_btnGroup->checkedButton());
    if (checkedButton->objectName() == "btn_bluewhite")
    {
        if (g_sInitQSSPath != ":/qss/bluewhite/bluewhite.qss")
        {
            g_sInitQSSPath = ":/qss/bluewhite/bluewhite.qss";
            emit SetQSSSignal();
        }
    }
    else
    {
        if (g_sInitQSSPath != ":/qss/darkorange/darkorange.qss")
        {
            g_sInitQSSPath = ":/qss/darkorange/darkorange.qss";
            emit SetQSSSignal();
        }
    }
}
