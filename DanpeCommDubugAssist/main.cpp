#include "Main/commdubugassist.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    g_sAppName = a.applicationFilePath().section('/', -1);
    bool isOK = CheckRepeatProcess();
    if (isOK)
    {
        QMessageBox m_r;
        m_r.setWindowTitle(QString("蛋皮通信调试助手 ") + VERSION);
        m_r.setWindowIcon(QIcon(":/image/DCDA.ico"));
        m_r.setText("程序已经在运行中，是否确认需要双开程序？双开程序为独立程序，无法保存任何设置！");
        m_r.addButton("确认", QMessageBox::ActionRole);
        m_r.addButton("取消", QMessageBox::ActionRole);
        int oknum = m_r.exec();
        if (oknum == 1)
        {
            return 0;
        }
        else
        {
            g_bIndependent = true;
        }
    }
    CommDubugAssist w;
    w.show();
    return a.exec();
}
