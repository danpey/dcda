QT       += core gui serialport network xml multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Main/basecode.cpp \
    Main/clothes.cpp \
    Main/commdubugassist.cpp \
    Main/preferences.cpp \
    Main/widgetpar.cpp \
    Other/SendFile.cpp \
    Other/about.cpp \
    Other/help.cpp \
    Other/interactunit.cpp \
    Other/quicksendunit.cpp \
    Other/seniorsendcycle.cpp \
    Other/upgra.cpp \
    SerialPort/serialportcycle.cpp \
    TCPClient/tcpclient.cpp \
    TCPClient/tcpclientcycle.cpp \
    TCPServer/tcpserver.cpp \
    TCPServer/tcpservercycle.cpp \
    TCPServer/tcpserverinteractunit.cpp \
    TCPServer/tcpserverquicksendunit.cpp \
    TCPServer/tcpserverseniorsendcycle.cpp \
    main.cpp \

HEADERS += \
    Main/clothes.h \
    Main/commdubugassist.h \
    Main/preferences.h \
    Main/widgetpar.h \
    Other/QReplyTimeout.h \
    Other/SendFile.h \
    Other/about.h \
    Other/help.h \
    Other/interactunit.h \
    Other/quicksendunit.h \
    Other/seniorsendcycle.h \
    Other/upgra.h \
    SerialPort/serialportcycle.h \
    TCPClient/tcpclient.h \
    TCPClient/tcpclientcycle.h \
    TCPServer/tcpserver.h \
    TCPServer/tcpservercycle.h \
    TCPServer/tcpserverinteractunit.h \
    TCPServer/tcpserverquicksendunit.h \
    TCPServer/tcpserverseniorsendcycle.h \
    Main/basecode.h

FORMS += \
    Main/clothes.ui \
    Main/preferences.ui \
    Other/about.ui \
    Other/help.ui \
    Other/interactunit.ui \
    Other/quicksendunit.ui \
    Other/seniorsendcycle.ui \
    Other/upgra.ui \
    TCPServer/tcpserverinteractunit.ui \
    TCPServer/tcpserverquicksendunit.ui \
    TCPServer/tcpserverseniorsendcycle.ui \
    Main/commdubugassist.ui

TRANSLATIONS += \
    DanpeCommDubugAssist_zh_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    DanpeCommDubugAssist_zh_CN.ts \
    README.md \
    UpdatePlan.md

RC_ICONS = image/DCDA.ico

VERSION = 0.1.0.2

QMAKE_TARGET_PRODUCT = DanpeCommDubugAssist
QMAKE_TARGET_COMPANY = Danpe
QMAKE_TARGET_COPYRIGHT = Danpe

RESOURCES += \
    qss.qrc
