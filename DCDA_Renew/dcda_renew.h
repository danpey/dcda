#ifndef DCDA_UPDATE_H
#define DCDA_UPDATE_H

#include <QMainWindow>
#include <QUrl>
#include <QFile>
#include <QSettings>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtCore>
#include <QObject>
#include <QDebug>
#include <QDomDocument>
#include <QFileDialog>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QDir>
#include <QTextStream>
#include <QSocketNotifier>
#include <QTime>
#include <QCoreApplication>
#include <QProcess>

#include "widgetpar.h"

QT_BEGIN_NAMESPACE
namespace Ui { class DCDA_Renew; }
QT_END_NAMESPACE

class DCDA_Renew : public QMainWindow
{
    Q_OBJECT

public:
    DCDA_Renew(QWidget *parent = nullptr);
    ~DCDA_Renew();

    void InitQSS();
    void ReadUpDate();
    void inform(QString strdata);
    bool AskInform(QString strdata);

private slots:
    void ReadyRead();
    void updateDataReadProgress(qint64 bytesRead, qint64 bytesTotal);
    void Finished();
    void ReadFromServer(int);

    void on_btn_cancel_clicked();
    void on_btn_update_clicked();

//    void on_btn_test_clicked();

private:
    Ui::DCDA_Renew *ui;

    void InitTitlepar();
    bool CheckAndEstablishFiles(QString path);
    void updateStart();

    void MySleep(unsigned int msec);

    WidgetPar *m_selMainWidget;
    TitleBar *pTitleBar;

    QString m_sInitQSSPath;
    QString m_sAppnames;

    ///
    /// \brief 程序所在路径
    ///
    QString m_sAppPath;

    QString m_sVer;
    QString m_sNewVer;
    QString m_sDate;
    QString m_sDownPath;
    QStringList m_sUpContents;

    bool m_bCanUpdate = false;
    bool m_bGetUpdate = false;

    int m_nUpdateState = 0;

    QUrl m_url;
    QUrl m_DownUrl;
    QFile *m_file;
    QString m_sFilename;

    QFile filein;
    QSocketNotifier * sn;

    QNetworkAccessManager *m_DownManager;
    QNetworkReply *m_DownReply;

    QProcess *m_pro = nullptr;

    long long m_bytesCurrentReceived = 0;

};
#endif // DCDA_UPDATE_H
