# DanpeCommDubugAssist  蛋皮通讯调试助手

#### 介绍
本软件支持串口 / TCP Server / TCP Client 三合一的通讯调试工具。
并且支持快捷发送、互动发送以及高级循环发送的高级功能。
软件是基于 Qt Creator 写的，意味着可以支持多平台编译以及使用。
也欢迎各位提出更好的建议帮助我一起优化该软件。

详细介绍地址：[https://danpe.top/dcda/](https://danpe.top/dcda/)

开源地址：[https://gitee.com/danpe/DanpeCommDubugAssist](https://gitee.com/danpe/DanpeCommDubugAssist)

软件截图：

![软件截图](http://danpe.top/wp-content/uploads/2020/04/%E8%BD%AF%E4%BB%B6%E6%88%AA%E5%9B%BE.png)

#### 依赖
- Qt5Core
- Qt5Gui
- Qt5Network
- Qt5SerialPort
- Qt5Svg
- Qt5Widgets
- Qt5Xml

#### Linux 编译依赖

- Arch Linux 编译依赖

```bash
yay -S qt5-svg qt5-serialport qt5-multimedia
```

#### Arch Linux 安装蛋皮通讯调试助手

```bash
# 发行版
yay -S dcda
# 测试版
yay -S dcda-git
```

#### 更新日志
    Beta 1.0.1 (2020-6-2)
      1. 更新帮助内容
      2. 修改与优化关于界面
      3. 添加软件LOGO
      4. 优化关闭软件的提示
      5. 将功能建议与报告bug的菜单指向网站连接
      6. 优化版本号的显示
      7. 添加菜单快捷键
      8. 增加拖拽外部文件到程序内即可导入文件内容到发送框
      9. 增加了多个按钮的快捷键，并对可使用快捷键的按钮添加了快捷键提示
      10. 主界面所有表格增加了内容提示功能，便于在表格内容展示不完整时通过提示显示完整内容
      11. 添加更换皮肤页面，新增两款皮肤
      12. 为了配合新增的皮肤功能，统一界面的整体风格，删除了白底黑字的复选框功能
      13. 添加主界面窗口置顶的功能
      14. 添加了首选项设置，支持修改与保存多项设置
      15. 支持导出和导入快捷发送和交互发送的功能
      16. 优化软件部分操作
      17. 添加软件双开提醒并支持多开软件
      18. 新增软件自动检测更新的功能，并支持在线更新

    Beta 1.0.0 (2020-4-22)
      1. 正式发布测试版本
